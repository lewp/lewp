**This project is no longer maintained. Please find its successor written in Rust at https://github.com/emirror-de/lewp-rs :-)**

![lewp logo](logo/lewp-transparent-background.inkscape.svg  "lewp logo")

# Why lewp?

Many PHP frameworks already exist that support developers in creating websites and apps such as [Laravel](https://laravel.com/), [Symfony](https://symfony.com/), or [Laminas](https://docs.laminas.dev/) just to name a few. They usually follow the [MVC pattern](https://www.tutorialspoint.com/design_pattern/mvc_pattern.htm). You can create a controller for every route and add a model as well as a view for this, all in separate folders. So far, nothing new, a well known and proven concept. However, several problems can arise regarding e.g. code quality, re-usability as well as best practices of SEO and much more. The architecture of these frameworks can lead developers to become inconsistent considering their code structure. Because the files are spread over the whole project, the reusability of code can easily suffer. In addition to that, the usage of templating  systems like [Twig](https://twig.symfony.com/) can make the developer create invalid HTML code without knowing it or create problems relating to for example page loading times [(which is a crucial factor)](https://www.marketingdive.com/news/google-53-of-mobile-users-abandon-sites-that-take-over-3-seconds-to-load/426070/), the [critical rendering path](https://varvy.com/pagespeed/critical-render-path.html) or avoiding ["extra whitespace between HTML tags to avoid browser rendering quirks under some circumstances"](https://twig.symfony.com/doc/3.x/filters/spaceless.html). In the latter (a cite from the [Twig](https://twig.symfony.com/doc/2.x/filters/spaceless.html) documentation), Twig has a property to prevent this, but you need to explicitly enable it and "its performance is directly related to the text size you are working on". Developing in these environments can run the risk of spreading those problems over your whole project, making it almost impossible to recover from without rewriting your code base.

So the idea was to create a framework that tries to minimize the impact of the stated problems without making compromises on the comfort of programming.

Therefore, lewp focuses on

* making it **easy** for the developer in **creating valid** HTML code,
* make the developer **instantly know where** the **files are stored**,
* ***true* modularity** and therefore **re-usability** of source code,
* **minimize** the [**cumulative layout shift**](https://web.dev/cls/) on loading
* applying **SEO** best practices already in development setup,
* **minimization** **page loading times**, especially [FCP](https://web.dev/first-contentful-paint/) and [TTI](https://web.dev/interactive/),
* **flexibility** for the developer (you are able to add other dependencies to your modules without effort),
* enabling to run **multiple websites** on the **same code base**,
* **reducing** the **external dependencies** that are sent to the client and therefore
* **minimize** the **chaos in** your projects **source code**

lewp additionally includes features like

- lazy loading for images,

- localization without effort,

- websocket support,

- and many more

out of the box!

Sounds like utopia? Well lets see if lewp fits for your project by explaining the concepts and strategy to tackle these complex goals.

# Conceptional thoughts

The very initial goal of lewp was to enable developers to create a code base that can be used for multiple websites without the need of making too much changes. As a result of that, the upgrade process of single modules on multiple websites is reduced to the module itself and therefore super easy (but it is still possible to use different versions on different websites, as you will see).

To enable the developer to always write valid HTML code, lewp uses the DOMDocument class of PHP. There are three main classes in a lewp webpage

* Site, representing the whole website,

* Page, representing a single page of your website,

* Module, representing a part of your Page.

The last two classes extend the DOMDocument class. The Site class manages all requests and echos the matching page to the user. The Page class will get instanciated by the Site class and is able to contain as many instances of the Module class as your page requires. In Addition to that, you can add custom elements that are not contained in a module to the page (not recommended).

In addition to that, the usage of DOMDocument enables the developer to have similar syntax on the server side as well as on the client side when creating or removing elements from the webpage (e.g. createElement function).

Another aspect, besides the code quality on the server side is the quality as well as modularity on the client side. To enable true modularity on the client side for CSS, the code written automatically gets namespaced, so you will never need to worry about affecting other elements on the page. This simply cannot happen! The result for the developer is less css classes as well as CSS code that is way better to read. For true modularity considering your JavaScript, the code of the module is being wrapped into an anonymous function that gets executed on the modulewrapper, so its code is only applied to the element itself and everything that it contains. So as long as you do not explicitly wish to leave the scope you can be sure that your code is applied to the module only.

# The file hierarchy and <img title="lewp logo" src="logo/lewp-transparent-background.inkscape.svg" alt="lewp logo" style="zoom:33%;" width="50"> levels

One of the key concepts of lewp is the file hierarchy (inspired by the [Filesystem Hierarchy Standard](https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)) that allows developers to create modules that are independent already on the file system. By dividing the hierarchy in different access levels (the "lewp levels"), lewp enables the developer to choose whether a module can be used on multiple websites or not, or if resources such as icons, images, fonts, static HTML, videos etc. should be available throughout different domains.

Because creating the file hierarchy for newcomers can be a pain, lewp provides a [template project](https://gitlab.com/lewp/lewp-template-project) that contains all you need for the start.
Here is a brief impression of the file hierarchy for the lewp.org domain:

```bash
.                                  # --> the "global" level
├── lib
│   └── js
├── modules                        # --> the "global module" level
├── resources
│   ├── fonts
│   ├── html
│   ├── icons
│   ├── images
│   ├── json
│   ├── l10n
│   ├── movies
│   └── text
└── www
    └── lewp.org                   # --> the "page" level
        ├── bin
        ├── css
        ├── etc
        ├── js
        ├── lib
        │   └── js
        ├── modules                # --> the "site module" level
        ├── resources
        │   ├── fonts
        │   ├── html
        │   ├── icons
        │   ├── images
        │   ├── l10n
        │   ├── movies
        │   └── text
        └── var
```

These are the folders where you spend all your time developing your next top websites & apps! The comments on the right hand-side give you the location of the available lewp levels and its terms, so you know what the following text is talking about.

## The <img title="lewp logo" src="logo/lewp-transparent-background.inkscape.svg" alt="lewp logo" style="zoom:33%;" width="50"> levels in details

The file hierarchy of lewp has been separated into four levels, that partially repeat themselves. Except the global level, all levels have the following folders in common:

```bash
.
├── css            # --> store your css files here
├── etc            # --> folder for configuration files
├── js             # --> your JavaScript has to go in here
├── lib            # --> put your custom PHP dependencies here
│   └── js         # --> your JavaScript dependencies
├── resources
│   ├── fonts      # --> folder for your fonts
│   ├── html       # --> static HTML files
│   ├── icons      # --> icons that you need
│   ├── images     # --> images that beautify your webpage
│   ├── l10n       # --> your l10n files for different languages live here
│   ├── movies     # --> movie/video storage, if you got some
│   └── txt        # --> static text files
├── vendor         # --> your external dependencies added by your composer.json
└── var            # --> use this to store data during runtime using VarFolder class
```

The purpose of each folder is described in the comments on the right hand-side. In the following section, the specific properties of each level will be explained in detail.

### The "global" level

When talking about the global level, all first level subdirectories in the root folder of your hierarchy are meant. It contains the the global module level ```modules```, the page level ```www```, as well as the ```lib``` and ```resources``` folder.

***Files and modules stored in the global level are shared with every site defined in the page level.*** So you can store your common files for different websites at one place. We will get to the point how to access them soon.

Invoking  ```tree -L 1``` on the global level gives the following output:

```bash
.                                  # --> global level
├── lib
├── modules                        # --> global module level
├── resources
└── www                            # --> page level
```

#### Purposes and recommendations for the different folders

##### lib

Store all your files for the global level in here. They are automatically added by the lewp's integrated autoloader using the [PSR-4](ttps://www.php-fig.org/psr/psr-4/) convention.

##### modules

As said before, this contains all modules that are shared across all websites that are stored in the page level.

##### resources

Store all your resources in here that should be available for all websites that you developed with lewp.

##### www

You will find all websites that you create with lewp in here. The websites are stored in separate folders named by their domain. It is not required to name them by the domain, but recommended to make it easier to find them.

### The "page" level

As mentioned in the section above, the page levels are stored in the ```www``` of the global level. There is a page level for every website that you create. In this case "lewp.org". Again, invoking the first level tree command gives you an overview:

```bash
www/lewp.org/                      # --> page level
├── bin                            # --> root folder of your webserver
├── css                            # --> page specific css code
├── etc                            # --> page specific configurations
├── js                             # --> page specific JavaScript code
├── lib                            # --> page specific models
├── modules                        # --> site module level
├── pages                          # --> page definitions
├── resources                      # --> resources available to the domain
└── var                            # --> workspace for your page
```

It looks pretty similar to the folders of the global level. In here, the ```bin``` and ```pages``` folder have a special meaning. The ```bin```  folder is where your webserver should point its root directory to. This enables maximum security to your application, because only your ```index.php``` as well as ```robots.txt``` is available to the outside.

### The "module" level

*NEEDS TO BE DONE*

# Level hierarchy and replacement strategy

Based on the knowledge about the file hierarchy, it is possible to create a top to bottom replacement strategy that has the following order.

* module level

* page level

* global level

The replacement strategy enables the possibility to specialize all files that are used for the website you are working on. But it also gives you the option to generalize your files so that you can use them on multiple websites. This can be any files, for example images or localization ones.

## Replacement strategy example

Let's say your browser wants to access https://lewp.org/res/images/welcome/logo.svg. When the request is received, Lewp looks for the file in three different locations:

* resources/images folder on the module level

* resources/images folder on the page level

* resources/images folder on the module level

# How <img title="" src="logo/lewp-transparent-background.inkscape.svg" alt="" width="50"> maps routes to your pages

This paragraph describes the mapping between the pages folder and the resulting routes

## Dynamic route parameters

# How <img src="logo/lewp-transparent-background.inkscape.svg" title="" alt="" width="50"> handles module isolation

## CSS

## JavaScript

# Minification and cache of CSS and JavaScript

# Client side scripting

# Docker setup

If you want support this project, you can use the docker setup provided in the ```Docker``` folder. Before you can start developing, you need a lewp project root, provided in the [lewp-template-project](https://gitlab.com/lewp/lewp-template-project). Follow these steps to have a fully working setup:

```bash
# create a folder where you want to develop and change to it
cd <your-desired-lewp-development-folder>
# clone the project folder
git clone --recursive git@gitlab.com:lewp/lewp-template-project.git
# clone the lewp repository
git clone git@gitlab.com:lewp/lewp.git
```

Now that you have both repositories available locally, you need to link the project folder to the docker setup.

```bash
cd lewp/Docker
# Create a symbolic link to the template project
ln -s ../../lewp-template-project dev
```

You are ready to go! Simply start the docker container by

```bash
docker-compose up -d
```

and you can start contributing to lewp.

If you just want to use lewp to create your next web project, use the docker setup from the [lewp-template-project](https://gitlab.com/lewp/lewp-template-project). You will also find the required composer setup in the repository.
