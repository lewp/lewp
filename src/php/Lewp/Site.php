<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\Config;
use Lewp\FileHierarchy;
use Lewp\Resolve;
use Lewp\RequestProcessor;
use Lewp\Session;
use Lewp\Utilities\Sitemap as SitemapProvider;
use Lewp\UrlPaths;
use Lewp\VarFolder;
use \MatthiasMullie\Minify;

/**
 * \brief Representation of a website that uses the page and module class.
 *
 */
class Site
{

    const KEY_URI_PATH_DYNAMIC_PARTS = "dynamic_uri_parts";

    /**
     * \brief Contains the website configuration.
     */
    private $configuration;

    /**
     * \brief Contains the startpage id of the website.
     */
    private $startpage_id;

    /**
     * \brief Request processor instance.
     */
    private $request_processor;

    /**
     * \brief The file hierarchy instance.
     */
    private $file_hierarchy;

    /**
     * \brief List of modules that are prepended to every page.
     */
    private $preceding_modules;

    /**
     * \brief List of modules that are appended to every page.
     */
    private $subsequent_modules;

    /**
     * \brief Contains the language of the site, en per default.
     */
    private $language = 'en';

    /**
     * \brief A lewp session instance.
     */
    private $session;

    public function __construct(
        $top_level_domain,
        $startpage_id,
        $file_hierarchy_root
    ) {
        // register autoloader for lib folder
        spl_autoload_register(function ($class_name) {
            $this->autoloader($class_name);
        });

        $this->file_hierarchy = new FileHierarchy(
            $file_hierarchy_root,
            $top_level_domain
        );

        $this->session = new Session($startpage_id, '');

        $this->preceding_modules = new \SplQueue();
        $this->subsequent_modules = new \SplQueue();
        $this->configuration = new Config($this->file_hierarchy, '', '');

        $this->startpage_id = $startpage_id;
        $this->request_processor = new RequestProcessor(
            $_SERVER['REQUEST_URI'],
            $this->configuration->getValue(Config::KEY_ENABLE_LANGUAGE_URI)
        );
        if ($this->request_processor->getLanguage() !== '') {
            $this->language = $this->request_processor->getLanguage();
            $this->session->set('language', $this->language, true);
        } elseif (!is_null($this->session->get('language', true))) {
            $this->language = $this->session->get('language', true);
        } elseif ($this->configuration->getValue(Config::KEY_LANGUAGE) !== false
        ) {
            $this->language =
                $this->configuration->getValue(Config::KEY_LANGUAGE);
        }
    }

    public function __destruct()
    {
        $this->processRequest();
    }

    /**
     * \brief Automatically loads classes that are stored in the php dependency
     * folders of the file hierarchy.
     *
     * \param string $class_name The class name that is being searched for.
     *
     * \retval bool True if successful.
     */
    final public function autoloader($class_name) : bool
    {
        $class_name = ltrim($class_name, '\\');
        $file_name  = '';
        $namespace = '';
        if ($last_ns_pos = strrpos($class_name, '\\')) {
            $namespace = substr($class_name, 0, $last_ns_pos);
            $class_name = substr($class_name, $last_ns_pos + 1);
            $file_name  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';

        $file_path = $this->file_hierarchy->findFile(
            'phpdep',
            $file_name,
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        if ($file_path !== false) {
            require $file_path;
            return true;
        }
        return false;
    }


    final public function getStartpageId()
    {
        return $this->startpage_id;
    }

    final protected function estimateModuleAndFileIdFromRequest() : array
    {
        $ret = [
            $this->request_processor->getNextRequestPart()
        ];
        if (!$this->request_processor->isEmpty()) {
            array_push($ret, $this->request_processor->getNextRequestPart());
        } else {
            array_unshift($ret, '');
        }
        return $ret;
    }

    final protected function getModule(string $module_id)
    {
        // we need to get the derived class, so we cannot create the instance
        // directly
        $file = $this->file_hierarchy->findModuleDefinition($module_id);
        if ($file === false) {
            return false;
        }
        if (!file_exists($file)) {
            $this->send404();
            return false;
        }
        $module = include $file;
        $module->initialize($this->file_hierarchy);
        return $module;
    }

    final protected function processRequest()
    {
        if ($this->request_processor->isEmpty()) {
            $this->sendPage($this->startpage_id);
        }

        $page_id = Resolve::uriToId($this->request_processor->getFullRequest());
        if ($page_id === $this->startpage_id) {
            $this->redirectToStartpage();
        }

        switch ($this->request_processor->getNextRequestPart()) {
            case UrlPaths::RESOURCES:
                $this->sendResource();
                break;
            case UrlPaths::VAR_FOLDER:
                $this->sendVarFile();
                break;
            case UrlPaths::SITEMAP_TXT:
                $this->sendSitemapTxt();
                break;
            case UrlPaths::SITEMAP_XML:
                $this->sendSitemapXml();
                break;
            case UrlPaths::ROBOTS_TXT:
                $this->sendFile("public-root", UrlPaths::ROBOTS_TXT, "");
            default:
                $this->sendPage($page_id);
                break;
        }
    }

    final public function sendPage($page_id = false)
    {
        $page_id = ($page_id !== false)
            ? $page_id
            : Resolve::uriToId($this->request_processor->getFullRequest());
        [$page_file, $dynamic_parts] = $this->findPageToSend($page_id);
        $page_object = include $page_file;
        $page_object->setLanguage($this->language);
        $page_object->initialize($this->file_hierarchy);
        for ($i = 0; $i < $this->preceding_modules->count(); ++$i) {
            $current = $this->preceding_modules->dequeue();
            $page_object->addModule($current[0], $current[1]);
        }
        $page_object->run([
            self::KEY_URI_PATH_DYNAMIC_PARTS => $dynamic_parts
        ]);
        for ($i = 0; $i < $this->subsequent_modules->count(); ++$i) {
            $current = $this->subsequent_modules->dequeue();
            $page_object->addModule($current[0], $current[1]);
        }
        $page_object->sendHeaders();
        echo $page_object->render();
        exit();
    }

    /**
     * \brief Finds the page definition file that will be sent to the client.
     * If the page id is not found, it will check the next upper level id if it
     * exists and adds the part that has not been found to the dynamic_parts
     * variable.
     *
     * \param string $page_id The page id that should be sent.
     *
     * \retval array An array containing the filepath in the first cell and
     * the dynamic parts in the second cell.
     */
    private function findPageToSend($page_id) : array
    {
        $id_parts = Resolve::idExplode($page_id);
        $reverse_dynamic_parts = [];
        // try to find page without dynamic content
        $page_file = $this->file_hierarchy->findPageDefinition($page_id);
        while ($page_file === false) {
            // prepare dynamic parts
            $reverse_dynamic_parts[] = array_pop($id_parts);
            $page_id = Resolve::arrayToId($id_parts);
            if (empty($id_parts)) {
                //nothing left to process
                $this->send404();
            }
            // find page
            $page_file = $this->file_hierarchy->findPageDefinition($page_id);
        }
        return [
            $page_file,
            array_reverse($reverse_dynamic_parts)
        ];
    }

    final protected function sendResource()
    {
        $part = $this->request_processor->getNextRequestPart();
        switch ($part) {
            case UrlPaths::AJAX:
                $this->processAjax();
                break;
            case UrlPaths::AJAX_JSON:
                $this->processAjaxJson();
                break;
            case UrlPaths::CSS:
                $this->sendCss();
                break;
            case UrlPaths::JAVASCRIPT:
                $this->sendJavascript();
                break;
            case UrlPaths::JAVASCRIPT_DEPENDENCY:
                $this->sendJavascriptDependency();
                break;
            case UrlPaths::MODULE:
                $this->sendModuleHtml();
                break;
            case UrlPaths::MODULE_FOOTPRINT:
                $this->sendFootprint();
                break;
            case UrlPaths::FONTS:
            case UrlPaths::IMAGES:
            case UrlPaths::ICONS:
            case UrlPaths::MOVIES:
            default:
                $this->sendResourceFile($part);
                $this->send404();
                break;
        }
    }

    final protected function send404()
    {
        header("HTTP/1.1 404 Not Found");
        exit();
    }

    final protected function sendCss()
    {
        $id = $this->request_processor->getNextRequestPart();
        switch ($id) {
            case UrlPaths::PAGE:
                $this->sendPageCss();
                break;
            case UrlPaths::SITE:
                $this->sendSiteCss();
                break;
            case UrlPaths::CSS_NON_RENDER_CRITICAL:
                $this->sendNonRenderCriticalCss();
                break;
            default:
                $module = $this->getModule($id);
                if (!$module->hasCss()) {
                    $this->send404();
                    return;
                }
                header('Content-type: text/css');
                header('Vary: Accept-Encoding');
                echo (new Minify\CSS($module->getCss()))->minify();
                exit();
            break;
        }
    }

    final protected function sendJavascript()
    {
        $id = $this->request_processor->getNextRequestPart();
        switch ($id) {
            case UrlPaths::LEWP_JAVASCRIPT:
                $this->sendLewpJavascript();
                break;
            case UrlPaths::PAGE:
                $this->sendPageJavascript();
                break;
            default:
                $module = $this->getModule($id);
                header('Content-type: application/javascript');
                header('Vary: Accept-Encoding');
                echo $module->getJavascript();
                exit();
            break;
        }
    }

    final protected function sendLewpJavascript()
    {
        $path = realpath(Resolve::toFilepath([
            __DIR__,
            "..",
            "..",
            "js",
            "lewp.js"
        ]));
        if ($path === false) {
            $this->send404();
        }
        header('Content-type: application/javascript');
        header('Vary: Accept-Encoding');
        echo file_get_contents($path) . $this->lewpJavascriptConfig();



        // development addition
        $path = realpath(Resolve::toFilepath([
            __DIR__,
            "..",
            "..",
            "js",
            "LewpFootprintCollection.js"
        ]));
        echo file_get_contents($path);
        $path = realpath(Resolve::toFilepath([
            __DIR__,
            "..",
            "..",
            "js",
            "LewpModuleCollection.js"
        ]));
        echo file_get_contents($path);
        $path = realpath(Resolve::toFilepath([
            __DIR__,
            "..",
            "..",
            "js",
            "LewpModule.js"
        ]));
        echo file_get_contents($path);
        exit();
    }

    private function lewpJavascriptConfig() : string
    {
        return
            "Lewp.prototype.config = {"
            . "languageUriEnabled:"
            . (
                ($this->configuration->getValue(Config::KEY_ENABLE_LANGUAGE_URI))
                ? 'true'
                : 'false'
            )
            . "};";
    }

    final protected function sendPageJavascript()
    {
        $page_id = $this->request_processor->getNextRequestPart();
        $file = $this->file_hierarchy->getFile(
            'js',
            $page_id . '.js',
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        if ($file === false) {
            $this->send404();
        }
        header('Content-type: application/javascript');
        header('Vary: Accept-Encoding');
        echo $file;
        exit();
    }

    final protected function sendJavascriptDependency()
    {
        [$module_id, $file_id] = $this->estimateModuleAndFileIdFromRequest();
        $dependency = $this->file_hierarchy->getFile(
            'jsdep',
            $file_id . '.js',
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        if ($dependency === false) {
            $this->send404();
        }
        header('Content-type: application/javascript');
        header('Vary: Accept-Encoding');
        echo $dependency;
        exit();
    }

    final protected function sendFootprint()
    {
        $module_id = $this->request_processor->getNextRequestPart();
        $module = $this->getModule($module_id);
        header('Content-type: application/json');
        header('Vary: Accept-Encoding');
        if ($module === false) {
            echo false;
            exit();
        }
        echo $module->getFootprint();
        exit();
    }

    final protected function sendPageCss()
    {
        $page_id = $this->request_processor->getNextRequestPart();
        $file = $this->file_hierarchy->getFile(
            'css',
            $page_id . '.css',
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        if ($file === false) {
            $this->send404();
        }
        header('Content-type: text/css');
        header('Vary: Accept-Encoding');
        // TODO: use css processor for compression and generated file in var folder
        echo (new Minify\CSS($file))->minify();
        exit();
    }

    final protected function sendSiteCss()
    {
        $path = $this->file_hierarchy->generatePath(
            new FileHierarchy\SiteLevel($this->file_hierarchy->getTLD()),
            'css'
        );
        $filepath = Resolve::toFilepath([$path, "site.css"]);
        if (!file_exists($filepath)) {
            $this->send404();
        }
        header('Content-type: text/css');
        header('Vary: Accept-Encoding');
        // TODO: use css processor for compression and generated file in var folder
        echo (new Minify\CSS($filepath))->minify();
        exit();
    }

    final protected function sendNonRenderCriticalCss()
    {
        $id = $this->request_processor->getNextRequestPart();
        $module = $this->getModule($id);
        if (!$module->hasCss()) {
            $this->send404();
            return;
        }
        header('Content-type: text/css');
        header('Vary: Accept-Encoding');
        echo $module->getNonRenderCriticalCss();
        exit();
    }

    final protected function sendModuleHtml()
    {
        $module_id = $this->request_processor->getNextRequestPart();
        $page_id = $this->request_processor->getNextRequestPart();
        if ($page_id === '') { // send startpage content
            $page_id = $this->startpage_id;
        }
        $module = $this->getModule($module_id);
        $module->setPageId($page_id);
        $module->setLanguage($this->language);
        header('Content-type: text/html');
        header('Vary: Accept-Encoding');
        $module->run();
        echo $module->render();
        exit();
    }

    final protected function sendResourceFile(string $file_type)
    {
        [$module_id, $file_id] = $this->estimateModuleAndFileIdFromRequest();
        $success = $this->sendFile(
            'res-' . $file_type,
            Resolve::idToFilepathRel($file_id),
            $module_id
        );
        if (!$success) {
            $this->sendFile(
                'res',
                Resolve::idToFilepathRel($file_type),
                $module_id
            );
        }
        $this->send404();
    }

    final protected function sendFile(string $folder_id, string $file_name, string $module_id) : bool
    {
        $file = $this->file_hierarchy->findFile(
            $folder_id,
            Resolve::idToFilepathRel($file_name),
            $this->file_hierarchy->factoryLevelArray([], $module_id)
        );
        if ($file === false) {
            return false;
        }
        header('Content-type: ' . mime_content_type($file));
        header('Vary: Accept-Encoding');
        header('Cache-Control: max-age=86400');
        echo file_get_contents($file);
        exit();
    }

    final protected function processAjaxJson()
    {
        [$module_id, $file_id] = $this->estimateModuleAndFileIdFromRequest();
        $fileparts = pathinfo($file_id);
        if ($fileparts['extension'] !== 'php') {
            $file_id .= '.php';
        }
        $file_name = $this->file_hierarchy->findFile(
            'ajax',
            $file_id,
            $this->file_hierarchy->factoryLevelArray([], $module_id)
        );
        if ($file_name === false) {
            $this->send404();
        }
        $object = include $file_name;
        $object->sendResponse();
        exit();
    }

    final protected function processAjax()
    {
        [$module_id, $file_id] = $this->estimateModuleAndFileIdFromRequest();
        $fileparts = pathinfo($file_id);
        if ($fileparts['extension'] !== 'php') {
            $file_id .= '.php';
        }
        $file_name = $this->file_hierarchy->findFile(
            'ajax',
            $file_id,
            $this->file_hierarchy->factoryLevelArray([], $module_id)
        );
        if ($file_name === false) {
            $this->send404();
        }
        include $file_name;
        exit();
    }

    final protected function sendVarFile()
    {
        [$module_id, $file_id] = $this->estimateModuleAndFileIdFromRequest();
        $var_folder = new VarFolder($this->file_hierarchy, $module_id);
        if (!$var_folder->isPublic($file_id)) {
            $this->send404();
        }
        $success = $this->sendFile(
            'var',
            Resolve::idToFilepathRel($file_id),
            $module_id
        );
        if (!$success) {
            $this->send404();
        }
    }

    final protected function sendSitemapTxt()
    {
        $sitemap = new SitemapProvider($this->file_hierarchy, $this->startpage_id);
        echo $sitemap->getSitemapTxt();
        exit();
    }

    final protected function sendSitemapXml()
    {
        header('Content-type: application/xml');
        header('Vary: Accept-Encoding');
        $sitemap = new SitemapProvider($this->file_hierarchy, $this->startpage_id);
        echo $sitemap->getSitemapXml();
        exit();
    }

    final protected function redirectToStartpage()
    {
        header('Status: 301 Moved Permanently', false, 301);
        $protocol = ($this->configuration->getValue(Config::KEY_HTTPS))
            ? "https"
            : "http";
        header('Location: /');
        exit();
    }

    final public function prependModule(string $module_id, array $options = [])
    {
        $this->preceding_modules->enqueue([$module_id, $options]);
    }

    final public function appendModule(string $module_id, array $options = [])
    {
        $this->subsequent_modules->enqueue([$module_id, $options]);
    }
}
