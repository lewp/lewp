<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

/**
 * \brief Custom error handler for lewp.
 *
 */
class ErrorHandler
{

    /**
     * \brief Determines which errors/warnings/hints are shown.
     * If set to 2: Errors, Warnings and Hints.
     * If set to 1: Errors, Warnings.
     * If set to 0: Errors.
     */
    const ERROR_SHOW_LEVEL = 2;

    public static function initialize()
    {
        $error_handler = new ErrorHandler();
        set_error_handler([$error_handler, "createMessage"]);
    }

    /**
     * \brief Creates the wrapper including its style for the message that will
     * be shown. If $errno is E_USER_ERROR, the die() function will be called
     * afterwards.
     */
    public function createMessage($errno, $errstr)
    {
        if (!in_array($errno, [E_USER_WARNING, E_USER_NOTICE, E_USER_ERROR])) {
            return;
        }
        switch ($errno) {
            case E_USER_ERROR:
                $span_text = 'LEWP ERROR:';
                break;
            case E_USER_WARNING:
                if (self::ERROR_SHOW_LEVEL < 1) {
                    return;
                }
                $span_text = 'LEWP WARNING:';
                break;
            default:
            case E_USER_NOTICE:
                if (self::ERROR_SHOW_LEVEL < 2) {
                    return;
                }
                $span_text = 'LEWP NOTICE:';
                break;
        }
        $dom = new \DOMDocument();
        $div = $dom->createElement('div');
        $span = $dom->createElement('span', $span_text);
        $this->setStandardStyle($div, $errno);
        $style = "font-weight: bold;" .
            "font-style: italic;" .
            "padding-right: 10px;";
        $span->setAttribute("style", $style);
        $span_message = $dom->createElement('span', $errstr);
        $div->appendChild($span);
        $div->appendChild($span_message);
        $dom->appendChild($div);
        echo $dom->saveHTML();
        if ($errno === E_USER_ERROR) {
            die();
        }
    }

    /**
     * \brief Sets the style of the box that wraps the message.
     */
    private function setStandardStyle(&$dom_node, $errno)
    {
        $style = "padding: 10px;" .
            "margin: 10px;" .
            "border: medium solid ";
        switch ($errno) {
            case E_USER_ERROR:
                $style .= '#D32121';
                break;
            case E_USER_WARNING:
                $style .= '#65E055';
                break;
            default:
            case E_USER_NOTICE:
                $style .= '#0E08F3';
                break;
        }
        $style .= ";background: #D3D3D3;";
        $dom_node->setAttribute("style", $style);
    }
}
