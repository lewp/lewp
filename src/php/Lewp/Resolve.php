<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

/**
 * \brief Implements conventions that are used within lewp.
 */
class Resolve
{

    const ID_SEPARATOR = '_';

    /**
     * \brief Converts the given id to a relative filepath using the convention.
     *
     * Replaces all dashes with a DIRECTORY_SEPARATOR constant.
     *
     * \param string $id The id to be converted.
     *
     * \retval string The converted filepath.
     */
    public static function idToFilepathRel($id)
    {
        $id = str_replace('..', '', $id);
        return str_replace(self::ID_SEPARATOR, DIRECTORY_SEPARATOR, $id);
    }

    /**
     * \brief Converts the given relative filepath to an id using the lewp convention.
     *
     * Replaces all DIRECTORY_SEPARATOR with dashes.
     *
     * \param string $relpath The relative filepath to be converted.
     *
     * \retval string The converted id.
     */
    public static function filepathRelToId($relpath)
    {
        $id = str_replace('..', '', $relpath);
        return str_replace(DIRECTORY_SEPARATOR, self::ID_SEPARATOR, $relpath);
    }

    /**
     * \brief Converts the given id to a valid URI part.
     *
     * Replaces all dashes with a slash.
     *
     * \param string $id The id to be converted.
     *
     * \retval string The converted URI.
     */
    public static function idToUri($id)
    {
        $id = str_replace('..', '', $id);
        return str_replace(self::ID_SEPARATOR, '/', $id);
    }

    /**
     * \brief Converts the given URI part to an id using the lewp convention.
     *
     * Replaces all slashes with dashes.
     *
     * \param string $relpath The URI part to be converted.
     *
     * \retval string The converted id.
     */
    public static function uriToId($uri)
    {
        $id = str_replace('..', '', $uri);
        return str_replace('/', self::ID_SEPARATOR, $uri);
    }

    /**
     * \brief Finds the folder id of the given file id.
     * Uses idToFilepathRel and converts all slashes by dashes again.
     *
     * \param string $id The id for what the folder is being looked for.
     *
     * \retval string Folder id of the given $id.
     */
    public static function findFolderId($id)
    {
        $pathinfo = pathinfo(self::idToFilepathRel($id));
        return str_replace('/', self::ID_SEPARATOR, $pathinfo['dirname']);
    }

    /**
     * \brief Implodes the given array to a filepath using the
     * DIRECTORY_SEPARATOR constant.
     *
     * \param array $names The path values.
     *
     * \retval string The imploded string/filepath.
     */
    public static function toFilepath(array $names)
    {
        return implode(DIRECTORY_SEPARATOR, array_filter($names));
    }

    /**
     * \brief Explodes the given string by dashes.
     *
     * Used to process for example page ids in modules.
     *
     * \param string $id The id to explode.
     *
     * \retval array Containing all values of the id.
     */
    public static function idExplode($id)
    {
        return explode(self::ID_SEPARATOR, $id);
    }

    /**
     * \brief Implodes the given array by dashes.
     *
     * \param array $array The array to implode.
     *
     * \retval string String containg the resulting id.
     */
    public static function arrayToId($array)
    {
        return implode(self::ID_SEPARATOR, $array);
    }

    /**
     * \brief Converts the given id to its JavaScript convention equivalent.
     *
     * \param string $id The id to convert.
     *
     * \retval string The converted id.
     */
    public static function idToJavascript($id)
    {
        $id = strtolower($id);
        $separator = '-';
        $pos = strpos($id, $separator);
        while ($pos !== false) {
            $id[++$pos] = strtoupper($id[$pos]);
            $pos = strpos($id, $separator, $pos);
        }
        return str_replace($separator, '', $id);
    }

    /**
     * \brief Finds the parent id of the given one.
     *
     * \param string $id The id to be processed.
     *
     * \retval string The found parent id. If no parent id is present,
     * an empty string is being returned.
     */
    public static function toParentId($id)
    {
        $arr = explode(self::ID_SEPARATOR, $id);
        array_splice($arr, sizeof($arr) - 1, 1);
        return implode(self::ID_SEPARATOR, $arr);
    }

    /**
     * \brief Replaces DIRECTORY_SEPARATOR and dots with dashes in the given string.
     *
     * This function is used to convert domain and module names to the css class
     * naming convention.
     *
     * \param string $string String to be edited.
     *
     * \retval string Input string with replaced characters.
     *
     */
    public static function stringToCssClassName($string)
    {
        $string = str_replace(
            [DIRECTORY_SEPARATOR, '.', Resolve::ID_SEPARATOR],
            "-",
            $string
        );
        // test if last char is minus
        $string = ($string[strlen($string) - 1] === "-")
            ? substr($string, 0, strlen($string) - 1)
            : $string;
        // test if first char is minus
        $string = ($string[0] === "-")
            ? substr($string, 1, strlen($string))
            : $string;

        return $string;
    }
}
