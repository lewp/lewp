<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Interfaces;

interface IFileHierarchy
{

    /**
     * \brief True if the FileHierarchy has been initialized and all folders are
     * available.
     */
    public function isInitialized() : bool;

    /**
     * \brief Returns the top level domain of the file hierarchy.
     */
    public function getTLD() : string;

    /**
     * \brief Returns the filepath to the root folder.
     */
    public function getRootFolder() : string;

    /**
     * \brief Generates the absolute folder path to the given id on the given
     * file hierarchy level.
     *
     * \param ALevel $level The level object to be used.
     * \param String $relative_id The relative path id.
     *
     * \retval String/Bool The absolute folder path. False if not found.
     *
     */
    public function generatePath(\Lewp\FileHierarchy\ALevel $level, $relative_id);

    /**
     * \brief Creates a folder in the file hierarchy.
     *
     * \param ALevel $level The level object to be used.
     * \param String $relative_id The relative path id.
     *
     * \retval String/Bool The absolute folder path. False if the folder was not created.
     *
     */
    public function createFolder(\Lewp\FileHierarchy\ALevel $level, $relative_id);

    /**
     * \brief Searches for the module definition file.
     *
     * \retval mixed If found, it returns the absolute filepath as string.
     * False otherwise.
     */
    public function findModuleDefinition(string $module_id);

    /**
     * \brief Searches for the page definition file.
     *
     * \retval mixed If found, it returns the absolute filepath as string.
     * False otherwise.
     */
    public function findPageDefinition(string $page_id);

    /**
     * \brief Searches for the given file in the FileHierarchy.
     *
     * \param string $folder_id The level specific folder id. It can either be
     * separated using the ID_SEPARATOR or a dash. The folders are defined in
     * the Lewp\FileHierarchy namespace.
     * \param string $file_name The relative file name. It can either be written
     * using the OS specific DIRECTORY_SEPARATOR, or as a lewp id using
     * the ID_SEPARATOR provided by the Resolve class.
     * \param array $levels (optional) The levels that should be searched for the file.
     *
     * \retval mixed If found, it returns the absolute filepath as string.
     * False otherwise.
     */
    public function findFile(string $folder_id, string $file_name, array $levels);

    /**
     * \brief Returns the content of the found file in the FileHierarchy.
     * If a relative filename is given, the conversion of the file id can be
     * turned off by the $convert_file_id parameter.
     *
     * \param string $folder_id The level specific folder id.
     * \param string $file_id The relative file id.
     * \param array $levels The levels in which the file should be looked for.
     * \param bool $convert_file_id True if the id should be converted to a filename.
     *
     * \retval mixed If found, it returns the content as string.
     * False otherwise.
     */
    public function getFile(string $folder_id, string $file_id, array $levels, bool $convert_file_id = true);

    /**
     * \brief Searches the given file and returns its sha1 checksum.
     *
     * \param string $folder_id The folder id.
     * \param string $file_id The file id.
     * \param array $levels The levels in which the file should be looked for.
     *
     * \retval mixed False if file has not been found. Otherwise sha1 checksum
     * as string.
     */
    public function getSha1(string $folder_id, string $file_id, array $levels);

    /**
     * \brief Returns an array containing the module folders that are available
     * at site and global level for the given folder id.
     *
     * \param string $module_id The module id.
     * \param string $folder_id The folder id that is being searched for.
     *
     * \retval array The array containing the absolute folder paths or false if
     * non-existent.
     */
    public function findFolderPaths(string $module_id, string $folder_id): array;

    /**
     * \brief Determines all file ids in the given resource folder. If a
     * subfolder of the resource folder should be crawled, it can be given by
     * the folder id parameter.
     *
     * \param string $module_id The module id.
     * \param string $resource_id The resource folder id as it is specified in
     *                            the file hierarchy.
     * \param string $folder_id The subfolder of the resource folder.
     * \param string $pattern The pattern of the files such as '*.jpg'. Defaults to '*'.
     */
    public function collectResourceFileIds(
        string $module_id,
        string $resource_id,
        string $folder_id,
        string $pattern = '*'
    ): array;

    /**
     * \brief Collects all css filenames that belong to the module. Preferring
     * specific folder. Sorts the resulting array by name.
     *
     * \param string $module_id The module id.
     *
     * \retval array Returns the css filenames, an empty array otherwise.
     */
    public function collectCssFilenames(string $module_id): array;

    /**
     * \brief Collects all js filenames that belong to the module. Preferring
     * specific folder. Sorts the resulting array by name.
     *
     * \param string $module_id The module id.
     *
     * \retval array Returns the js filenames, an empty array otherwise.
     */
    public function collectJavascriptFilenames(string $module_id): array;

    /**
     * \brief Loads a textfile from the resource directory and returns it as
     * string. The extension of the text files is always .txt. If the given
     * id does not contain the extension, it will be appended.
     *
     * \param string $module_id The module id.
     * \param string $file_id The file id relative to the resource directory.
     * The extension will always be .txt. If not given in the id, it gets appended.
     *
     * \retval string The loaded string from the text file. An empty string if file
     * has not been found.
     */
    public function loadTextFile(string $module_id, string $file_id): string;

    /**
     * \brief Loads a JSON from the resource directory and returns it as
     * associative array. The extension of the text files is always .json.
     * If not included in the id, it will be appended.
     *
     * \param string $module_id The module id.
     * \param string $file_id The file id relative to the resource directory.
     * The extension will always be .json.
     *
     * \retval array/bool The loaded JSON from the given file. False if file
     * has not been found.
     */
    public function loadJsonFile(string $module_id, string $file_id, bool $asArray = true);

    /**
     * \brief Loads a file from the resource directory and returns its content
     * as string.
     *
     * \param string $module_id The module id.
     * \param string $file_id The file id relative to the resource directory.
     *
     * \retval string The loaded string from the text file. An empty string if not found.
     */
    public function loadResourceFile(string $module_id, string $file_id): string;

    /**
     * \brief Loads the given HTML file from the resource directory.
     *
     * \param string $module_id The module id.
     * \param string $file_id The HTML file id. The extension will always be
     * .html. Will be appended if not included in the id,.
     *
     * \retval mixed False on fail. Otherwise a string containing the loaded html file.
     */
    public function loadHtmlFile(string $module_id, string $file_id);

    /**
     * \brief Loads a localization file from the resource directory and returns it as
     * string. The extension will not be corrected, so it needs to be included.
     *
     * \param string $module_id The module id.
     * \param string $file_id The file id relative to the resource directory.
     *
     * \retval string The loaded string from the text file. An empty string if file
     * has not been found.
     */
    public function loadL10nFile(string $module_id, string $file_id);

    public function getL10n(string $module_id, string $language, string $key, string $default): string;
}
