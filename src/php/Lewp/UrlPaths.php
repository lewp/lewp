<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

/**
 * \brief Contains all reserved pathnames for a lewp website.
 *
 */
class UrlPaths
{

    const LEWP_JAVASCRIPT = 'lewp';
    const MODULE = 'module';
    const PAGE = 'page';
    const SITE = 'site';
    const RESOURCES = 'res';
    const JAVASCRIPT_DEPENDENCY = 'jsdep';
    const CSS = 'css';
    const CSS_NON_RENDER_CRITICAL = 'non-render-critical';
    const CSS_RENDER_CRITICAL = 'render-critical';
    const JAVASCRIPT = 'js';
    const MODULE_FOOTPRINT = 'footprint';
    const FONTS = 'fonts';
    const ICONS = 'icons';
    const IMAGES = 'images';
    const MOVIES = 'movies';
    const AJAX = 'ajax';
    const AJAX_JSON = 'ajax-json';
    const VAR_FOLDER = 'var';
    const SITEMAP_TXT = 'sitemap.txt';
    const SITEMAP_XML = 'sitemap.xml';
    const ROBOTS_TXT = 'robots.txt';

    public static function implodeToAbsolute(array $paths)
    {
        return "/" . implode("/", $paths);
    }
}
