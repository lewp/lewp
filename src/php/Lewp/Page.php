<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\Config;
use Lewp\Dependencies;
use Lewp\Html5DomDocument;
use Lewp\FileHierarchy;
use Lewp\FileHierarchy\SiteLevel;
use Lewp\ModuleRegister;
use Lewp\Resolve;
use Lewp\UrlPaths;
use \MatthiasMullie\Minify;

/**
 * \brief Representation of a page that is used in a website.
 *
 */
abstract class Page extends Html5DomDocument
{

    /**
     * \brief The id of the element that is wrapping the page.
     */
    const HTML_WRAPPER_ID = "pagewrapper";

    /**
     * \brief Definition of the document type that will be added to the beginning
     * of the HTML page.
     */
    const HTML_DOCUMENT_TYPE = "<!DOCTYPE HTML>";

    /**
     * \brief Key that is passed to the module options on calling, defining
     * the amount how often the module has been called already.
     */
    const MODULE_OPTION_EXECUTION_COUNT = "execution_count";

    /**
     * \brief The page id.
     */
    private $page_id;

    /**
     * \brief Contains the module objects that are on the page in order
     * they have been added.
     */
    private $modules = [];

    /**
     * \brief The options that belong to the modules that are in the $modules
     * variable.
     */
    private $modules_options = [];

    /**
     * \brief Register that holds how often the module has been called already.
     */
    private $module_execution_count = [];

    /**
     * \brief The absolute filename to the page css file. False if if does not exist.
     */
    private $css_file_name;

    /**
     * \brief The absolute filename to the page css file. False if if does not exist.
     */
    private $js_file_name;

    /**
     * \brief The html element.
     */
    private $html_element;

    /**
     * \brief The head element.
     */
    private $head_element;

    /**
     * \brief The body element.
     */
    private $body_element;

    /**
     * \brief The page tag (the wrapping div around all modules).
     */
    private $tag_page;

    /**
     * \brief Contains all elements that will be prepended to the head tag.
     */
    private $head_prepend_elements = [];

    /**
     * \brief Contains all elements that will be appended to the head tag.
     */
    private $head_append_elements = [];

    /**
     * \brief Contains the page configuration, defined in the configuration
     * file.
     */
    private $configuration = null;

    /**
     * \brief Holds the language of the page as two character string.
     */
    private $language = 'en';

    /**
     * \brief The file hierarchy instance.
     */
    protected $file_hierarchy;

    private $session;

    private $module_register;

    public function __construct()
    {
        parent::__construct();
    }

    final public function __invoke(string $key, string $default): string
    {
        $language = ($this->getLanguage() !== '') ? $this->getLanguage() : 'en';
        return $this->file_hierarchy->getL10n('', $language, $key, $default);
    }

    public function initialize(FileHierarchy $file_hierarchy)
    {
        $this->file_hierarchy = $file_hierarchy;
        $this->page_id = $this->resolvePageId();
        $this->session = new Session($this->page_id, '');
        $this->configuration = new Config($this->file_hierarchy, $this->page_id);
        $this->initializePage();

        // create module register
        $this->module_register = new ModuleRegister(
            $this->file_hierarchy,
            $this->page_id,
            $this->language
        );

        // check if css and js is available
        $this->css_file_name = $this->file_hierarchy->findFile(
            'css',
            $this->page_id . '.css',
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        $this->js_file_name = $this->file_hierarchy->findFile(
            'js',
            $this->page_id . '.js',
            $this->file_hierarchy->factoryLevelArray([], '')
        );
    }

    /**
     * \brief Using debug_backtrace, the page id is being estimated. If the base
     * path (folder where pages are stored) is not present, an error will be
     * thrown.
     *
     * \retval string The page id that has been estimated.
     */
    private function resolvePageId() : string
    {
        // get filename of calling file
        $file_name = (new \ReflectionObject($this))->getFileName();
        // get base filepath to the pages folder
        $base_path = $this->file_hierarchy->generatePath(
            new SiteLevel($this->file_hierarchy->getTLD()),
            'pages'
        );
        if (strpos($file_name, $base_path) !== 0) {
            trigger_error(
                'Cannot resolve page id. Please check your setup!',
                E_USER_ERROR
            );
        }
        $max_len = strlen($file_name) - strlen($base_path) - 5;
        return Resolve::filepathRelToId(
            substr($file_name, strlen($base_path) + 1, $max_len)
        );
    }

    /**
     * \brief Initializes the basic elements such as html, head and body tag.
     */
    private function initializePage()
    {
        // create all basic elements
        $this->html_element = $this->createElement('html');
        $this->head_element = $this->createElement('head');
        $this->body_element = $this->createElement('body');
        if (empty($this->configuration->getValue(Config::KEY_DISABLE_PAGE_WRAPPER))) {
            $this->body_element->appendChild($this->createAndSetupElement(
                $this->configuration->getValue(Config::KEY_WRAPPER_MODULE_ELEMENT_TYPE),
                '',
                ["id" => self::HTML_WRAPPER_ID]
            ));
        }
    }

    /**
     * \brief Shorthand function to createElement and setAttribute afterwards.
     *
     * \param string $tag_name The name of the tag that will bei created.
     * \param string $initial_text The initial nodeValue.
     * \param array $attributes_values A list of key value pairs where the key
     * the resulting attribute and the value the value of it.
     *
     * \retval \DOMElement The created element.
     */
    protected function createAndSetupElement($tag_name, $initial_text, $attributes_values = [])
    {
        return $this->createElement($tag_name, $initial_text, $attributes_values);
    }

    /**
     * \brief Returns the page css. Defaults to an empty string if not available.
     *
     * \retval string The page css. If not available, an empty string will be returned.
     */
    final public function getCss() : string
    {
        if (empty($this->css_file_name)
            || !is_file($this->css_file_name)
        ) {
            return '';
        }
        return file_get_contents($this->css_file_name);
    }

    /**
     * \brief If the page css file is available, this will return true.
     *
     * \retval bool True if page css exists.
     */
    final public function hasCss() : bool
    {
        return (empty($this->css_file_name) || !is_file($this->css_file_name))
            ? false
            : true;
    }

    /**
     * \brief Returns the page js. Defaults to an empty string if not available.
     *
     * \retval string The page js. If not available, an empty string will be returned.
     */
    final public function getJavascript() : string
    {
        if (empty($this->js_file_name)
            || !is_file($this->js_file_name)
        ) {
            return '';
        }
        return file_get_contents($this->js_file_name);
    }

    /**
     * \brief If the page js file is available, this will return true.
     *
     * \retval bool True if page js exists.
     */
    final public function hasJavascript() : bool
    {
        return (empty($this->js_file_name) || !is_file($this->js_file_name))
            ? false
            : true;
    }

    /**
     * \brief Adds the given element to the list that will be prepended to the
     * head tag on rendering.
     */
    final public function prependToHeadTag($element)
    {
        $this->head_prepend_elements[] = $element;
    }

    /**
     * \brief Adds the given element to the list that will be appended to the
     * head tag on rendering.
     */
    final public function appendToHeadTag($element)
    {
        $this->head_append_elements[] = $element;
    }

    /**
     * \brief Adds the given module and the corresponding options to the page.
     *
     * \param string $module_id The module id of the module to be added.
     * \param array $options The module options.
     *
     * \retval bool True if the module has been added successfully.
     */
    final public function addModule(string $module_id, array $options = []) : bool
    {

        $this->module_register->register($module_id);

        $this->modules[] = $module_id;

        //$this->modules[] = include $module_file_path;
        $this->modules_options[] = array_merge(
            $options,
            [
                self::MODULE_OPTION_EXECUTION_COUNT =>
                $this->module_execution_count[$module_id]
            ]
        );
        return true;
    }

    /**
     * \brief Sends the page headers to the client.
     */
    final public function sendHeaders()
    {
        // send header with encoding
        header('Content-Type: text/html; charset=' . $this->configuration->getValue('encoding'));

        // send content security policy
        header('Content-Security-Policy: ' . $this->configuration->getValue('csp_header'));

        // send security related headers
        header('X-Content-Type-Options: nosniff');
        header('X-XSS-Protection: 1; mode=block');
        header('X-Frame-Options: SAMEORIGIN');
        header('Referrer-Policy: no-referrer');
    }

    /**
     * \brief Renders the page and returns the resulting HTML code.
     *
     * \retval string The resulting HTML code after rendering.
     */
    final public function render() : string
    {
        //TODO: Add noscript elements for non-render-critical css

        $this->head_element->appendChild($this->createAndSetupElement(
            'meta',
            '',
            ['charset' => $this->configuration->getValue('encoding')]
        ));
        // prepend elements to head tag
        for ($i = 0; $i < sizeof($this->head_prepend_elements); ++$i) {
            $this->head_element->appendChild($this->head_prepend_elements[$i]);
        }

        // set page title
        $this->head_element->appendChild(
            $this->createAndSetupElement('title', $this->configuration->getValue("title"))
        );

        // add viewport if enabled
        if ($this->configuration->getValue(Config::KEY_ADD_VIEWPORT) === true) {
            $this->head_element->appendChild(
                $this->createAndSetupElement(
                    'meta',
                    '',
                    [
                        "name" => "viewport",
                        "content" => 'width=device-width, initial-scale=1.0, user-scalable=no'
                    ]
                )
            );
        }

        // add favicon
        $this->addFaviconTag();

        // CSS TAGS
        $this->setupCss();

        // add lewp namespace initialization
        $this->head_element->appendChild($this->createAndSetupElement(
            'script',
            '',
            [
                'src' => UrlPaths::implodeToAbsolute(
                    [
                        UrlPaths::RESOURCES,
                        UrlPaths::JAVASCRIPT,
                        UrlPaths::LEWP_JAVASCRIPT
                    ]
                )
            ]
        ));

        // add dependencies for all modules
        $this->appendModuleDependencies();

        // JS TAGS
        $this->addJavascriptTags();

        // append elements to head tag
        for ($i = 0; $i < sizeof($this->head_append_elements); ++$i) {
            $this->head_element->appendChild($this->head_append_elements[$i]);
        }

        // set language if configured
        if (!empty($this->configuration->getValue(Config::KEY_LANGUAGE))) {
            $this->html_element->setAttribute(
                'lang',
                $this->configuration->getValue(Config::KEY_LANGUAGE)
            );
        } else {
            $this->html_element->setAttribute('lang', $this->language);
        }

        $this->runAndAppendModulesHtml();

        // add all major elements to the DOMDocument
        //$this->body_element->appendChild($this->tag_page);
        $this->html_element->appendChild($this->head_element);
        $this->html_element->appendChild($this->body_element);
        $this->appendChild($this->html_element);

        return self::HTML_DOCUMENT_TYPE . $this->saveHTML();
    }

    /**
     * \brief Gets the site css.
     *
     * \retval string Empty string if the site css is not available.
     */
    final protected function getSiteCss() : string
    {
        $path = $this->file_hierarchy->generatePath(
            new SiteLevel($this->file_hierarchy->getTLD()),
            'css'
        );
        $filepath = Resolve::toFilepath([$path, "site.css"]);
        if (!file_exists($filepath)) {
            return '';
        }
        return (new Minify\CSS($filepath))->minify();
    }

    /**
     * \brief Gets the page css.
     *
     * \retval string Empty string if the page css is not available.
     */
    final protected function getPageCss() : string
    {
        if (!$this->hasCss()) {
            return '';
        }
        $path = $this->file_hierarchy->generatePath(
            new SiteLevel($this->file_hierarchy->getTLD()),
            'css'
        );
        $filepath = Resolve::toFilepath([$path, $this->getPageId() . ".css"]);
        if (!file_exists($filepath)) {
            return '';
        }
        return (new Minify\CSS($filepath))->minify();
    }

    /**
     * \brief Adds all link tags containing the css of the modules to the page.
     *
     * \retval bool Always true.
     */
    final protected function addModulesCss() : bool
    {
        for ($i = 0; $i < sizeof($this->modules); ++$i) {
            $module_instance = $this->module_register->get($this->modules[$i]);
            if (!$module_instance->hasCss()) {
                continue;
            }
            $this->head_element->appendChild($this->createAndSetupElement(
                'link',
                '',
                [
                    "rel" => "stylesheet",
                    "type" => "text/css",
                    "href" => UrlPaths::implodeToAbsolute(
                        [
                            UrlPaths::RESOURCES,
                            UrlPaths::CSS,
                            $this->modules[$i]
                        ]
                    )
                ]
            ));
        }
        return true;
    }

    final protected function setupCss() : bool
    {
        $module_dependencies = [];
        $processed_module_ids = [];

        $inlined_css = $this->getSiteCss() . $this->getPageCss();
        for ($i = 0; $i < sizeof($this->modules); ++$i) {
            if (in_array($this->modules[$i], $processed_module_ids)) {
                continue;
            }
            $module_instance = $this->module_register->get($this->modules[$i]);
            $processed_module_ids[] = $this->modules[$i];
            // get dependencies
            $deps = new Dependencies(
                $this->file_hierarchy,
                $this->modules[$i]
            );
            $module_dependencies = array_merge(
                $module_dependencies,
                $deps->getModuleDependencies()
            );

            if (!$module_instance->hasCss()) {
                continue;
            }
            $inlined_css .= $module_instance->getRenderCriticalCss();
        }
        // process dependencies
        $module_dependencies = array_unique($module_dependencies);

        foreach ($module_dependencies as $module_id) {
            $module_instance = $this->module_register->get($module_id);
            // append critical css
            $inlined_css .= $module_instance->getRenderCriticalCss();
        }

        if ($inlined_css === '') {
            return false;
        }
        $this->head_element->appendChild($this->createAndSetupElement(
            'style',
            $inlined_css
        ));
        return true;
    }

    /**
     * \brief Adds the favicon tag to the page if configured.
     *
     * \retval bool False if it is not configured.
     */
    final protected function addFaviconTag() : bool
    {
        $favicon_url = $this->configuration->getValue('favicon_url');
        if ($favicon_url === false || $favicon_url === '') {
            return false;
        }
        $this->head_element->appendChild($this->createAndSetupElement('link', '', [
            'rel' => 'shortcut icon',
            'href' => $favicon_url
        ]));
        return true;
    }

    /**
     * \brief Adds the all required js tags to the head tag if available.
     *
     * \retval bool Always true.
     */
    final protected function addJavascriptTags() : bool
    {
        // add page js
        if ($this->hasJavascript()) {
            $this->head_element->appendChild($this->createAndSetupElement(
                'script',
                '',
                [
                    "src" => UrlPaths::implodeToAbsolute(
                        [
                            UrlPaths::RESOURCES,
                            UrlPaths::JAVASCRIPT,
                            UrlPaths::PAGE,
                            $this->page_id
                        ]
                    ),
                    'async' => 'async'
                ]
            ));
        }
        return true;
    }


    /**
     * \brief Runs all modules and appends them to the page tag.
     *
     * \retval bool Always true.
     */
    final protected function runAndAppendModulesHtml() : bool
    {
        for ($i = 0; $i < sizeof($this->modules); ++$i) {
            $module = $this->module_register->run(
                $this->modules[$i],
                $this->modules_options[$i]
            );
            //$this->modules[$i]->checkForUnusedDependencies();
            if (empty($this->configuration->getValue(Config::KEY_DISABLE_PAGE_WRAPPER))) {
                $this->body_element->firstChild->appendChild(
                    $this->importNode($module->documentElement, true)
                );
            } else {
                $this->body_element->appendChild(
                    $this->importNode($module->documentElement, true)
                );
            }
        }
        return true;
    }

    final protected function appendModuleDependencies() : bool
    {
        $module_dependencies = [];
        $javascript_dependencies = [];
        // collect all dependencies
        for ($i = 0; $i < sizeof($this->modules); ++$i) {
            $deps = new Dependencies(
                $this->file_hierarchy,
                $this->modules[$i]
            );
            $javascript_dependencies =
                array_merge($javascript_dependencies, $deps->getJavascriptDependencies());
            $module_dependencies =
                array_merge($module_dependencies, $deps->getModuleDependencies());
        }
        $module_dependencies = array_unique($module_dependencies);
        $javascript_dependencies = array_unique($javascript_dependencies);


        // add module tags if required
        foreach ($module_dependencies as $module_id) {
            if ($this->module_register->get($module_id)->hasJavascript()) {
                $this->head_element->appendChild($this->createAndSetupElement(
                    'script',
                    '',
                    [
                        "src" => UrlPaths::implodeToAbsolute(
                            [
                                UrlPaths::RESOURCES,
                                UrlPaths::JAVASCRIPT,
                                $module_id
                            ]
                        ),
                        'async' => 'async'
                    ]
                ));
            }
        }
        return true;
    }

    /**
     * \brief Sets the language of the page.
     */
    final public function setLanguage(string $language) : string
    {
        if (strlen($language) !== 2) {
            return '';
        }
        $this->language = $language;
        return $this->language;
    }

    /**
     * \brief Gets the language of the page.
     */
    final public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * \brief Synonym for $this->page_id.
     *
     * \retval string Returns the current page id.
     */
    final public function getPageId() : string
    {
        return $this->page_id;
    }

    final public function getSession() : Session
    {
        return $this->session;
    }

    abstract public function run(array $options = []) : bool;
}
