<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\FileHierarchy;
use Lewp\Resolve;
use \Symfony\Component\HttpFoundation\Session\Session as SymfonySession;

/**
 * \brief Representation of a module that can be included in a webpage.
 *
 */
class Session extends SymfonySession
{

    const NAMESPACE_CHAR = "/";

    private $module_id;

    private $module_execution_count;

    private $module_prefix = '';


    private $page_id;

    private $page_prefix = '';


    public function __construct(string $page_id = '', string $module_id = '', int $module_execution_count = 0)
    {
        parent::__construct();
        //$this->start();

        // module level
        $this->module_id = $module_id;
        $this->module_execution_count = $module_execution_count;
        $this->module_prefix = (!empty($module_id))
            ? $module_id . self::NAMESPACE_CHAR . $module_execution_count . self::NAMESPACE_CHAR
            : '';

        // page level
        $this->page_id = $page_id;
        $this->page_prefix = (!empty($page_id))
            ? $page_id . self::NAMESPACE_CHAR
            : '';
    }

    public function set($name, $value, $force_global = false)
    {
        if ($force_global) {
            parent::set($name, $value);
            return;
        }
        parent::set($this->page_prefix . $this->module_prefix . $name, $value);
    }

    public function get($name, $default = null)
    {
        if (!is_null($default) && is_bool($default) && $default === true) {
            return parent::get($name);
        }
        return parent::get($this->page_prefix . $this->module_prefix . $name)
            ?? parent::get($this->page_prefix . $name)
            ?? parent::get($name);
    }

    /**
     * \brief Shortcut method for $this->getFlashBag()->add function.
     */
    public function addFlash(string $type, $message)
    {
        $this->getFlashBag()->set($name, $value);
    }
}
