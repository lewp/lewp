<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

/**
 * \brief Base class for custom utilities. Contains convention definitions for
 * utilities that are created to extend lewp.
 * All derived classes can be configured using its class name in lowercase
 * letters.
 *
 */
class Utilities
{

    /**
     * \brief The key where the configuration is available for this utility.
     * It is defined by the class name in lowercase letters.
     */
    protected $configuration_key;

    public function __construct()
    {
        $this->configuration_key =
            strtolower((new \ReflectionClass($this))->getShortName());
    }
}
