<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\FileHierarchy;
use Lewp\Websocket\Message;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

/**
 * \brief Implements the websocket server to be used in combination with lewp.
 */
class WebSocket implements MessageComponentInterface
{

    /**
     * \brief SplObjectStorage that contains the file hierarchy specific for
     * the client.
     */
    protected $clients;

    /**
     * \brief Contains the root folder of the file hierarchy.
     */
    private $root_folder;

    public function __construct(string $file_hierarchy_root)
    {
        $this->root_folder = $file_hierarchy_root;
        $this->clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $connection)
    {
        try {
            $origin = $connection->httpRequest->getHeaders()["Origin"][0];
            $origin = str_replace("http://", "", $origin);
            $origin = str_replace("https://", "", $origin);
        } catch (Exception $ex) {
            echo "Connection was denied because of invalid origin!\n";
            return;
        }
        $file_hierarchy = new FileHierarchy($this->root_folder, $origin);
        $this->clients->attach($connection, $file_hierarchy);
        echo "New connection:\nId: {$connection->resourceId}\nOrigin: {$origin}\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $message = new Message();
        $message->decode($msg);
        if (!$message->isValid()) {
            return;
        }
        $filepath = realpath(
            $this->clients[$from]->findModuleDefinition($message->getModuleId())
        );
        if ($filepath === false) {
            return;
        }
        $module = include $filepath;
        if (!($module instanceof \Lewp\Module)) {
            return;
        }
        $module->initialize($this->clients[$from]);
        $module->setPageId($message->getPageId());

        $from->send($module->onWebsocketRequest($message)->encode());

        //$module_worker = new WsThread($from, $module, $message);
        //$this->clients[$from]["pool"]->submit($module_worker);
        //$module_worker->start();

        //$fromOrigin = $from->httpRequest->getHeaders()["Origin"];
        //foreach ($this->clients as $client) {
        //    $clientOrigin = $client->httpRequest->getHeaders()["Origin"];
        //    if (($from !== $client)
        //        && ($fromOrigin === $clientOrigin)) {
        //        // The sender is not the receiver, send to each client connected
        //        $client->send($msg);
        //    }
        //}
    }
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
    }
}
