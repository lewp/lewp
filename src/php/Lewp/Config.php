<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\FileHierarchy;
use Lewp\FileHierarchy\GlobalLevel;
use Lewp\FileHierarchy\SiteLevel;
use Lewp\FileHierarchy\GlobalModuleLevel;
use Lewp\FileHierarchy\SiteModuleLevel;
use Lewp\Interfaces\IFileHierarchy;
use Lewp\Resolve;

/**
 * \brief Representation of a configuration file. Can be used all three lewp
 * types site, page and module.
 * If used with site, both parameters have to be omitted.
 * To use it with a module that is not specific to a page, insert an empty
 * string to the page_id parameter.
 *
 */
class Config
{

    const KEY_DEPENDENCIES = 'dependencies';
    const KEY_TITLE = 'title';
    const KEY_LANGUAGE = 'language';
    const KEY_CSP_HEADER = 'cspheader';
    const KEY_ADD_VIEWPORT = 'addviewport';
    const KEY_ENCODING = 'encoding';
    const KEY_HIDDEN = 'hidden';
    const KEY_LINK_TEXT = 'linktext';
    const KEY_HTTPS = 'https';
    const KEY_WRAPPER_MODULE_ELEMENT_TYPE = 'element_type_module';
    const KEY_WRAPPER_PAGE_ELEMENT_TYPE = 'element_type_page';
    const KEY_ENABLE_LANGUAGE_URI = 'enable_language_uri';
    const KEY_DISABLE_PAGE_WRAPPER = 'disable_page_element';
    const KEY_DEFER_IMAGES = 'defer_images';
    const KEY_ENABLE_HTML_CACHE = 'enable_html_cache';
    const KEY_ENABLE_LAZY_LOADING = 'enable_lazy_loading';

    /**
     * \brief The page id that the configuration belongs to.
     */
    private $page_id;

    /**
     * \brief The module id that the configuration belongs to.
     */
    private $module_id;

    /**
     * \brief The actual configuration array.
     */
    private $configuration = null;

    /**
     * \brief The file hierarchy instance.
     */
    protected $file_hierarchy;

    public function __construct(IFileHierarchy $file_hierarchy, string $page_id = '', string $module_id = '')
    {
        $this->file_hierarchy = $file_hierarchy;
        $this->module_id = $module_id;
        $this->page_id = $page_id;
        $this->configuration = $this->load();
        $this->validate();
    }

    /**
     * \brief Loads the configuration of the module and/or the page, if existing.
     * Prefers the domain specific module configuration, and if that does not
     * exist, it takes the generic module configuration.
     * It also merges the page and site level configuration into the resulting
     * array.
     *
     * \param string $page_id The page id.
     *
     * \param string $module_id The module id.
     *
     * \retval array The configuration array of the module. An empty array if
     * there is no configuration.
     */
    protected function load() : array
    {
        $retarray = $this->defaultConfiguration();
        $levels = [
            new SiteLevel($this->file_hierarchy->getTLD())
        ];
        $config_filenames = [
            'site'
        ];
        if ($this->page_id !== '') {
            $levels [] = new SiteLevel($this->file_hierarchy->getTLD());
            $config_filenames [] = Resolve::idToFilepathRel($this->page_id);
        }
        array_push(
            $levels,
            new GlobalModuleLevel($this->module_id),
            new SiteModuleLevel($this->file_hierarchy->getTLD(), $this->module_id)
        );
        array_push(
            $config_filenames,
            'config',
            'config'
        );

        for ($i = 0; $i < sizeof($levels); ++$i) {
            //$base_path = FileHierarchy::generatePath($levels[$i], 'config');
            $base_path = $this->file_hierarchy->generatePath($levels[$i], 'config');
            if ($base_path !== false) {
                $config_filename =
                    $base_path . DIRECTORY_SEPARATOR . $config_filenames[$i] . '.php';

                if (file_exists($config_filename)) {
                    $conf_array = include $config_filename;
                } else {
                    continue;
                }

                if (is_array($conf_array)) {
                    $retarray = array_merge($retarray, $conf_array);
                }
            }
        }
        return $retarray;
    }

    /**
     * \brief Contains the default configuration that is applied before loading
     * the configuration file.
     *
     * \retval array The default configuration array.
     */
    protected function defaultConfiguration() : array
    {
        return [
            self::KEY_ENCODING => 'UTF-8',
            self::KEY_ENABLE_LANGUAGE_URI => true,
            self::KEY_TITLE => "Created using lewp! See lewp.org!",
            self::KEY_HTTPS => true,
            self::KEY_WRAPPER_MODULE_ELEMENT_TYPE => 'div',
            self::KEY_WRAPPER_PAGE_ELEMENT_TYPE => 'div',
            self::KEY_CSP_HEADER => "script-src 'self';",
            self::KEY_DISABLE_PAGE_WRAPPER => true,
            self::KEY_ADD_VIEWPORT => true,
            self::KEY_DEFER_IMAGES => true,
            self::KEY_ENABLE_HTML_CACHE => false,
            self::KEY_ENABLE_LAZY_LOADING => false
        ];
    }

    /**
     * \brief Validates the configuration.
     *
     * \params bool $throw_exceptions (optional) If true, an exception will be
     * thrown on fail containing a specific error message.
     *
     * \retval bool False if the configuration has errors.
     */
    private function validate(bool $throw_exceptions = false) : bool
    {
        if (array_key_exists(
            self::KEY_DEPENDENCIES,
            $this->configuration
        )
            &&
            !is_array($this->configuration[self::KEY_DEPENDENCIES])
        ) {
            if ($throw_exceptions) {
                trigger_error(
                    'Validation of configuration failed! ' .
                    self::KEY_DEPENDENCIES .
                    ' must be of type array, is type ' .
                    gettype($this->configuration[self::KEY_DEPENDENCIES]) .
                    '!',
                    E_USER_ERROR
                );
            }
            return false;
        }

        return true;
    }

    /**
     * \brief If exists, the value of the given key will be returned.
     *
     * \param string $key The requested key.
     *
     * \retval mixed False if the key does not exist.
     */
    public function getValue(string $key)
    {
        if (!array_key_exists($key, $this->configuration)) {
            return false;
        }
        return $this->configuration[$key];
    }

    /**
     * \brief Sets the value of the given key.
     *
     * \param string $key The key.
     * \param mixed $value The value.
     *
     * \retval Config Returns itself.
     */
    public function setValue(string $key, $value) : Config
    {
        $this->configuration[$key] = $value;
        return $this;
    }

    /**
     * If the given key contains an array as value, this function can be used
     * as a shortcut of the build in array_search function.
     *
     * \param mixed $needle The string that should be searched for.
     * \param string $key The configuration key.
     *
     * \retval mixed The key where the needle has been found in.
     * False if the needle has not been found.
     */
    public function arraySearch($needle, string $key)
    {
        return array_search($needle, $this->configuration[$key]);
    }

    /**
     * Returns the complete configuration as array.
     *
     * \retval array The configuration array.
     */
    public function get()
    {
        return $this->configuration;
    }
}
