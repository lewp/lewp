<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\FileHierarchy;
use Lewp\FileHierarchy\SiteLevel;
use Lewp\FileHierarchy\SiteModuleLevel;
use Lewp\Resolve;

/**
 * \brief Manages the access to the var folder, either on specific or generic
 * module level.
 */
class VarFolder
{

    const PUBLIC_ACCESS_FILENAME = '.public';

    private $module_id = '';

    private $filepath = '';

    private $level;

    protected $file_hierarchy;

    public function __construct(
        \Lewp\Interfaces\IFileHierarchy $file_hierarchy,
        string $module_id,
        bool $site_level = false
    ) {
        $this->file_hierarchy = $file_hierarchy;
        $this->module_id = $module_id;
        if ($site_level) {
            $this->level = new SiteLevel($this->file_hierarchy->getTLD());
        } else {
            $this->level = new SiteModuleLevel(
                $this->file_hierarchy->getTLD(),
                $module_id
            );
        }
        $this->filepath = $this->createBaseFolder();
    }

    private function createBaseFolder()
    {
        $base_path = $this->file_hierarchy->createFolder($this->level, 'var');
        return $base_path;
    }

    private function resolvePath($folder_id)
    {

        $folder_path = Resolve::idToFilepathRel($folder_id);
        return $this->filepath . DIRECTORY_SEPARATOR . $folder_path;
    }

    /**
     * \brief Grants public access to the given folder id by creating a file
     * named as stated in the PUBLIC_ACCESS_FILENAME constant.
     *
     * \param string $folder_id The folder id that will be made available.
     *
     * \retval bool False if the folder id does not exist, or something went
     * wrong during creation. True on success.
     */
    public function grantPublicAccess(string $folder_id) : bool
    {

        $pubfile = $this->openFile(
            $folder_id . Resolve::ID_SEPARATOR . self::PUBLIC_ACCESS_FILENAME,
            'wb'
        );
        if ($pubfile === false) {
            return false;
        }
        fwrite($pubfile, '');
        $this->closeFile($pubfile);
        return true;
    }

    /**
     * \brief Revokes the public access to the given folder id by removing the
     * file named as stated in PUBLIC_ACCESS_FILENAME constant.
     *
     * \param string $folder_id The folder id.
     *
     * \retval bool Always true.
     */
    public function revokePublicAccess($folder_id) : bool
    {

        $this->deleteFile(
            $folder_id . Resolve::ID_SEPARATOR . self::PUBLIC_ACCESS_FILENAME
        );
        return true;
    }

    /**
     * \brief Creates the given subfolder in the var folder.
     *
     * \param string $folder_id The folder id that should be created.
     *
     * \retval mixed False if something went wrong during creation. Otherwise
     * the path to the folder.
     */
    public function createFolder($folder_id)
    {

        $filepath = $this->resolvePath($folder_id);

        if (@mkdir($filepath, 0775, true) === false) {
        };

        return realpath($filepath);
    }

    /**
     * \brief Removes the given folder from the var folder.
     *
     * \param string $folder_id The folder id that should be removed.
     *
     * \retval bool False if the input id is not a folder, true otherwise.
     */
    public function removeFolder($folder_id) : bool
    {

        $filepath = $this->resolvePath($folder_id);
        if (!is_dir($filepath)) {
            return false;
        }
        $directory_iterator = new \RecursiveDirectoryIterator(
            $filepath,
            \RecursiveDirectoryIterator::SKIP_DOTS
        );
        $files = new \RecursiveIteratorIterator(
            $directory_iterator,
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($filepath);
        return true;
    }

    /**
     * \brief Checks if the given file exists in the var folder. The id needs
     * to be relative to the var folder.
     *
     * \param string $file_id The file id that should be checked for.
     *
     * \retval bool False if the input id is not available, true otherwise.
     */
    public function fileExists($file_id) : bool
    {

        if ($this->filepath === false) {
            return false;
        }

        $filepath = Resolve::toFilepath(array(
            $this->filepath,
            Resolve::idToFilepathRel($file_id)
        ));

        return (file_exists($filepath)) ? true : false;
    }

    /**
     * \brief Opens the given file and returns its handle.
     *
     * \param string $file_id The file id that will be opened.
     * \param string $mode The file mode that is required for fopen.
     *
     * \retval mixed False on failure, otherwise the file handle.
     */
    public function openFile(string $file_id, string $mode = 'rb')
    {

        if ($this->filepath === false) {
            return false;
        }

        $filepath = Resolve::toFilepath(array(
            $this->filepath,
            Resolve::idToFilepathRel($file_id)
        ));

        if (!file_exists(dirname($filepath))) {
            if (@mkdir(dirname($filepath), 0775, true) === false) {
            };
        }

        return fopen($filepath, $mode);
    }

    /**
     * \brief Closes the given file that has been previously opened using
     * openFile or fopen.
     *
     * \param resource $file_handle The file handle that will be closed.
     *
     * \retval bool Returns the outcome of fclose.
     */
    public function closeFile($file_handle) : bool
    {

        return fclose($file_handle);
    }

    /**
     * \brief Deletes the file from the var folder using unlink.
     *
     * \param string $file_id The file that will be deleted as id.
     *
     * \retval bool False if the file does not exist, the outcome of unlink
     * otherwise.
     */
    public function deleteFile(string $file_id)
    {

        $filepath = $this->file_hierarchy->findFile(
            'var',
            Resolve::idToFilepathRel($file_id),
            [$this->level]
        );
        return ($filepath === false) ? false : unlink(realpath($filepath));
    }

    /**
     * \brief Loads the content of the file and returns it.
     *
     * \param string $file_id The file id.
     *
     * \retval mixed The content of the file, false otherwise.
     */
    public function loadFile(string $file_id)
    {
        return $this->file_hierarchy->getFile('var', $file_id, [$this->level]);
    }

    /**
     * \brief Returns the file size of the given file id.
     *
     * \param string $file_id The file id.
     *
     * \retval int The filesize of the given file id.
     */
    public function getFileSize($file_id) : int
    {

        return ($this->filepath === false) ?
            false :
            filesize(realpath(Resolve::toFilepath(array(
                    $this->filepath,
                    Resolve::idToFilepathRel($file_id)
            ))));
    }

    /**
     * \brief Checks if the file is publicly available.
     *
     * \param string $id The id to be checked for.
     *
     * \retval bool True if the file or folder is publicly available.
     */
    public function isPublic(string $id) : bool
    {
        if (!is_dir($this->resolvePath($id))) {
            $id = Resolve::findFolderId($id);
        }
        return $this->fileExists(
            Resolve::arrayToId([$id, self::PUBLIC_ACCESS_FILENAME])
        );
    }
}
