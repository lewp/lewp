<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use \IvoPetkov\HTML5DOMDocument as IvoPetkovHtml;

/**
 * \brief HTML5DOMDocument by Ivo Petkov with some useful additions.
 *
 */
class Html5DomDocument extends IvoPetkovHtml
{

    /**
     * \brief Extended standard createElement function with possibility to add attributes.
     *
     * \param string $name The name of the tag that will bei created.
     * \param string $value The initial nodeValue.
     * \param array $attributes A list of key value pairs where the key
     * the resulting attribute and the value the value of it.
     *
     * \retval \DOMElement The created element.
     */
    public function createElement($tagName, $value = NULL, array $attributes = [])
    {
        $tag = parent::createElement($tagName, strval($value));
        foreach ($attributes as $key => $attr_value) {
            $tag->setAttribute($key, strval($attr_value));
        }
        return $tag;
    }

    /**
     * \brief Resets the HTML of the document by removing all children of
     * documentElement.
     *
     * \retval bool Always true.
     */
    public function resetHtml()
    {
        while ($this->documentElement->hasChildNodes()) {
            $this->documentElement->removeChild(
                $this->documentElement->childNodes[0]
            );
        }
        return true;
    }

    /**
     * \brief Sets the given attribute to the value on the module wrapping element.
     *
     * \param string $name The attribute name to be set.
     * \param string $value The value it will contain.
     *
     * \retval Module The module itself.
     */
    final public function setAttribute(string $name, string $value)
    {
        $this->firstChild->setAttribute($name, $value);
        return $this;
    }

}
