<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\Interfaces\IFileHierarchy;

class ModuleRegister
{

    /**
     * \brief Contains the file hierarchy where the register works on.
     */
    private $file_hierarchy;

    /**
     * \brief The language of the page.
     */
    private $language;

    /**
     * \brief The page id.
     */
    private $page_id;

    /**
     * \brief Contains all module instances.
     */
    private $module_instances = [];

    public function __construct(
        IFileHierarchy $file_hierarchy,
        string $page_id,
        string $language
    ) {
        $this->page_id = $page_id;
        $this->language = $language;
        $this->file_hierarchy = $file_hierarchy;
    }

    public function register(string $module_id) : bool
    {
        if (array_key_exists($module_id, $this->module_instances)) {
            // module has already been added to the register
            return false;
        }
        // get the module definition file
        $module_file =
            $this->file_hierarchy->findModuleDefinition($module_id);
        if ($module_file === false) {
            // module does not exist
            trigger_error(
                "Module " . $module_id . " could not be found",
                E_USER_WARNING
            );
            return false;
        }
        $module = include $module_file;

        $module->initialize($this->file_hierarchy);
        $module->setPageId($this->page_id);
        $module->setLanguage($this->language);
        $module->setModuleRegister($this);

        $this->module_instances[$module_id] = $module;
        return true;
    }

    public function get(string $module_id)
    {
        $this->register($module_id);
        return $this->module_instances[$module_id];
    }

    public function run(string $module_id, array $options = [])
    {
        // make sure the module is registered
        $this->register($module_id);

        $this->module_instances[$module_id]->resetHtml();

        // add session
        $session = new Session(
            $this->page_id,
            $module_id,
            $this->module_instances[$module_id]->getExecutionCount()
        );
        $this->module_instances[$module_id]->setSession($session);

        $this->module_instances[$module_id]->registerAutoloader();
        $this->module_instances[$module_id]->increaseExecutionCount();
        $this->module_instances[$module_id]->run($options);
        $this->module_instances[$module_id]->deferImages();
        $this->module_instances[$module_id]->unregisterAutoloader();

        return $this->module_instances[$module_id];
    }
}
