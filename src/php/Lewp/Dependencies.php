<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\Config;
use Lewp\Resolve;
use Lewp\Utilities;

/**
 * \brief Representation of the dependencies of a lewp module.
 */
class Dependencies extends Utilities
{

    private $module_id;

    /**
     * \brief Contains the module dependencies that have been resolved as an
     * array of strings.
     */
    private $dependencies = null;

    /**
     * \brief Contains the JavaScript dependencies that have been resolved as
     * an array of strings.
     */
    private $javascript_dependencies;

    private $file_hierarchy;

    public function __construct(FileHierarchy $file_hierarchy, string $module_id)
    {
        parent::__construct();
        $this->file_hierarchy = $file_hierarchy;
        $this->module_id = $module_id;
        $this->dependencies = $this->resolveDependencies($this->module_id);
        $this->javascript_dependencies = $this->resolveJavascriptDependencies();
    }

    /**
     * \brief Collects the first level of dependencies of the given configuration.
     *
     * \param Config $config The input configuration.
     *
     * \retval array The resulting dependency array, with unique values.
     */
    private function resolveFirstLevel(Config $config) : array
    {
        $deps = [];
        $config = $config->getValue($this->configuration_key);
        if (isset($config["modules"])) {
            $deps = $config["modules"];
        }
        return $deps;
    }

    /**
     * \brief Resolves the dependencies for the given module id.
     *
     * \param string $module_id The module id.
     * \param array &$resolved_modules Helping parameter that is being used to
     * collect all dependencies. For internal use only!
     *
     * \returns array An array containing the modules that the given module relies on.
     */
    private function resolveDependencies(
        string $module_id,
        array &$resolved_modules = []
    ) : array {
        $config = new Config($this->file_hierarchy, '', $module_id);
        // get all modules that are directly depending on the given one
        $retarray = $this->resolveFirstLevel($config);
        // determine which ones already have been resolved
        // merging the module id into it prevents that the id will be added to
        // the resolved array when a loop is present
        $intersection = array_intersect(
            array_merge($resolved_modules, [$this->module_id]),
            $retarray
        );
        // if the module id is present in the intersection, a loop has been found
        if (in_array($this->module_id, $intersection)) {
            trigger_error(
                'Loop dependency detected for modules "' .
                $this->module_id .
                '" and "' .
                $module_id .
                '"!',
                E_USER_ERROR
            );
        }
        // calculate which modules have not been resolved yet
        $diff = array_diff($retarray, array_merge($resolved_modules, $intersection));
        // exit the function if there are no more modules to process
        if (empty($diff)) {
            return $resolved_modules;
        }
        $resolved_modules = array_merge($resolved_modules, $diff);
        // do the above for every id that is left in the diff array
        array_map(function ($module_id) use (&$resolved_modules) {
            return $this->resolveDependencies($module_id, $resolved_modules);
        }, $diff);
        // finally return the resulting dependency array
        return $resolved_modules;
    }

    private function resolveJavascriptDependencies()
    {
        if (is_null($this->dependencies)) {
            $this->dependencies = $this->resolveDependencies($this->module_id);
        }
        // get own javascript dependencies
        $config = new Config($this->file_hierarchy, '', $this->module_id);
        $js_deps = (
            isset($config->getValue($this->configuration_key)['javascript'])
        )
        ? $config->getValue($this->configuration_key)['javascript']
        : [];
        $ret = (!empty($js_deps)) ? $js_deps : [];
        return array_unique($ret);
    }

    /**
     * \brief Returns the dependency array of the module given at construction.
     *
     * \retval array The dependency array.
     */
    public function getModuleDependencies() : array
    {
        return $this->dependencies;
    }

    /**
     * \brief Returns the JavaScript dependency array of the module given at
     * construction. The dependency array is an array of strings. Each string
     * contains the following information: module-id/javascript-dependency
     *
     * \retval array The JavaScript dependency array.
     */
    public function getJavascriptDependencies() : array
    {
        return $this->javascript_dependencies;
    }
}
