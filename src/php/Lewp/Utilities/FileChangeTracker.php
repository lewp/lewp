<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Utilities;

use Lewp\Resolve;
use Lewp\Utilities;
use Lewp\VarFolder;

/**
 * \brief Used to track file changes.
 */
class FileChangeTracker extends Utilities
{

    /**
     * \brief The module id.
     */
    private $module_id;

    /**
     * \brief The folder id where the file is stored in the file hierarchy.
     */
    private $folder_id;

    /**
     * \brief The file id in the file hierarchy.
     */
    private $file_id;

    /**
     * \brief The file hierarchy instance.
     */
    private $file_hierarchy;

    /**
     * \brief The var folder instance.
     */
    private $var_folder;

    /**
     * \brief The workspace folder for the current instance.
     */
    private $workspace_id;

    /**
     * \brief True if the state has changed since the last instanciation.
     */
    private $has_state_changed;

    public function __construct(
        \Lewp\Interfaces\IFileHierarchy $file_hierarchy,
        string $module_id,
        string $folder_id,
        string $file_id
    ) {
        parent::__construct();
        $this->file_hierarchy = $file_hierarchy;
        $this->module_id = $module_id;
        $this->folder_id = $folder_id;
        $this->file_id = $file_id;
        $this->workspace_id = Resolve::arrayToId(
            ['tmp', $this->configuration_key, $this->folder_id]
        );
        $this->state_file_id = Resolve::arrayToId(
            [$this->workspace_id, $this->file_id . '.sha1']
        );
        $this->var_folder = new VarFolder($this->file_hierarchy, $this->module_id);
        $this->var_folder->createFolder($this->workspace_id);
    }

    /**
     * \brief Saves the given file state to the equivalent var folder.
     *
     * \param string $file_state The file state representation.
     *
     * \retval bool Always true.
     */
    private function saveFileState(string $file_state)
    {
        $file_state_file = $this->var_folder->openFile(
            $this->state_file_id,
            'wb'
        );
        fwrite($file_state_file, $file_state);
        $this->var_folder->closeFile($file_state_file);
        return true;
    }

    /**
     * \brief Loads the previously saved file state from the var folder.
     *
     * \retval mixed The file state representation as string. False on fail.
     */
    private function getFileState()
    {
        if (!$this->var_folder->fileExists($this->state_file_id)) {
            return false;
        }
        return $this->var_folder->loadFile($this->state_file_id);
    }

    /**
     * \brief Calculates the file state from the given resource on construction.
     *
     * \retval mixed The file state representation as string. False on fail.
     */
    private function calculateFileState()
    {
        $source_file_name = $this->file_hierarchy->findFile(
            $this->folder_id,
            Resolve::idToFilepathRel($this->file_id),
            $this->file_hierarchy->factoryLevelArray([], $this->module_id)
        );
        if ($source_file_name === false) {
            return false;
        }
        return sha1_file($source_file_name);
    }

    /**
     * \brief Updates the state if the file has changed.
     *
     * \retval bool True if the file has changed or no state has been saved yet,
     * false otherwise.
     */
    private function updateState() : bool
    {
        //if (!$this->var_folder->fileExists($this->state_file_id)) {
        //    return false;
        //}
        $old_state = $this->getFileState();
        $current_state = $this->calculateFileState();
        if ($old_state === false) { // an old state does not exist
            $this->saveFileState($current_state);
            return true;
        }
        if ($old_state !== $current_state) {
            $this->saveFileState($current_state);
            return true;
        }
        return false;
    }

    /**
     * \brief Returns true if the file has changed since last instanciation.
     *
     * \retval bool True if the file has changed or no state has been saved yet,
     * false otherwise.
     */
    public function hasStateChanged() : bool
    {
        if (empty($this->file_id)) {
            return false;
        }
        $this->has_state_changed = $this->updateState();
        return $this->has_state_changed;
    }
}
