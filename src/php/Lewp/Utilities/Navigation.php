<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Utilities;

use Lewp\Config;
use Lewp\FileHierarchy;
use Lewp\FileHierarchy\SiteLevel;
use Lewp\Resolve;
use Lewp\Utilities;

/**
 * \brief Collects all information about the available webpages.
 *
 */
class Navigation extends Utilities
{

    /**
     * \brief Configuration key that defines the page to follow. With this,
     * a custom order can be defined. If not set, the order will be alphabetically.
     */
    const CONFIG_KEY_FOLLOWS_PAGE = 'followspage';

    /**
     * \brief Holds the array containg the meta information about the pages.
     */
    private $pages = [];

    /**
     * \brief The file hierarchy instance.
     */
    protected $file_hierarchy;

    public function __construct(FileHierarchy $file_hierarchy)
    {
        parent::__construct();
        $this->file_hierarchy = $file_hierarchy;
        $this->pages = $this->createPagesArray();
        $this->sortPages(); // sort them by the CONFIG_KEY_FOLLOWS_PAGE
    }

    /**
     * \brief Recursively walks through the pages directory and selects all pages that are
     * available. Skips the index.php.
     *
     * \param string $level_id The level id that should be searched.
     *
     * \retval array Returns an array that contains all pages available in the
     * given level.
     */
    protected function collectLevel(string $level_id)
    {
        // get the base path of all pages in the website
        $base_path = $this->file_hierarchy->generatePath(
            new SiteLevel($this->file_hierarchy->getTLD()),
            "pages"
        );
        if ($base_path === false) {
            return [];
        }
        if ($level_id !== '') { // extend the base path by the given level id
            $base_path = realpath(
                $base_path . DIRECTORY_SEPARATOR . Resolve::idToFilepathRel($level_id)
            );
            if ($base_path === false) { // it does not exist
                return [];
            }
        }
        // get all php files except for the index.php
        $files = array_filter(
            glob($base_path . DIRECTORY_SEPARATOR . '*.php'),
            function ($x) {
                return (substr($x, -9) !== 'index.php');
            }
        );
        $ret = [];
        foreach ($files as $page) { // collect all information
            if (is_dir($page)) {
                continue;
            }
            $info = pathinfo($page);
            $full_id = ($level_id !== '')
                ? $level_id . Resolve::ID_SEPARATOR . $info['filename']
                : $info['filename'];
            $config = new Config($this->file_hierarchy, $full_id, '');
            $nav_provider_config = $config->getValue($this->configuration_key);
            if ($nav_provider_config === false) {
                $nav_provider_config = [];
            }
            $nav_provider_config += [
                Config::KEY_LINK_TEXT => $info['filename'],
                Config::KEY_HIDDEN => false,
                self::CONFIG_KEY_FOLLOWS_PAGE => ''
            ];
            if ($nav_provider_config[Config::KEY_HIDDEN] === true) {
                continue;
            }
            $ret[] = [
                'baseid' => $level_id,
                "id" => $info['filename'],
                Config::KEY_TITLE => $config->getValue(Config::KEY_TITLE),
            ]
            + $nav_provider_config;
        }
        return $ret;
    }

    /**
     * \brief Finds the array that holds all available pages and its subpages
     * of the website.
     *
     * \param string $level_id The level that should be searched for.
     *
     * \retval mixed The array that has been found. False if the id has not
     * been found.
     */
    public function findLevel(string $level_id = '')
    {
        if ($level_id === '') {
            return $this->pages;
        }
        $levels = Resolve::idExplode($level_id);
        $resulting_level = $this->pages;
        $i = 0;
        while ($i < sizeof($levels)) {
            // find the key
            $key = array_search($levels[$i], array_column($resulting_level, "id"));
            if ($key === false) {
                return false;
            }
            $resulting_level = $resulting_level[$key]['subpages'];
            if (empty($resulting_level)) {
                break;
            }
            ++$i;
        }
        return $resulting_level;
    }

    /**
     * \brief Creates the array that is required for all other functions to work.
     * It collects all levels of pages that are available on the website.
     *
     * \param string $level_id The level it that should be collected. Not intended
     * to be set, it is used for recursiveness.
     *
     * \retval array The pages array that has been determined.
     */
    protected function createPagesArray(string $level_id = '')
    {
        $current_level = $this->collectLevel($level_id);
        if (empty($current_level)) {
            return [];
        }
        usort(
            $current_level,
            function ($x, $y) {
                return strcasecmp($x['id'], $y['id']);
            }
        );
        foreach (array_keys($current_level) as $page_key) {
            $current_level[$page_key]['subpages'] =
                $this->createPagesArray(
                    ($level_id === '')
                    ? $current_level[$page_key]['id']
                    : $level_id . Resolve::ID_SEPARATOR . $current_level[$page_key]['id']
                );
        }
        return $current_level;
    }

    /**
     * \brief Returns all pages and its subpages that are available on the website.
     *
     * \retval array The array containing all pages and subpages on the website.
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * \brief Experimental function. Collects all pages that are on a specific
     * level.
     * 0 = The main level.
     * 1 = All pages that are in a folder.
     * ...
     *
     * \param int $level The level of the column.
     *
     * \retval array The column of the pages array. Returns an empty array if the
     * column is not available.
     */
    public function getColumn(int $level = 0)
    {
        if ($level === 0) {
            return $this->pages;
        }
        $current_level = 1;
        $subs = array_column($this->pages, "subpages");
        while ($current_level < $level) {
            $tmp = [];
            foreach ($subs as $sub) {
                $tmp = array_merge($tmp, array_column($sub, "subpages"));
            }
            $subs = $tmp;
            ++$current_level;
        }
        return array_filter($subs);
    }

    /**
     * \brief Same as getColumn function, but the key can be specified.
     *
     * \param string $key The key that should be in the resulting array.
     * \param int $level The level that will be collected.
     *
     * \retval array An array containing the values of the key column in the
     * given level of the page array.
     */
    public function getKeyColumn(string $key, int $level = 0)
    {
        if ($level === 0) {
            return array_column($this->pages, $key);
        }
        $current_level = 1;
        $subs = array_column($this->pages, "subpages");
        while ($current_level < $level) {
            $tmp = [];
            foreach ($subs as $sub) {
                $tmp = array_merge($tmp, array_column($sub, "subpages"));
            }
            $subs = $tmp;
            ++$current_level;
        }
        $ret = [];
        foreach ($subs as $sub) {
            $ret = array_merge($ret, array_column($sub, $key));
        }
        natcasesort($ret);
        return $ret;
    }

    /**
     * \brief Returns the complete list of the page ids that are available. Can
     * be used to create a sitemap.
     *
     * \retval array The array containing all page ids that are available.
     */
    public function getCompletePageIdList()
    {
        $id_list = [];
        $this->deepWalkPages(function (array $level, &$user_data) {
            foreach ($level as $page) {
                $user_data[] = ($page['baseid'] !== "")
                    ? $page['baseid'] . Resolve::ID_SEPARATOR . $page['id']
                    : $page['id'];
            }
        }, $id_list);
        return $id_list;
    }

    /**
     * \brief Same as deepWalkPages, but with the ability to change something
     * in the pages array.
     *
     * \param function $level_function The user function that will be executed on
     * every level.
     * \param mixed &$user_data Optional. The user data that can be passed to the
     * $level_function. Can be used to collect data from the pages array.
     * \param array &$current_level The current level that will be walked through.
     * Do not use this parameter, its purpose is only for recursiveness.
     *
     * \retval array An array containing all return values of the $level_function.
     * It has the same structure as the pages array, but with only numbers as keys.
     *
     */
    private function deepWalkPagesRef(
        $level_function,
        &$user_data,
        array &$current_level
    ) : array {
        $ret = [];
        $ret[] = $level_function($current_level, $user_data);
        foreach ($current_level as $page) {
            if (!empty($page['subpages'])) {
                $ret[] = $this->deepWalkPagesRef(
                    $level_function,
                    $user_data,
                    $page['subpages']
                );
            }
        }
        return $ret;
    }

    /**
     * \brief Walks through the pages array and applies a given user function on
     * every level.
     *
     * \param function $level_function The function that should be executed on
     * every level. The signature must look like this:\n
     * function(array $level, &$user_data)\n
     * The second parameter is optional, its reference will be passed to it so
     * it is possible for the user to get the desired information in the calling
     * stack of the function.
     * \param mixed &$user_data Optional. It is used to collect data on doing the
     * walk through the pages array.
     *
     * \retval array An array containing all return values of the $level_function.
     * It has the same structure as the pages array, but with only numbers as keys.
     */
    public function deepWalkPages($level_function, &$user_data) : array
    {
        $pages_copy = $this->pages;
        return $this->deepWalkPagesRef($level_function, $user_data, $pages_copy);
    }

    /**
     * \brief Sorts all pages by the CONFIG_KEY_FOLLOWS_PAGE.
     *
     * \retval bool Always true.
     */
    private function sortPages() : bool
    {
        $data = false;
        $this->deepWalkPagesRef(
            function (array &$level) {
                $this->sortLevel($level);
            },
            $data,
            $this->pages
        );
        return true;
    }

    /**
     * \brief Sorts the given level by CONFIG_KEY_FOLLOWS_PAGE.
     *
     * \param array &$level_array The level array that will be sorted.
     *
     * \retval bool Always true.
     */
    private function sortLevel(array &$level_array) : bool
    {
        $followspage_column = array_column($level_array, self::CONFIG_KEY_FOLLOWS_PAGE);
        $ret = [];
        foreach ($followspage_column as $key => $value) {
            if ($value !== "") {
                continue;
            }
            $ret[] = $level_array[$key];
        }
        // remove all that follow no one
        $followspage_column = array_filter($followspage_column);

        while (!empty($followspage_column)) {
            foreach ($followspage_column as $key => $value) {
                // get the ids that are already processed
                $id_column = array_column($ret, 'id');
                // check if the current item can be inserted somewhere
                $element_to_insert = $level_array[$key];
                $key_to_follow = array_search($value, $id_column);
                if ($key_to_follow === false) { // key not available yet
                    continue; // try another one
                }
                array_splice($ret, $key_to_follow + 1, 0, [$element_to_insert]);
                $followspage_column[$key] = ""; // mark the one as processed
                // stop the loop because the processed one needs to be removed
                break;
            }
            // remove processed one
            $followspage_column = array_filter($followspage_column);
        }
        $level_array = $ret;
        return true;
    }
}
