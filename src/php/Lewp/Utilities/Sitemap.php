<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Utilities;

use Lewp\Config;
use Lewp\FileHierarchy;
use Lewp\Utilities\Navigation as NavigationProvider;
use Lewp\Interfaces\IFileHierarchy;
use Lewp\Resolve;
use Lewp\Utilities;

/**
 * \brief Creates a sitemap of the current website using the file hierarchy.
 */
class Sitemap extends Utilities
{

    /**
     * \brief The configuration key that defines the protocol for the sitemap.
     */
    const CONFIG_KEY_PROTOCOL = "protocol";

    /**
     * \brief The configuration key that defines if the startpage should be
     * hidden in the sitemap.
     */
    const CONFIG_KEY_HIDE_STARTPAGE = "hidestartpage";

    /**
     * \brief The startpage id of the website.
     */
    private $startpage_id;

    /**
     * \brief The navigation provider object.
     */
    private $navigation_provider;

    /**
     * \brief The configuration array for the sitemap, defined in the site
     * configuration.
     */
    private $configuration;

    /**
     * \brief An array that contains all links that have been generated on
     * object instantiation.
     */
    private $sitemap;

    public function __construct(IFileHierarchy $file_hierarchy, string $startpage_id)
    {
        parent::__construct();
        $this->startpage_id = $startpage_id;
        $this->top_level_domain = $file_hierarchy->getTLD();
        $this->configuration = (new Config($file_hierarchy, '', ''))->getValue($this->configuration_key);
        $this->configuration = (!isset($this->configuration))
            ? $this->configuration + $this->defaultConfiguration()
            : $this->defaultConfiguration();

        $this->navigation_provider = new NavigationProvider($file_hierarchy);
        $this->relevant_page_ids = $this->filterIds(
            $this->navigation_provider->getCompletePageIdList()
        );
        $this->sitemap = $this->createSitemap();
    }

    /**
     * \brief Returns the default configuration of a sitemap object.
     *
     * \retval array The default configuration.
     */
    private function defaultConfiguration()
    {
        return [
            self::CONFIG_KEY_PROTOCOL =>
                (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off')
                ? 'http'
                : 'https',
            self::CONFIG_KEY_HIDE_STARTPAGE => true
        ];
    }

    /**
     * \brief Returns the ids without the startpage id if configured.
     *
     * \retval array The ids filtered by startpage id if configured.
     */
    private function filterIds($ids)
    {
        if ($this->configuration[self::CONFIG_KEY_HIDE_STARTPAGE]) {
            // remove startpage
            return array_filter($ids, function ($id) {
                    return (strcmp($id, $this->startpage_id) !== 0);
            });
        }
        return $ids;
    }

    /**
     * \brief Creates the sitemap links.
     *
     * \retval array An array containing all links relevant for this website.
     */
    private function createSitemap() : array
    {
        $protocol = $this->configuration[self::CONFIG_KEY_PROTOCOL];
        $ret = [
            $protocol . '://' . $this->top_level_domain
        ];
        foreach ($this->relevant_page_ids as $id) {
            array_push(
                $ret,
                $protocol . '://' . $this->top_level_domain . '/' . Resolve::idToUri($id)
            );
        }
        return $ret;
    }

    /**
     * \brief Returns the generated sitemap as txt.
     *
     * \retval string The generated sitemap as txt.
     */
    public function getSitemapTxt() : string
    {
        return utf8_encode(implode("\n", $this->sitemap));
    }

    /**
     * \brief Returns the generated sitemap as xml.
     *
     * \retval string The generated sitemap as xml.
     */
    public function getSitemapXml() : string
    {
        $header = '<?xml version="1.0" encoding="UTF-8"?>';
        $dom = new \DOMDocument();
        $urlset = $dom->createElement('urlset');
        $urlset->setAttribute(
            "xmlns",
            "http://www.sitemaps.org/schemas/sitemap/0.9"
        );
        foreach ($this->sitemap as $loc_string) {
            $url = $dom->createElement('url');
            $loc = $dom->createElement('loc', $loc_string);
            $url->appendChild($loc);
            $urlset->appendChild($url);
        }
        $dom->appendChild($urlset);
        return $header . $dom->saveHTML();
    }
}
