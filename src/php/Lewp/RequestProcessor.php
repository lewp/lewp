<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

/**
 * \brief Used for processing an http/https request string.
 * Cuts off any GET request part.
 * ATTENTION: If used with language support enabled, the first "directory" in
 * the request will be interpreted as language if it consists of two
 * characters!
 *
 */
class RequestProcessor
{

    /**
     * \brief Array containing all parts of the initial request, excluding GET
     * parameters and excluding the anchor.
     */
    private $initial_request;

    /**
     * \brief Contains the initial path of the request without the root slash.
     */
    private $initial_path;

    /**
     * \brief Contains the initial path of the request as array
     * (including language if set).
     */
    private $initial_path_array;

    /**
     * \brief Same as $initial_path_array but excluding the language.
     */
    private $initial_path_array_without_language;

    /**
     * \brief Containing the anchor of the initial request.
     */
    private $initial_parameter_anchor;

    /**
     * \brief Containing the get parameter of the initial request.
     */
    private $initial_parameter_get;

    /**
     * \brief Holds the parts that have not been processed yet. Is used in
     * combination with the getNextRequestPart function.
     */
    private $shifting_request_parts;

    /**
     * \brief True if the automatic language detection is enabled.
     */
    private $enable_language_uri;

    /**
     * \brief Holds the language in two characters.
     */
    private $language = '';

    public function __construct(
        string $request,
        bool $enable_language_uri = true
    ) {
        $this->enable_language_uri = $enable_language_uri;

        $this->initial_path = $this->removeRootSlash(
            $this->removeParameters($request)
        );
        $this->initial_path_array = $this->explodePath($this->initial_path);
        $this->initial_path_array_without_language =
            $this->removeLanguage($this->initial_path_array);
        $this->initial_parameter_get = $this->extractParameterGet($request);
        $this->initial_parameter_anchor =
            $this->extractParameterAnchor($request);
        $this->language = $this->extractLanguage($request);

        $this->shifting_request_parts =
            $this->initial_path_array_without_language;
    }

    /**
     * \brief Removes the root slash of the request if available.
     *
     * \param string $request The request string.
     *
     * \retval string The request string without the root slash if there was one.
     */
    private function removeRootSlash(string $request) : string
    {
        return (substr($request, 0, 1) === "/") ? substr($request, 1) : $request;
    }

    /**
     * \brief Extracts the anchor from the given request and returns it.
     *
     * \param string $request The request string.
     *
     * \retval string The anchor of the request. If there is none an empty
     * string is returned.
     */
    private function extractParameterAnchor(string $request) : string
    {
        $ret = explode('#', $request);
        return (sizeof($ret) === 2) ? $ret[1] : '';
    }

    /**
     * \brief Extracts the GET parameters from the given request and returns it.
     *
     * \param string $request The request string.
     *
     * \retval string The GET parameter of the request. If there is none an empty
     * string is returned.
     */
    private function extractParameterGet(string $request) : string
    {
        $ret = explode('?', explode('#', $request)[0]);
        return (sizeof($ret) === 2) ? $ret[1] : '';
    }

    /**
     * \brief Extracts the language from the given request and returns it.
     *
     * \param string $request The request string.
     *
     * \retval string The language of the request. If there is none an empty
     * string is returned.
     */
    private function extractLanguage(string $request) : string
    {
        $request = $this->removeRootSlash($request);
        if (!$this->enable_language_uri) {
            return '';
        }
        $array = explode("/", $request);
        return (strlen($array[0]) === 2) ? $array[0] : '';
    }

    /**
     * \brief Removes the GET parameter and anchor from the string and returns
     * it.
     *
     * \param string $request The request string.
     *
     * \retval string The request without the GET parameters and anchor.
     */
    private function removeParameters(string $request) : string
    {
        $ret = explode('#', $request)[0];
        return explode('?', $ret)[0];
    }

    /**
     * \brief Explodes the given path by / and returns the resulting array.
     * If the resutling array contains an empty string an empty array is being
     * returned.
     *
     * \param string $path The path.
     *
     * \retval array The exploded array.
     */
    private function explodePath(string $path) : array
    {
        $array = explode("/", $path);
        return (sizeof($array) === 1 && $array[0] === '')
            ? []
            : $array;
    }

    /**
     * \brief Removes the language from the given array that has previously been
     * processed by the explodePath function.
     *
     * \param array $request_array The array to be processed.
     *
     * \retval array Copy of the array without the language in it.
     */
    private function removeLanguage(array $request_array) : array
    {
        if (empty($request_array)) {
            return [];
        }
        if ($this->enable_language_uri && strlen($request_array[0]) === 2) {
            array_shift($request_array);
        }
        return $request_array;
    }

    /**
     * \brief Returns the next request part. The request will be processed from
     * left to the right. The language will not be returned.
     *
     * \retval string The next request part in the series.
     */
    public function getNextRequestPart() : string
    {
        $part = array_shift($this->shifting_request_parts);
        return (!isset($part) || empty($part))
            ? ''
            : $part;
    }

    /**
     * \brief Returns the number of request parts that are not returned by the
     * getNextRequestPart function yet.
     *
     * \retval int The number of request parts that are left.
     */
    public function requestPartCount() : int
    {
        return sizeof($this->shifting_request_parts);
    }

    /**
     * \brief Returns the full request as a string, either with root slash or
     * without. Same for the language.
     *
     * \param bool $remove_root_slash If set to true, the request will be
     * returned without the leading slash.
     * \param bool $include_language If set to true, the language is being
     * inserted.
     *
     * \retval string The resulting request.
     */
    public function getFullRequest(
        bool $remove_root_slash = true,
        bool $include_language = false
    ) : string {
        $path = [];
        if ($include_language && ($this->language !== '')) {
            $path [] = $this->language;
        }
        $path = array_merge($path, $this->initial_path_array_without_language);
        $path = implode("/", $path);
        if ($this->initial_parameter_get !== '') {
            $path .= "?" . $this->initial_parameter_get;
        }
        if ($this->initial_parameter_anchor !== '') {
            $path .= "#" . $this->initial_parameter_anchor;
        }
        return ($remove_root_slash)
            ? $path
            : '/' . $path;
    }

    /**
     * \brief Returns true if all request parts have been returned by the
     * getNextRequestPart function.
     *
     * \retval bool True if no part is left.
     */
    public function isEmpty() : bool
    {
        return empty($this->shifting_request_parts);
    }

    /**
     * \brief Returns the language of the request.
     *
     * \retval string The language as a two character string, an empty string
     * if not available.
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * \brief Returns true if a language has been set.
     *
     * \retval bool True if a language is available.
     */
    public function hasLanguage() : bool
    {
        return ($this->language !== '');
    }

    /**
     * \brief Sets the language of the request.
     *
     * \retval bool True if set successfully.
     */
    public function setLanguage(string $language) : bool
    {
        if (strlen($language) !== 2) {
            return false;
        }
        $this->language = $language;
        return true;
    }
}
