<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Css;

use Lewp\Css\Splitter;
use Lewp\FileHierarchy;
use Lewp\Interfaces\IFileHierarchy;
use Lewp\Resolve;
use Lewp\Utilities\FileChangeTracker;
use Lewp\VarFolder;
use \Sabberworm\CSS\Parser as CSSParser;
use \Padaliyajay\PHPAutoprefixer\Autoprefixer as CSSAutoPrefixer;
use \MatthiasMullie\Minify;

class Processor
{

    const FILENAME_NON_RENDER_CRITICAL = 'nonrendercritical.css';

    const FILENAME_RENDER_CRITICAL = 'rendercritical.css';

    private $module_id;

    private $var_folder;

    private $css_file_names = [];

    private $file_change_states = [];

    private $sandboxedCombinedCss = '';

    private $workspace_folder_id;

    public function __construct(
        IFileHierarchy $file_hierarchy,
        string $module_id,
        array $css_file_names
    ) {
        $this->module_id = $module_id;
        $this->css_file_names = $css_file_names;

        $this->workspace_folder_id = 'tmp' . Resolve::ID_SEPARATOR . 'css';

        $this->var_folder = new VarFolder($file_hierarchy, $this->module_id);
        $this->var_folder->createFolder($this->workspace_folder_id);

        foreach ($this->css_file_names as $css_file) {
             $tmp = new FileChangeTracker(
                 $file_hierarchy,
                 $this->module_id,
                 'css',
                 basename($css_file)
             );
            $this->file_change_states[] = $tmp->hasStateChanged();
        }

        if (in_array(true, $this->file_change_states)) {
            $this->sandboxedCombinedCss =
                $this->sandboxCss($this->getCombinedCssFiles());
            $this->createSplittedFiles($this->sandboxedCombinedCss);
        }
    }

    private function loadCssFilesIfRequired() : bool
    {
        if ($this->sandboxedCombinedCss === '') {
            $this->sandboxedCombinedCss =
                $this->sandboxCss($this->getCombinedCssFiles());
            return true;
        }
        return false;
    }

    private function getCombinedCssFiles() : string
    {
        $combined_css = '';
        for ($i = 0; $i < sizeof($this->css_file_names); ++$i) {
            $combined_css .= file_get_contents($this->css_file_names[$i]);
        }
        return $combined_css;
    }

    private function createSplittedFiles(string $complete_css)
    {
        $splitter = new Splitter();
        $render_critical = $splitter->getRenderCriticalCss($complete_css);
        $non_render_critical = $splitter->getNonRenderCriticalCss($complete_css);

        // add vendor prefixes
        $render_critical = (new CSSAutoPrefixer($render_critical))->compile();
        $non_render_critical = (new CSSAutoPrefixer($non_render_critical))->compile();

        // minify both before saving
        $render_critical = (new Minify\CSS($render_critical))->minify();
        $non_render_critical = (new Minify\CSS($non_render_critical))->minify();

        $non_render_critical_file = $this->var_folder->openFile(
            Resolve::arrayToId([
                $this->workspace_folder_id,
                self::FILENAME_NON_RENDER_CRITICAL
            ]),
            'wb'
        );
        fwrite($non_render_critical_file, $non_render_critical);
        $this->var_folder->closeFile($non_render_critical_file);

        $render_critical_file = $this->var_folder->openFile(
            Resolve::arrayToId([
                $this->workspace_folder_id,
                self::FILENAME_RENDER_CRITICAL
            ]),
            'wb'
        );
        fwrite($render_critical_file, $render_critical);
        $this->var_folder->closeFile($render_critical_file);
        return true;
    }

    private function sandboxCss(string $css) : string
    {
        // add module classes to all css tags
        $css_classes =
            //'.'.Resolve::stringToCssClassName(FileHierarchy::getTLD())
            '.' . Resolve::stringToCssClassName($this->module_id);

        $parser = new CSSParser($css);
        $css_parsed = $parser->parse();
        foreach ($css_parsed->getAllDeclarationBlocks() as $block) {
            foreach ($block->getSelectors() as $selector) {
                // only add classes to the first selector if they are separated
                // by whitespaces
                $white_sep = explode(' ', $selector->getSelector(), 2);
                $pseudo_sep = explode(':', $white_sep[0], 2);
                $ext_selector = $pseudo_sep[0] . $css_classes;
                if (sizeof($pseudo_sep) === 2) {
                    $ext_selector .= ':' . $pseudo_sep[1];
                }
                if (sizeof($white_sep) === 2) {
                    $ext_selector .= ' ' . $white_sep[1];
                }
                $selector->setSelector($ext_selector);
            }
        }
        return $css_parsed->render();
    }

    /**
     * \brief Returns the render critical css of the module.
     *
     * \retval string The render critical sandboxed css.
     */
    public function getRenderCriticalCss() : string
    {
        $file_id = Resolve::arrayToId(
            [$this->workspace_folder_id, self::FILENAME_RENDER_CRITICAL]
        );
        if (!$this->var_folder->fileExists($file_id)) {
            return '';
        }
        return $this->var_folder->loadFile($file_id);
    }

    /**
     * \brief Returns the non render critical css of the module.
     *
     * \retval string The non render critical sandboxed css.
     */
    public function getNonRenderCriticalCss() : string
    {
        $file_id = Resolve::arrayToId(
            [$this->workspace_folder_id, self::FILENAME_NON_RENDER_CRITICAL]
        );
        if (!$this->var_folder->fileExists($file_id)) {
            return '';
        }
        return $this->var_folder->loadFile($file_id);
    }

    /**
     * \brief Returns the complete css of the module, render critical as well
     * as non render critical.
     *
     * \retval string The complete sandboxed css of the module.
     */
    public function getCss() : string
    {
        $this->loadCssFilesIfRequired();
        return $this->sandboxedCombinedCss;
    }
}
