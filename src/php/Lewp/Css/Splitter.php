<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Css;

use \Sabberworm\CSS\Parser as CSSParser;

class Splitter
{

    private $non_render_critical_properties = [
        'animation-',
        'border',
        'border-',
        'color',
        'opacity',
        'content',
        'quotes',
        'counter-reset',
        'counter-increment',
        'list-',
        'outline',
        'outline-',
        'page-break-',
        'tab-size',
        'text-',
        'line-height',
        'vertical-align',
        'letter-spacing',
        'word-spacing',
        'white-space',
        'word-',
        'backface-visibility', // these properties might be added to the render
        'perspective', // critical array
        'perspective-',
        'transform',
        'transform-', // until this one
        'transition',
        'transition-',
        'cursor',
        'box-shadow',
        'box-decoration-break',
        'caret-color',
        'filter',
        'hyphens',
        'isolation',
        'mix-blend-mode',
        'object-',
        'scroll-behavior',
        'unicode-bidi',
        'user-select'
    ];

    private $render_critical_properties = [
        '--',
        'height',
        'max-height',
        'max-width',
        'min-height',
        'min-width',
        'width',
        'align-',
        'flex',
        'flex-',
        'font',
        'font-',
        'justify-',
        'order',
        'margin',
        'margin-',
        'column-',
        'columns',
        'padding',
        'padding-',
        'caption-side',
        'empty-cells',
        'table-layout',
        'direction',
        'display',
        'position',
        'top',
        'right',
        'bottom',
        'left',
        'float',
        'clear',
        'z-index',
        'overflow',
        'overflow-',
        'resize',
        'clip',
        'visibility',
        'box-sizing',
        'grid',
        'grid-',
        'pointer-events',
        'writing-mode',
        'background',
        'background-'
    ];

    private function filterCss(string $css_string, array $filter_array) : string
    {
        $parser = new CSSParser($css_string);
        $css_parsed = $parser->parse();
        foreach ($css_parsed->getAllRuleSets() as $ruleset) {
            foreach ($filter_array as $rule_to_remove) {
                $ruleset->removeRule($rule_to_remove);
            }
            if (empty($ruleset->getRules())) {
                //TODO: @media queries are not yet supported by sabberworm
                $css_parsed->remove($ruleset);
            }
        }
        return $css_parsed->render();
    }

    public function getRenderCriticalCss(string $css_string) : string
    {
        return $this->filterCss($css_string, $this->non_render_critical_properties);
    }

    public function getNonRenderCriticalCss(string $css_string) : string
    {
        return $this->filterCss($css_string, $this->render_critical_properties);
    }
}
