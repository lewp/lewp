<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\FileHierarchy;

use Lewp\Resolve;

/**
 * \brief Implements the module level behavior of the lewp file hierarchy.
 *
 */
trait TModuleLevel
{

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function specific() : array
    {
        return [
            'modulefile' => 'module.php',
            'configfile' => 'etc' . DIRECTORY_SEPARATOR . 'config.php',
            'css' => 'css',
            'config' => 'etc',
            'ajax' => 'ajax'
        ];
    }

    public function getPrefix() : string
    {
        return Resolve::toFilepath([
            parent::getPrefix(),
            parent::specific()['modules'],
            Resolve::idToFilepathRel($this->id)
        ]);
    }

    public function getSpecificName($specific_id)
    {
        return (!isset($this->specific()[$specific_id]))
            ? false
            : $this->specific()[$specific_id];
    }
}
