<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\FileHierarchy;

use Lewp\FileHierarchy\GlobalLevel;
use Lewp\FileHierarchy\TModuleLevel;

/**
 * \brief Implements the global module level of the lewp file hierarchy.
 *
 */
class GlobalModuleLevel extends GlobalLevel
{

    use TModuleLevel;
}
