<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\FileHierarchy;

/**
 * \brief Abstract representation of a level in the lewp file hierarchy.
 *
 * The specific folders as well as the prefix have to be defined.
 */
abstract class ALevel
{

    /**
     * \brief Defines the base folders of the hierarchy, that every level has
     * in common.
     *
     * \retval Array Key-Value pairs representing the base directory structure.
     */
    protected function base() : array
    {
        return [
            'js' => 'js',
            'phpdep' => 'lib',
            'jsdep' => 'lib' . DIRECTORY_SEPARATOR . 'js',
            'res' => 'resources',
            'res-images' => 'resources' . DIRECTORY_SEPARATOR . 'images',
            'res-fonts' => 'resources' . DIRECTORY_SEPARATOR . 'fonts',
            'res-icons' => 'resources' . DIRECTORY_SEPARATOR . 'icons',
            'res-movies' => 'resources' . DIRECTORY_SEPARATOR . 'movies',
            'res-text' => 'resources' . DIRECTORY_SEPARATOR . 'text',
            'res-json' => 'resources' . DIRECTORY_SEPARATOR . 'json',
            'res-html' => 'resources' . DIRECTORY_SEPARATOR . 'html',
            'res-l10n' => 'resources' . DIRECTORY_SEPARATOR . 'l10n',
            'var' => 'var'
        ];
    }

    /**
     * \brief Defines the specific folders of the implemented level.
     *
     * \retval Array Key-Value pairs representing the specific directory structure.
     */
    abstract protected function specific() : array;

    /**
     * \brief Defines the prefix of the specific level.
     *
     * \retval String Prefix of the specific level.
     */
    abstract public function getPrefix() : string;

    /**
     * \brief Returns the complete folder list of the specific level.
     *
     * \retval Array Key-Value pairs containing the available folders in that level.
     */
    public function getFolderList() : array
    {
        return array_merge($this->base(), $this->specific());
    }
}
