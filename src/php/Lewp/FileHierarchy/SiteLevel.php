<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\FileHierarchy;

use Lewp\FileHierarchy;
use Lewp\FileHierarchy\GlobalLevel;
use Lewp\Resolve;

/**
 * \brief Implements the site level of the lewp file hierarchy.
 *
 */
class SiteLevel extends GlobalLevel
{

    protected $top_level_domain;

    public function __construct(string $top_level_domain)
    {
        $this->top_level_domain = $top_level_domain;
    }

    protected function specific() : array
    {
        return [
            'public-root' => 'bin',
            'css' => 'css',
            'config' => 'etc',
            'pages' => 'pages',
            'modules' => 'modules'
        ];
    }

    public function getPrefix() : string
    {
        return Resolve::toFilepath(
            [
                parent::specific()['websites'],
                $this->top_level_domain
            ]
        );
    }
}
