<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\AjaxResponse;

/**
 * \brief Can be used to respond to a AJAX request from the client.
 * Communication is done using JSON in both directions.
 */
abstract class Json
{

    private $request;

    private $additional_headers;

    final public function __construct()
    {
        // read post data
        $request_data = file_get_contents('php://input');
        // try to convert it to an array
        $this->request = json_decode($request_data, true);
    }

    /**
     * \brief Returns the request parameter that have been sent.
     */
    final public function getRequest() : array
    {
        return $this->request;
    }

    /**
     * \brief Adds a header to the response. It will be send before the
     * Content-type header.
     */
    final public function addHeader(string $header)
    {
        $this->additional_headers [] = $header;
    }

    /**
     * \brief Sends the response to the server.
     */
    final public function sendResponse()
    {
        foreach ($this->additional_headers as $header) {
            header($header);
        }
        header("Content-type: application/json");
        echo json_encode($this->respond());
    }

    /**
     * \brief Needs to be implemented by the user. Its return value is being
     * send to the client.
     */
    abstract public function respond() : array;
}
