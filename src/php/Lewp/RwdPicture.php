<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\FileHierarchy;
use Lewp\Interfaces\IFileHierarchy;
use Lewp\Resolve;
use Lewp\Utilities\FileChangeTracker;
use Lewp\VarFolder;

/**
 * \brief Optimizes images for responsive web design and is able to create a
 * <picture> element containing the optimized images.
 *
 */
class RwdPicture
{

    const SCALING_FACTOR = 0.1;

    private $temporary_image_folder_id;

    private $module_id;

    private $alt_text;

    private $error = false;

    private $optimized_images_count = 10;

    /**
     * \brief Associative array containing the details of the landscape image.
     * The following entries are available:
     * id
     * path
     * hierarchy_level
     * var_folder
     * height
     * width
     */
    private $landscape;

    /**
     * \brief See $landscape variable.
     */
    private $portrait;

    private $file_hierarchy;

    public function __construct(
        IFileHierarchy $file_hierarchy,
        string $landscape_id,
        string $portrait_id,
        string $module_id = '',
        int $optimized_images_count = 10,
        string $alt_text = ''
    ) {

        $this->module_id = $module_id;

        $this->file_hierarchy = $file_hierarchy;

        $this->temporary_image_folder_id = 'tmp' . Resolve::ID_SEPARATOR . 'rwdpicture';
        $this->alt_text = $alt_text;
        $this->optimized_images_count = $optimized_images_count;
        // find landscape image
        $this->landscape['id'] = $landscape_id;
        $this->landscape['path'] = $this->file_hierarchy->findFile(
            'res-images',
            Resolve::idToFilepathRel($this->landscape['id']),
            $this->file_hierarchy->factoryLevelArray([], $this->module_id)
        );
        if ($this->landscape['path'] === false) {
            trigger_error(
                'RwdPicture: Landscape image "'
                . $this->landscape['id']
                . '" could not be found!',
                E_USER_ERROR
            );
            $this->error = true;
            return false;
        }
        $this->landscape['var_folder'] = new VarFolder(
            $this->file_hierarchy,
            $this->module_id
        );
        $this->landscape['filechangetracker'] = new FileChangeTracker(
            $this->file_hierarchy,
            $this->module_id,
            'res-images',
            $this->landscape['id']
        );
        // find portrait image
        $this->portrait['id'] = $portrait_id;
        $this->portrait['path'] = $this->file_hierarchy->findFile(
            'res-images',
            Resolve::idToFilepathRel($this->portrait['id']),
            $this->file_hierarchy->factoryLevelArray([], $this->module_id)
        );
        if ($this->portrait['path'] === false) {
            trigger_error(
                'RwdPicture: Portrait image "'
                . $this->portrait['id']
                . '" could not be found!',
                E_USER_ERROR
            );
            $this->error = true;
            return false;
        }
        $this->portrait['var_folder'] = new VarFolder(
            $this->file_hierarchy,
            $this->module_id
        );
        $this->portrait['filechangetracker'] = new FileChangeTracker(
            $this->file_hierarchy,
            $this->module_id,
            'res-images',
            $this->portrait['id']
        );

        $this->allowPublicAccess('landscape');
        $this->allowPublicAccess('portrait');
        // increase execution time temporarily
        set_time_limit(240);
        $this->createScaledImagesIfRequired('landscape');
        $this->createScaledImagesIfRequired('portrait');
    }

    private function getSha1Id($image_orientation)
    {

        $image = $this->getImage($image_orientation);
        return Resolve::arrayToId([
            $this->temporary_image_folder_id,
            $image['id'] . '.sha1'
        ]);
    }

    private function getScaledId($image_orientation, $scaling_factor)
    {

        $image = $this->getImage($image_orientation);
        $info = pathinfo($image['id']);
        return Resolve::arrayToId([
            $this->temporary_image_folder_id,
            $info['filename'] . strval($scaling_factor * 100) . '.' . $info['extension']
        ]);
    }

    private function createSteps()
    {

        return range(1, $this->optimized_images_count - 1);
    }

    private function allowPublicAccess($image_orientation)
    {

        $image = $this->getImage($image_orientation);
        $var_dir = Resolve::findFolderId(
            $this->temporary_image_folder_id . Resolve::ID_SEPARATOR .
                $image['id']
        );
        $image['var_folder']->grantPublicAccess($var_dir);
        return true;
    }

    private function getImage($image_orientation)
    {

        $image = $this->landscape;
        switch ($image_orientation) {
            case 'landscape':
                $image = $this->landscape;
                break;
            case 'portrait':
                $image = $this->portrait;
                break;
        }
        return $image;
    }

    private function loadSourceImage($image_orientation)
    {

        $image = $this->getImage($image_orientation);

        $info = pathinfo($image['path']);
        $ext = ($info['extension'] === 'jpeg') ? 'jpg' : $info['extension'];
        switch ($ext) {
            case 'bmp':
                $new_image = imagecreatefromwbmp($image['path']);
                break;
            case 'gif':
                $new_image = imagecreatefromgif($image['path']);
                break;
            case 'jpg':
                $new_image = imagecreatefromjpeg($image['path']);
                break;
            case 'png':
                $new_image = imagecreatefrompng($image['path']);
                break;
            default:
                $new_image = false;
                break;
        }
        return ['resource' => $new_image, 'type' => $ext];
    }

    private function scaleAndSaveImage($image_orientation, $scaling_factor)
    {

        $image = $this->getImage($image_orientation);

        $scaled_filename = $this->getScaledId($image_orientation, $scaling_factor);
        $scaled_handle = $image['var_folder']->openFile($scaled_filename, 'wb');
        $original = $this->loadSourceImage($image_orientation);
        list($width, $height) = getimagesize($image['path']);
        $scaled_width = intval($width * $scaling_factor);
        $scaled_height = intval($height * $scaling_factor);
        $scaled_image = imagecreatetruecolor($scaled_width, $scaled_height);
        // preserve transparency
        if ($original['type'] === 'gif' ||
            $original['type'] === 'png'
        ) {
            imagecolortransparent(
                $scaled_image,
                imagecolorallocatealpha($scaled_image, 0, 0, 0, 127)
            );
            imagealphablending($scaled_image, false);
            imagesavealpha($scaled_image, true);
        }

        if ($original['resource'] === false) {
            return false;
        }
        imagecopyresampled(
            $scaled_image,
            $original['resource'],
            0,
            0,
            0,
            0,
            $scaled_width,
            $scaled_height,
            $width,
            $height
        );
        switch ($original['type']) {
            case 'bmp':
                imagewbmp($scaled_image, $scaled_handle);
                break;
            case 'gif':
                imagegif($scaled_image, $scaled_handle);
                break;
            case 'jpg':
                imagejpeg($scaled_image, $scaled_handle);
                break;
            case 'png':
                imagepng($scaled_image, $scaled_handle);
                break;
            default:
                return false;
        }
        $image['var_folder']->closeFile($scaled_handle);

        return true;
    }

    private function sourceImageChanged($image_orientation)
    {
        $image = $this->getImage($image_orientation);
        return $image['filechangetracker']->hasStateChanged();
    }

    private function createScaledImagesIfRequired($image_orientation)
    {

        $image = $this->getImage($image_orientation);
        if (!$this->sourceImageChanged($image_orientation)) {
            // check if all scaled are available
            foreach ($this->createSteps() as $step) {
                if (!$image['var_folder']->fileExists(
                    $this->getScaledId($image_orientation, $step * self::SCALING_FACTOR)
                )
                || ($image['var_folder']->getFileSize(
                    $this->getScaledId($image_orientation, $step * self::SCALING_FACTOR)
                ) === 0)
                ) {
                    $this->scaleAndSaveImage($image_orientation, $step * self::SCALING_FACTOR);
                }
            }
            return false;
        }

        foreach ($this->createSteps() as $step) {
            $this->scaleAndSaveImage($image_orientation, $step * self::SCALING_FACTOR);
        }
        return true;
    }

    public function getPictureTag($wrapped_by_dom_document = false)
    {

        $dom = new \DOMDocument();
        $picture = $dom->createElement('picture');

        if ($this->error === true) {
            return false;
        }

        foreach (['landscape', 'portrait'] as $orientation) {
            $image = $this->getImage($orientation);
            list($width, $height) = getimagesize($image['path']);
            foreach ($this->createSteps() as $step) {
                $source = $dom->createElement('source');
                $link_array = [ UrlPaths::VAR_FOLDER ];
                if ($this->module_id !== '') {
                    $link_array[] = $this->module_id;
                }
                $link_array[] = $this->getScaledId(
                    $orientation,
                    $step * self::SCALING_FACTOR
                );
                $link = UrlPaths::implodeToAbsolute($link_array);

                $source->setAttribute('srcset', $link);
                $scaled_height = $height * ($step * self::SCALING_FACTOR);
                $scaled_width = $width * ($step * self::SCALING_FACTOR);
                switch ($orientation) {
                    case 'portrait':
                        $media = '(max-height: ' . strval($scaled_height) . 'px)';
                        break;
                    case 'landscape':
                    default:
                        $media = '(max-width: ' . strval($scaled_width) . 'px)';
                        break;
                }
                $media .= ' and (orientation: ' . $orientation . ')';
                $source->setAttribute('media', $media);
                $picture->appendChild($source);
            }
        }

        // link mapping to original images
        $link_mapping = ['res', 'images'];

        // original portrait
        if ($this->module_id !== '') {
            $link_mapping[] = $this->module_id;
        }
        $link_mapping[] = $this->portrait['id'];

        $original_portrait = $dom->createElement('source');
        $original_portrait->setAttribute('srcset', UrlPaths::implodeToAbsolute($link_mapping));
        $original_portrait->setAttribute('media', '(orientation: portrait)');
        $picture->appendChild($original_portrait);

        // fallback image, landscape original
        $link_mapping[sizeof($link_mapping) - 1] = $this->landscape['id'];

        $fallback = $dom->createElement('img');
        $fallback->setAttribute('src', UrlPaths::implodeToAbsolute($link_mapping));
        $fallback->setAttribute('alt', $this->alt_text);
        $picture->appendChild($fallback);

        $dom->appendChild($picture);

        if ($wrapped_by_dom_document === true) {
            return $dom;
        }

        return $picture;
    }

    public function setAltText(string $alt_text)
    {
        $this->alt_text = $alt_text;
    }
}
