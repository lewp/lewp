<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\{
    Config,
    Css\Processor as CssProcessor,
    Dependencies,
    FileHierarchy\GlobalLevel,
    FileHierarchy\SiteLevel,
    FileHierarchy\GlobalModuleLevel,
    FileHierarchy\SiteModuleLevel,
    Html5DomDocument,
    Interfaces\IFileHierarchy,
    Js\Processor as JsProcessor,
    Resolve,
    VarFolder,
    Websocket\Message as WsMessage
};
use \W3C\HTMLEntities;

/**
 * \brief Representation of a module that can be included in a webpage.
 *
 */
abstract class Module extends Html5DomDocument
{

    /**
     * \brief Defines the classname of the wrapping HTML tag.
     */
    const HTML_WRAPPER_CLASS = 'modulewrapper';
    /**
     * \brief Contains the img src. Will be copied to the src attribute if
     * deferred image loading is enabled.
     */
    const HTML_ATTR_DATA_DEFERRED_IMAGE_SRC = 'data-lewp-deferred-image-src';

    const HTML_ATTR_DATA_FOOTPRINT = 'data-lewp-footprint';

    /**
     * \brief Contains the module id.
     */
    private $module_id;

    /**
     * \brief Contains the optional site id. Required for loading the configuration
     * of the page as well.
     */
    private $page_id = '';

    /**
     * \brief Contains the module configuration, defined in the configuration
     * file.
     */
    protected $configuration = null;

    /**
     * \brief Contains the dependency object of the module.
     */
    private $dependencies = null;

    /**
     * \brief After the run function has been called, this variable contains the
     * modules that this module depends on. It is used to check if there are
     * unused dependencies in the configuration.
     */
    private $used_dependencies = [];

    /**
     * \brief Contains all css file names of the module at initialization.
     */
    private $css_file_names = [];

    /**
     * \brief Contains all js file names of the module at initialization.
     */
    private $js_file_names = [];

    /**
     * \brief Contains the language of the module.
     */
    private $language = '';

    /**
     * \brief The file hierarchy instance.
     */
    protected $file_hierarchy;

    /**
     * \brief A lewp session instance.
     */
    private $session = null;

    private $vendor_autoloader = null;

    private $module_register;

    /**
     * \brief Stores how often the module has been executed yet.
     */
    private $execution_count = 0;

    final public function __construct()
    {
        parent::__construct();
    }

    final public function __invoke(string $key, string $default): string
    {
        $language = ($this->getLanguage() !== '') ? $this->getLanguage() : 'en';
        return $this->file_hierarchy->getL10n($this->getModuleId(), $language, $key, $default);
    }

    final public function registerAutoloader()
    {
        // add vendor autoload on module level if available
        $vendor_autoload_file = dirname((new \ReflectionClass($this))->getFileName()) . "/vendor/autoload.php";
        if (file_exists($vendor_autoload_file)) {
            $this->vendor_autoloader = require $vendor_autoload_file;
            $this->vendor_autoloader->register(true);
        }

        // register autoloader for lib folder
        spl_autoload_register([$this, 'autoloader'], true, true);
    }

    final public function unregisterAutoloader()
    {

        if (!is_null($this->vendor_autoloader)) {
            $this->vendor_autoloader->unregister();
        }

        spl_autoload_unregister([$this, 'autoloader']);
    }

    public function initialize(IFileHierarchy $file_hierarchy)
    {

        $this->file_hierarchy = $file_hierarchy;
        // get module id
        $this->module_id = $this->resolveModuleId();

        $this->configuration = new Config($this->file_hierarchy, $this->page_id, $this->module_id);

        $this->dependencies = new Dependencies($this->file_hierarchy, $this->module_id);

        $this->css_file_names =
            $this->file_hierarchy->collectCssFilenames($this->module_id);
        $this->js_file_names =
            $this->file_hierarchy->collectJavascriptFilenames($this->module_id);


        $this->appendWrapperElement();
    }

    private function appendWrapperElement() : bool
    {
        // create wrapper element
        $modulewrap = $this->createElement(
            $this->configuration->getValue(Config::KEY_WRAPPER_MODULE_ELEMENT_TYPE)
        );
        $modulewrap->setAttribute('class', self::HTML_WRAPPER_CLASS);
        // set data attributes required for javascript/runtime loading and unloading

        $html_footprint = htmlentities($this->getFootprint(), ENT_QUOTES);

        $html_footprint = preg_replace_callback("/(&[a-z]+;)/", function ($m) {
            return HTMLEntities::getNumeric($m[0]);
        }, $html_footprint);
        $modulewrap->setAttribute(
            self::HTML_ATTR_DATA_FOOTPRINT,
            $html_footprint
        );
        $this->seal($modulewrap);
        parent::appendChild($modulewrap); // append it to the document
        return true;
    }

    /**
     * \brief Checks if all modules that have been specified in the configuration
     * file were used by the run method. If not, a warning will be shown.
     *
     * \retval Returns true if all dependencies have been used.
     */
    private function checkForUnusedDependencies() : bool
    {
        // check if there are unused modules in the dependency configuration
        $config_deps = $this->configuration->getValue(
            Config::KEY_DEPENDENCIES
        )['modules'];
        $used_deps = $this->used_dependencies;
        $diffs = array_diff($config_deps, $used_deps);
        if (sizeof($diffs) > 0) {
            trigger_error(
                'There are modules in the ' . Config::KEY_DEPENDENCIES .
                ' configuration key that are unused! Remove the following id\'s' .
                ' from the array to increase performance: ' .
                implode(', ', $diffs),
                E_USER_WARNING
            );
            return false;
        }
        return true;
    }

    /**
     * \brief Checks site and global level for the module folder to exist in
     * the file that called the function.
     * If this function fails, please check the backtrace if it uses the wrong
     * index.
     *
     * \retval array First value is the backtrace filepath, second is the base
     * path of the filehierarchy that has been found in it.
     */
    final protected function findModuleBasePaths() : array
    {
        // get filename of calling file
        //$trace = debug_backtrace();
        // index 2 because there are two files in between, the first is the page,
        // second is the module definition file itself
        // upper comment needs to be verified!
        //$filename = $trace[2]['file'];
        $filename = (new \ReflectionObject($this))->getFileName();
        $retval = [$filename];
        $site_path = $this->file_hierarchy->generatePath(new SiteLevel($this->file_hierarchy->getTLD()), 'modules');
        $global_path = $this->file_hierarchy->generatePath(new GlobalLevel(), 'modules');
        //$site_path = FileHierarchy::generatePath(new SiteLevel(), 'modules');
        //$global_path = FileHierarchy::generatePath(new GlobalLevel(), 'modules');
        if (($site_path !== false) && (strpos($filename, $site_path) === 0)) {
            $retval [] = $site_path;
        } elseif (($global_path !== false) && (strpos($filename, $global_path) === 0)
        ) {
            $retval [] = $global_path;
        } else {
            $retval [] = false;
        }
        return $retval;
    }

    /**
     * \brief Determines all file ids in the given resource folder. If a
     * subfolder of the resource folder should be crawled, it can be given by
     * the folder id parameter.
     *
     * \param string $resource_id The resource folder id as it is specified in
     *                            the file hierarchy.
     * \param string $folder_id The subfolder of the resource folder.
     * \param string $pattern The pattern of the files such as '*.jpg'. Defaults to '*'.
     */
    final protected function collectResourceFileIds(
        string $resource_id,
        string $folder_id,
        string $pattern = '*'
    ) : array {
        return $this->file_hierarchy->collectResourceFileIds(
            $this->module_id,
            $resource_id,
            $folder_id,
            $pattern
        );
    }

    /**
     * \brief Determines the module id by finding the module definition file
     * and the modules folder in the filename string.
     *
     * \retval string The identified module id.
     */
    final protected function resolveModuleId() : string
    {
        $paths = $this->findModuleBasePaths();
        // subtract the base path from the calling module file
        $relative_module_filename = substr(
            $paths[0],
            strlen($paths[1]) + 1,
            strlen($paths[0]) - 1
        );
        // get length of module filename
        $modulefile_len = strlen( // the level does not matter in this case
            (new GlobalModuleLevel(null))->getSpecificName('modulefile')
        );
        // subtract module filename
        $module_id = substr(
            $relative_module_filename,
            0,
            strlen($relative_module_filename) - $modulefile_len - 1
        );
        // replace all directory separator with dashes
        return Resolve::filepathRelToId($module_id);
    }

    /**
     * \brief Automatically loads classes that are stored in the php dependency
     * folders of the file hierarchy.
     *
     * \param string $class_name The class name that is being searched for.
     *
     * \retval bool True if successful.
     */
    final public function autoloader($class_name) : bool
    {
        $class_name = ltrim($class_name, '\\');
        $file_name  = '';
        $namespace = '';
        if ($last_ns_pos = strrpos($class_name, '\\')) {
            $namespace = substr($class_name, 0, $last_ns_pos);
            $class_name = substr($class_name, $last_ns_pos + 1);
            $file_name  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';

        $file_path = $this->file_hierarchy->findFile(
            'phpdep',
            $file_name,
            $this->file_hierarchy->factoryLevelArray([], $this->getModuleId())
        );
        if ($file_path !== false) {
            require $file_path;
            return true;
        }
        return false;
    }

    /**
     * \brief Returns the module id.
     *
     * \retval string The module id.
     */
    final public function getModuleId() : string
    {
        return $this->module_id;
    }

    /**
     * \brief Returns the name of the module.
     *
     * \retval string The module name.
     */
    final public function getModuleName() : string
    {
        return end(explode('_', $this->module_id));
    }

    /**
     * \brief Returns the page id if set.
     *
     * \retval string The page id, an empty string if not set.
     */
    final public function getPageId() : string
    {
        return $this->page_id;
    }

    /**
     * \brief Returns the top level domain.
     *
     * \retval string The top level domain.
     */
    final public function getTLD() : string
    {
        return $this->file_hierarchy->getTLD();
    }

    ///**
    // * \brief Returns configuration array of the module. It will contain the
    // * configuration of the site, page (if set), and general module.
    // *
    // * \param bool $reload If set to true, the configuration will be reloaded.
    // *
    // * \retval array The configuration array.
    // */
    //final public function getConfiguration() : Config
    //{
    //    return $this->configuration;
    //}

    /**
     * \brief Sets the page id. Depending on the page, the module is able to
     * behave differently. The configuration will be reloaded on executing this
     * function.
     *
     * \param string $page_id The page id.
     *
     * \retval bool Always true.
     *
     */
    final public function setPageId(string $page_id) : bool
    {
        $this->page_id = $page_id;
        $this->configuration = new Config($this->file_hierarchy, $this->page_id, $this->module_id);
        return true;
    }

    final public function loadTextFile(string $file_id): string
    {
        return $this->file_hierarchy->loadTextFile($this->module_id, $file_id);
    }

    final public function loadJsonFile(string $file_id, bool $asArray = true)
    {
        return $this->file_hierarchy->loadJsonFile($this->module_id, $file_id, $asArray);
    }

    final public function loadResourceFile(string $file_id) : string
    {
        return $this->file_hierarchy->loadResourceFile($this->module_id, $file_id);
    }

    /**
     * \brief Loads the given HTML file from the resource directory and appends
     * it to the module if $append set to true. If not, an array containing the
     * DOMNode objects will be returned.
     *
     * \param string $file_id The HTML file id. The extension will always be
     * .html. Will be appended if not included in the id,.
     *
     * \param array $options Set $options['append'] to true if the file should
     * be directly appended to the document.
     *
     * \retval mixed False on fail. Otherwise and array containing the imported
     * DOMNode objects.
     */
    final public function loadHtmlFile($file_id, $options = null)
    {
        $default_options = [
            'append' => false,
        ];
        $options = $options ?? $default_options;
        $options += $default_options;

        $file_content = $this->file_hierarchy->loadHtmlFile($this->module_id, $file_id);

        // load html file into DOMDocument
        $tmp_dom = new \DOMDocument();
        // check encoding
        $encoding = $this->configuration->getValue('encoding');
        $prefix = '<?xml encoding="' . $encoding . '" ?>' . '<div id="appended-html-file">';
        $suffix = '</div>';
        if (@$tmp_dom->loadHTML($prefix . $file_content . $suffix) === false) {
            return false;
        }

        // import into module
        $tmp_children = $tmp_dom->getElementById('appended-html-file')->childNodes;
        $imported_children = [];

        foreach ($tmp_children as $child) {
            // import them and append to document if requested
            $imported_children [] = $this->importNode($child, true);
            if ($options['append'] === true) {
                $this->appendChild(
                    $imported_children[sizeof($imported_children) - 1]
                );
            }
        }
        return $imported_children;
    }

    /**
     * \brief Appends the given DOMNode to the module.
     * Calls the seal function before the node is being added to the DOM.
     *
     * \params DOMNode $newChild The node to be added.
     *
     * \retval DOMNode The added node.
     */
    final public function appendChild(\DOMNode $newChild) : \DOMNode
    {
        $newChild = $this->importNode($newChild, true);
        $this->seal($newChild);
        return $this->firstChild->appendChild($newChild);
    }

    /**
     * \brief Adds to the given DOMNode and to all its childs the class attribute
     * to be able to identify and access the module by CSS or JavaScript.
     *
     * \params DOMNode $dom_node
     *
     * \retval bool True if class attribute has been added.
     */
    final public function seal(\DOMNode &$dom_node) : bool
    {
        if (!method_exists($dom_node, 'getAttribute')) {
            return false;
        }

        foreach ($dom_node->childNodes as $child) {
            if (empty(trim($dom_node->getAttribute(self::HTML_ATTR_DATA_FOOTPRINT)))) {
                // seal only children that are NOT a module
                $this->seal($child);
            }
        }

        $class_attribute = $dom_node->getAttribute('class');
        if ($class_attribute !== '') {
            $classes = explode(' ', $class_attribute);
        } else {
            $classes = [];
        }
        $classes_to_add = [
            //Resolve::stringToCssClassName(FileHierarchy::getTLD()),
            Resolve::stringToCssClassName($this->module_id)
        ];
        foreach ($classes_to_add as $c) {
            if (!in_array($c, $classes)) {
                $classes[] = $c;
            }
        }
        $dom_node->setAttribute('class', implode(' ', $classes));

        return true;
    }

    /**
     * \brief Appends a module to the current one and adds it to the dependency list.
     *
     * \param string $module_id The module to be appended.
     *
     * \retval bool True if module has been added successfully.
     */
    final public function appendModule(string $module_id, array $options = [])
    {
        // prevent to append itself to itself which would end in an endless loop
        if ($this->module_id === $module_id) {
            return false;
        }
        if (is_null($this->configuration)) {
            trigger_error(
                'Cannot append module ' . $module_id . ' to ' . $this->module_id .
                '! Configuration object is not available!',
                E_USER_ERROR
            );
        }
        $module_deps = $this->configuration->getValue(
            Config::KEY_DEPENDENCIES
        );
        if (($module_deps === false)
            || (!array_key_exists('modules', $module_deps))
            || (array_search($module_id, $module_deps['modules']) === false)
        ) {
            trigger_error(
                'Trying to append module ' . $module_id . ' to ' . $this->module_id .
                ' but it was not found in the configuration file.',
                E_USER_ERROR
            );
        }
        $module = $this->module_register->run($module_id, $options);

        foreach ($module->childNodes as $child) {
            $this->firstChild->appendChild($this->importNode($child, true));
        }

        if (array_search($module_id, $this->used_dependencies) === false) {
            $this->used_dependencies[] = $module_id;
        }

        return $module;
    }

    /**
     * \brief Adds the given css class to the module, but only to the wrapper
     * node.
     *
     * \param string $css_class The class to be added.
     *
     * \retval bool False if the getAttribute method does not exist.
     */
    final public function addClass(string $css_class) : bool
    {
        if (!method_exists($this->firstChild, 'getAttribute')) {
            return false;
        }
        $current_classes = explode(' ', $this->firstChild->getAttribute('class'));
        if (in_array($css_class, $current_classes)) {
            return true;
        }
        $current_classes[] = $css_class;
        $this->firstChild->setAttribute('class', implode(' ', $current_classes));
        return true;
    }

    /**
     * \brief Returns the footprint of the given module in JSON format.
     *
     * \param string $module_id The module id for which the footprint is requested.
     *
     * \retval string The footprint of the module in json format.
     */
    final public function getFootprint() : string
    {
        return json_encode(array(
            'dep' => $this->dependencies->getModuleDependencies(),
            'jsdep' => $this->dependencies->getJavascriptDependencies(),
            'serverId' => $this->module_id,
            'clientId' => Resolve::idToJavascript($this->module_id),
            'hasJavascript' => $this->hasJavascript(),
            'hasCss' => $this->hasCss(),
            'lazyLoading' => $this->configuration->getValue(Config::KEY_ENABLE_LAZY_LOADING)
        ));
    }

    /**
     * \brief Sends the footprint of the given module in JSON format to the browser.
     *
     * \retval bool Always true.
     */
    final public function sendFootprint() : bool
    {
        header('Content-Type: application/json');
        header('Vary: Accept-Encoding');
        echo $this->getFootprint();
        return true;
    }

    /**
     * \brief Used to check if the module has JavaScript files.
     *
     * \retval bool True if there are files available.
     */
    final public function hasJavascript() : bool
    {
        return !empty($this->js_file_names);
    }

    /**
     * \brief Combines all JavaScript files that belong to the module and
     * prepares them as a function that is stored in the lewp namespace.
     *
     * \retval string The combined and sandboxed JavaScript.
     */
    final public function getJavascript() : string
    {
        $footprint_string =
            'lewp.footprints.add('
            . $this->getFootprint()
            . ');';
        if (!$this->hasJavascript()) {
            return $footprint_string;
        }
        // prepare sandboxing variables
        $prefix = '';
        $suffix = '';
        // create a function to be able to initialize module during runtime
        $prefix .= 'lewp.initFunctions.set("' . Resolve::idToJavascript($this->module_id) .
            '", function() {';
        $suffix .= '});';

        $prefix .= 'const self = this;';
        // enable direct communication with parent module
        $parent_id = Resolve::toParentId($this->module_id);
        if ($parent_id !== "") {
            $reference = 'lewp.modules.' . Resolve::idToJavascript($parent_id);
            $prefix .= 'const parentModule = '
                . $reference
                . ' && '
                . $reference
                . '.slice(-1)[0];';
        }

        $suffix .= $footprint_string;

        $js_processor = new JsProcessor(
            $this->file_hierarchy,
            $this->module_id,
            $this->js_file_names
        );
        return $prefix . $js_processor->getJs() . $suffix;
    }

    /**
     * \brief Used to check if the module has CSS files.
     *
     * \retval bool True if there are files available.
     */
    final public function hasCss() : bool
    {
        return !empty($this->css_file_names);
    }


    /**
     * \brief Combines all CSS files that belong to the module.
     *
     * \retval string The combined CSS.
     */
    final public function getCss() : string
    {
        if (!$this->hasCss()) {
            return '';
        }
        $processor = new CssProcessor($this->file_hierarchy, $this->module_id, $this->css_file_names);
        return $processor->getCss();
    }

    final public function getRenderCriticalCss() : string
    {
        if (!$this->hasCss()) {
            return '';
        }
        $css_processor = new CssProcessor($this->file_hierarchy, $this->module_id, $this->css_file_names);
        return $css_processor->getRenderCriticalCss();
    }

    final public function getNonRenderCriticalCss() : string
    {
        if (!$this->hasCss()) {
            return '';
        }
        $css_processor = new CssProcessor($this->file_hierarchy, $this->module_id, $this->css_file_names);
        return $css_processor->getNonRenderCriticalCss();
    }

    /**
     * \brief Alias for $this->getConfiguration->getValue($key).
     *
     * \param $key mixed The key that is being requested from the configuration.
     *
     * \retval mixed The value of the requested key.
     */
    final public function getConfigValue($key)
    {
        return $this->configuration->getValue($key);
    }

    /**
     * \brief Returns the HTML string of the module.
     *
     * \retval string The HTML string that has been produced by the run function.
     */
    final public function render() : string
    {
        $this->checkForUnusedDependencies();
        return $this->saveHTML();
    }

    /**
     * \brief Set the language of the module. It needs to be a string consisting
     * of two characters. Otherwise the function will not set the language.
     *
     * \param $language string A two digit identification of the language
     *
     * \retval Module Itself.
     */
    final public function setLanguage(string $language)
    {
        if (strlen($language) !== 2) {
            return $this;
        }
        $this->language = $language;
        return $this;
    }

    /**
     * \brief Gets the language of the module.
     *
     * \retval string The language of the module, an empty string if not set.
     */
    final public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * \brief DEPRECATED! Use createElement instead! Shorthand function to createElement and setAttribute afterwards.
     *
     * \param string $tag_name The name of the tag that will bei created.
     * \param string $initial_text The initial nodeValue.
     * \param array $attributes_values A list of key value pairs where the key
     * the resulting attribute and the value the value of it.
     *
     * \retval \DOMElement The created element.
     */
    final protected function createAndSetupElement(
        $tag_name,
        $initial_text = '',
        $attributes_values = []
    ) {
        return $this->createElement($tag_name, $initial_text, $attributes_values);
    }

    /**
     * \brief Runs through all img elements and saves the src attribute to
     * the one declared in self::HTML_ATTR_DATA_DEFERRED_IMAGE_SRC. Then the
     * src attribute will be set to a 1x1 fully transparent base64 image.
     * On the client side, the self::HTML_ATTR_DATA_DEFERRED_IMAGE_SRC
     * attribute is being copied to the src attribute again to show the actual
     * image.
     *
     * \retval bool Always true.
     */
    final public function deferImages() : bool
    {
        if (!$this->configuration->getValue(Config::KEY_DEFER_IMAGES)) {
            return true;
        }
        $images = $this->querySelectorAll(Resolve::stringToCssClassName($this->getModuleId()) . ' img');
        foreach ($images as $img) {
            if (!$img->hasAttribute('src')) {
                continue;
            }
            $img->setAttribute(
                self::HTML_ATTR_DATA_DEFERRED_IMAGE_SRC,
                $img->getAttribute('src')
            );
            $img->setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
        }
        return true;
    }

    private function callerIsModuleRegister()
    {
        $trace = debug_backtrace();
        $i = 1;
        do {
            $class = $trace[$i++]['class'] ?? null;
        }
        while (isset($class) && $class === self::class);

        return (isset($class) && ($class === ModuleRegister::class));
    }

    final public function setSession(\Lewp\Session $session)
    {
        if (!empty($this->session)) {
            return;
        }
        $this->session = $session;
    }

    final public function getSession() : \Lewp\Session
    {
        return $this->session;
    }

    final public function increaseExecutionCount(): bool
    {
        if (!$this->callerIsModuleRegister()) {
            return false;
        }
        $this->execution_count++;
        return true;
    }

    final public function getExecutionCount() : int
    {
        return $this->execution_count;
    }

    final public function setModuleRegister(ModuleRegister &$module_register)
    {
        $this->module_register = $module_register;
    }

    final public function resetHtml()
    {
        if (!$this->callerIsModuleRegister()) {
            return false;
        }

        if ($this->getExecutionCount() > 0) {
            return parent::resetHtml();
        }
    }

    /**
     * \brief Defines the behavior of the module.
     *
     * \param $options array The arguments that can be passed to the function.
     *
     * \retval bool False if something failed during execution.
     */
    abstract public function run(array $options = []) : bool;

    /**
     * \brief Defines the behavior of the module for a request through the
     * websocket server.
     */
    abstract public function onWebsocketRequest(WsMessage $message);
}
