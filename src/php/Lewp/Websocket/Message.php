<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Websocket;

class Message
{

    const PARAM_MESSAGE_ID = "messageId";
    const PARAM_MODULE_ID = "moduleId";
    const PARAM_PAGE_ID = "pageId";
    const PARAM_PAYLOAD = "payload";

    private $is_valid = false;

    private $message_id = 0;

    private $module_id;

    private $page_id;

    private $payload = false;

    public function __construct(
        int $message_id = 0,
        string $module_id = '',
        string $page_id = '',
        $payload = false
    ) {
        $this->message_id = $message_id;
        $this->module_id = $module_id;
        $this->page_id = $page_id;
        $this->payload = $payload;
        $this->validateMessage();
    }

    public function setMessageId(int $id)
    {
        $this->message_id = $id;
    }

    public function getMessageId() : int
    {
        var_dump($this->message_id);
        return $this->message_id;
    }

    public function isValid() : bool
    {
        return $this->is_valid;
    }
    public function getModuleId() : string
    {
        return ($this->is_valid) ? $this->module_id : '';
    }

    public function getPageId() : string
    {
        return ($this->is_valid) ? $this->page_id : '';
    }

    public function getPayload()
    {
        return ($this->is_valid) ? $this->payload : '';
    }

    private function validateMessage() : bool
    {
        if (empty($this->module_id) || !is_string($this->module_id)) {
            return false;
        }
        if (!empty($this->page_id) && !is_string($this->page_id)) {
            return false;
        }
        $this->is_valid = true;
        return true;
    }

    public function encode() : string
    {
        if (!$this->is_valid) {
            return '';
        }
        $message = [
            self::PARAM_MESSAGE_ID => $this->message_id,
            self::PARAM_MODULE_ID => $this->module_id,
            self::PARAM_PAGE_ID => $this->page_id,
            self::PARAM_PAYLOAD => $this->payload
        ];
        return json_encode($message);
    }

    public function decode($message) : bool
    {
        $decoded_message = json_decode($message, true);
        if (!array_key_exists(self::PARAM_MODULE_ID, $decoded_message)) {
            return false;
        }
        $this->message_id = $decoded_message[self::PARAM_MESSAGE_ID];
        $this->module_id = $decoded_message[self::PARAM_MODULE_ID];

        if (array_key_exists(self::PARAM_PAGE_ID, $decoded_message)) {
            $this->page_id = $decoded_message[self::PARAM_PAGE_ID];
        }

        if (array_key_exists(self::PARAM_PAYLOAD, $decoded_message)) {
            $this->payload = $decoded_message[self::PARAM_PAYLOAD];
        }
        $this->validateMessage();
        return true;
    }
}
