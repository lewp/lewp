<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp;

use Lewp\{
    ErrorHandler,
    FileHierarchy\ALevel,
    FileHierarchy\GlobalLevel,
    FileHierarchy\GlobalModuleLevel,
    FileHierarchy\SiteLevel,
    FileHierarchy\SiteModuleLevel,
    Interfaces\IFileHierarchy,
    Resolve
};

/**
 * \brief Main file hierarchy class. Used to initialize the hierarchy and holds
 * the main information about it.
 */
class FileHierarchy implements IFileHierarchy
{

    const LEVEL_GLOBAL = 1;
    const LEVEL_SITE = 2;
    const LEVEL_GLOBAL_MODULE = 3;
    const LEVEL_SITE_MODULE = 4;

    /**
     * \brief True if initialize() function has been called.
     */
    private $is_initialized = false;

    /**
     * \brief Top level domain of the website.
     */
    private $top_level_domain = '';

    /**
     * \brief Filepath to the root folder of the file hierarchy.
     */
    private $root_folder = '';

    public function __construct($root_folder, $top_level_domain)
    {
        $this->initialize($root_folder, $top_level_domain);
    }

    public function isInitialized() : bool
    {
        return $this->is_initialized;
    }

    public function getTLD() : string
    {
        return $this->top_level_domain;
    }

    public function getRootFolder() : string
    {
        return $this->root_folder;
    }

    /**
     * \brief Initializes all member of the file hierarchy.
     *
     * \param string $root_folder The absolute filepath of the root folder of
     * the file hierarchy.
     *
     * \param string $top_level_domain The top level domain of the website.
     *
     * \retval Boolean False on fail.
     */
    private function initialize($root_folder, $top_level_domain) : bool
    {
        $root_folder = realpath($root_folder);
        if (!$root_folder) {
            return false;
        }

        $this->root_folder = $root_folder;
        $this->top_level_domain = $top_level_domain;
        $this->is_initialized = true;

        ErrorHandler::initialize();

        return true;
    }

    public function factoryLevelArray(array $level_numbers, string $module_id = '')
    {
        if (empty($level_numbers)) {
            return [
                new SiteModuleLevel($this->getTLD(), $module_id),
                new GlobalModuleLevel($module_id),
                new SiteLevel($this->getTLD()),
                new GlobalLevel()
            ];
        }
        $levels = [];
        if (in_array(self::LEVEL_SITE_MODULE, $level_numbers)
            && $module_id !== ''
        ) {
            $levels[] = new SiteModuleLevel($this->getTLD(), $module_id);
        }
        if (in_array(self::LEVEL_GLOBAL_MODULE, $level_numbers)
        ) {
            $levels[] = new GlobalModuleLevel($module_id);
        }
        if (in_array(self::LEVEL_SITE, $level_numbers)) {
            $levels[] = new SiteLevel($this->getTLD());
        }
        if (in_array(self::LEVEL_GLOBAL, $level_numbers)) {
            $levels[] = new GlobalLevel();
        }
        return $levels;
    }

    private function findPath(ALevel $level, $relative_id)
    {
        // make it possible to either use the id separator or a dash for
        // finding the folder of a level
        $relative_id = str_replace(Resolve::ID_SEPARATOR, "-", $relative_id);
        if (!isset($level->getFolderList()[$relative_id])) {
            return false;
        }
        $folder_name = $level->getFolderList()[$relative_id];

        return Resolve::toFilepath(
            [
                $this->root_folder,
                $level->getPrefix(),
                $folder_name
            ]
        );
    }

    public function generatePath(ALevel $level, $relative_id)
    {
        $path = $this->findPath($level, $relative_id);
        if ($path === false) {
            return false;
        }
        return realpath($path);
    }

    public function createFolder(ALevel $level, $relative_id)
    {
        $path = $this->findPath($level, $relative_id);
        if ($path === false) {
            return false;
        }
        if (@mkdir($path, 0775, true) === false) {
        };
        return realpath($path);
    }


    public function findModuleDefinition(string $module_id)
    {
        $file_path = $this->findFile(
            'modules',
            Resolve::idToFilepathRel($module_id)
                . DIRECTORY_SEPARATOR
                . (new GlobalModuleLevel(null))->getSpecificName('modulefile'),
            [
                new SiteModuleLevel($this->getTLD(), $module_id),
                new SiteLevel($this->getTLD()),
                new GlobalModuleLevel($module_id),
                new GlobalLevel()
            ]
        );
        return $file_path;
    }

    public function findPageDefinition(string $page_id)
    {
        $file_path = realpath(
            $this->generatePath(new SiteLevel($this->getTLD()), 'pages')
            . DIRECTORY_SEPARATOR
            . Resolve::idToFilepathRel($page_id) . '.php'
        );
        return $file_path;
    }

    public function findFile(string $folder_id, string $file_name, array $levels)
    {
        for ($i = 0; $i < sizeof($levels); ++$i) {
            //$base_path = FileHierarchy::generatePath($this->levels[$i], $folder_id);
            $base_path = $this->generatePath($levels[$i], $folder_id);
            if ($base_path === false) {
                continue;
            }
            if (is_file($base_path)) {
                return false;
            }
            if ($file_name === '') {
                return false;
            }
            $resolved_file_name = str_replace(Resolve::ID_SEPARATOR, "/", $file_name);
            $file_path = realpath($base_path . DIRECTORY_SEPARATOR . $resolved_file_name);
            if (($file_path !== false) && (is_file($file_path))) {
                return $file_path;
            }
        }
        return false;
    }

    /**
     * \brief Loads the content of the given file.
     *
     * \param string $file_name The absolute file name.
     *
     * \retval string The content of the file using file_get_contents.
     */
    private function load(string $file_name) : string
    {
        return file_get_contents($file_name);
    }

    public function getFile(
        string $folder_id,
        string $file_id,
        array $levels,
        bool $convert_file_id = true
    ) {
        if ($convert_file_id) {
            $file_id = Resolve::idToFilepathRel($file_id);
        }
        $file_path = $this->findFile($folder_id, $file_id, $levels);
        if ($file_path !== false) {
            return $this->load($file_path);
        }
        return false;
    }

    public function getSha1(string $folder_id, string $file_id, array $levels)
    {
        $file_name = $this->findFile($folder_id, $file_id, $levels);
        if ($file_name === false) {
            return false;
        }
        return sha1_file($file_name);
    }

    final public function findFolderPaths(string $module_id, string $folder_id): array
    {
        return [
            $this->generatePath(
                new SiteModuleLevel($this->top_level_domain, $module_id),
                $folder_id
            ),
            $this->generatePath(
                new GlobalModuleLevel($module_id),
                $folder_id
            )
        ];
    }

    final public function collectResourceFileIds(
        string $module_id,
        string $resource_id,
        string $folder_id,
        string $pattern = '*'
    ) : array {
        $folders = $this->findFolderPaths($module_id, $resource_id);
        $file_names = [];
        for ($i = 0; $i < sizeof($folders); ++$i) {
            if ($folders[$i] === false) {
                continue;
            }
            $file_names = glob(Resolve::toFilepath([
                $folders[$i],
                Resolve::idToFilepathRel($folder_id),
                $pattern
            ]));
            if (!empty($file_names)) {
                break;
            }
        }
        $filtered_ids = [];
        foreach ($file_names as &$file) {
            if (is_dir($file)) {
                continue;
            }
            $filtered_ids [] = Resolve::filepathRelToId(pathinfo($file)['basename']);
        }
        return $filtered_ids;
    }

    private function collectResource(string $module_id, string $type): array
    {
        $folders = $this->findFolderPaths($module_id, $type);
        for ($i = 0; $i < sizeof($folders); ++$i) {
            if ($folders[$i] === false) {
                continue;
            }
            $file_names = glob($folders[$i] . DIRECTORY_SEPARATOR . '*.'.$type);
            if (!empty($file_names)) {
                natcasesort($file_names);
                return $file_names;
            }
        }
        return [];
    }

    final public function collectCssFilenames(string $module_id): array
    {
        return $this->collectResource($module_id, 'css');
    }

    final public function collectJavascriptFilenames(string $module_id): array
    {
        return $this->collectResource($module_id, 'js');
    }

    final public function loadTextFile(string $module_id, string $file_id): string
    {
        if (substr($file_id, -4) !== '.txt') {
            $file_id .= '.txt';
        }
        $file_content = $this->getFile(
            'res-text',
            $file_id,
            $this->factoryLevelArray([], $module_id)
        );
        return ($file_content === false) ? '' : $file_content;
    }

    final public function loadJsonFile(string $module_id, string $file_id, bool $asArray = true)
    {
        if (substr($file_id, -5) !== '.json') {
            $file_id .= '.json';
        }
        $file = $this->getFile(
            'res-json',
            $file_id,
            $this->factoryLevelArray([], $module_id)
        );
        return ($file === false) ? false : json_decode($file, $asArray);
    }

    final public function loadResourceFile(string $module_id, string $file_id): string
    {
        $file_content = $this->getFile(
            'res',
            $file_id,
            $this->factoryLevelArray([], $module_id)
        );
        return ($file_content === false) ? '' : $file_content;
    }

    final public function loadHtmlFile(string $module_id, string $file_id)
    {
        if (substr($file_id, -5) !== '.html') {
            $file_id .= '.html';
        }
        $file_content = $this->getFile(
            'res-html',
            $file_id,
            $this->factoryLevelArray([], $module_id)
        );
        return ($file_content === false) ? '' : $file_content;
    }

    final public function loadL10nFile(string $module_id, string $file_id)
    {
        return $this->getFile(
            'res-l10n',
            $file_id,
            $this->factoryLevelArray([], $module_id)
        );
    }

    final public function getL10n(string $module_id, string $language, string $key, string $default): string
    {
        $l10n = $this->loadL10nFile($module_id, Resolve::arrayToId([$language, $key]));
        return ($l10n === false) ? $default : trim($l10n);
    }
}
