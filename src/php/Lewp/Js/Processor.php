<?php

/**
 * (c) Lewin Probst, info@emirror.de, www.emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 * It is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

declare(strict_types=1);

namespace Lewp\Js;

use Lewp\Interfaces\IFileHierarchy;
use Lewp\Resolve;
use Lewp\Utilities\FileChangeTracker;
use Lewp\VarFolder;
use \MatthiasMullie\Minify;

class Processor
{

    private $module_id;

    private $var_folder;

    private $js_file_names = [];

    private $combined_js_files = '';

    private $file_change_states = [];

    private $workspace_folder_id;

    public function __construct(IFileHierarchy $file_hierarchy, string $module_id, array $js_file_names)
    {
        $this->module_id = $module_id;
        $this->workspace_file_id = $module_id . '.js';
        $this->js_file_names = $js_file_names;

        $this->workspace_folder_id = 'tmp' . Resolve::ID_SEPARATOR . 'js';

        $this->var_folder = new VarFolder($file_hierarchy, $this->module_id);
        $this->var_folder->createFolder($this->workspace_folder_id);

        foreach ($this->js_file_names as $js_file) {
             $tmp = new FileChangeTracker(
                 $file_hierarchy,
                 $this->module_id,
                 'js',
                 basename($js_file)
             );
            $this->file_change_states[] = $tmp->hasStateChanged();
        }

        if (in_array(true, $this->file_change_states)) {
            $this->combined_js_files = $this->getCombinedJsFiles();
            $this->saveCombinedJsFiles();
        }
    }

    private function loadJsFilesIfRequired() : bool
    {
        if ($this->combined_js_files === '') {
            $this->combined_js_files = $this->getCombinedJsFiles();
            return true;
        }
        return false;
    }

    private function getCombinedJsFiles() : string
    {
        $combined_js = '';
        for ($i = 0; $i < sizeof($this->js_file_names); ++$i) {
            $combined_js .= file_get_contents($this->js_file_names[$i]);
        }
        return (new Minify\JS($combined_js))->minify();
    }

    private function saveCombinedJsFiles() : bool
    {
        $this->getJs();
        $var_file = $this->var_folder->openFile(
            Resolve::arrayToId([
                $this->workspace_folder_id,
                $this->workspace_file_id
            ]),
            'wb'
        );
        fwrite($var_file, $this->combined_js_files);
        $this->var_folder->closeFile($var_file);
        return true;
    }

    /**
     * \brief Returns the combined JavaScript of the module.
     *
     * \retval string The combined JavaScript.
     */
    public function getJs() : string
    {
        $file_id = Resolve::arrayToId(
            [$this->workspace_folder_id, $this->workspace_file_id]
        );
        if (!$this->var_folder->fileExists($file_id)) {
            return '';
        }
        return $this->var_folder->loadFile($file_id);
    }
}
