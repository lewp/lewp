

function LewpFootprintCollection() {
    this.__map = new Map();
}

LewpFootprintCollection.prototype.add = function(footprint) {
    if (this.__map.has(footprint.clientId)) {
        return false;
    }
    this.__map.set(footprint.clientId, footprint);
    return true;
}

LewpFootprintCollection.prototype.get = function(clientId) {
    if (this.__map.has(clientId)) {
        return this.__map.get(clientId);
    }
}
