

function LewpModuleCollection() {
    this.__map = new Map();
    this.__modules = {};
}

LewpModuleCollection.prototype.add = function(lewpModule) {
    // check if already in there
    if (this.__map.get(lewpModule.domNode) === lewpModule) {
        this.remove(lewpModule);
    }
    // add to array
    if (!this.__modules.hasOwnProperty(lewpModule.clientId)) {
        this.__modules[lewpModule.clientId] = [];
    }
    this.__modules[lewpModule.clientId].push(lewpModule);
    // add to map
    this.__map.set(lewpModule.domNode, lewpModule);
}

LewpModuleCollection.prototype.remove = function(lewpModule) {
    let ref = this.__map.get(lewpModule.domNode);
    this.__modules[ref.clientId] = this.__modules[ref.clientId].filter(function(value, index, arr) {
        return value !== lewpModule;
    });
    this.__map.delete(lewpModule.domNode);
}

LewpModuleCollection.prototype.get = function(clientId, index) {
    index = index || 0;
    return this.__modules[clientId][index];
}

LewpModuleCollection.prototype.getByDom = function(domReference) {
    return this.__map.get(domReference);
}

LewpModuleCollection.prototype.forEach = function(callback, thisArg) {
    this.__map.forEach(callback, thisArg);
}
