/**
 * lewp (c) by Lewin Probst, info@emirror.de, https://emirror.de
 *
 * This file is part of the lewp project originally available at
 * https://gitlab.com/lewp/lewp.
 *
 * lewp is licensed under MIT. More information on the license is provided
 * in the root folder of the repository in the LICENSE file.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see https://creativecommons.org/licenses/by-nc-sa/4.0/.
 *
 * It is explicitly prohibited to use this and any customized version of this
 * software to provide content that supports racism, violence, or any other kind
 * of content that harms human rights or animals.
 */

const HTML_ATTR_DATA_FOOTPRINT = "data-lewp-footprint";
const HTML_ATTR_DATA_DEFERRED_IMAGE_SRC = "data-lewp-deferred-image-src";
const ID_SEPARATOR = "_";
const URI_PATH_NON_RENDER_CRITICAL_CSS = "/res/css/non-render-critical";
const URI_PATH_JAVASCRIPT = "/res/js";
const URI_PATH_JAVASCRIPT_DEPENDENCY = "/res/jsdep";

/**
 * Provides interactions with the server during runtime.
 */
function Lewp() {

    // check if the current frame is the window
    this.__securityCheck();

    // Store for referencing to it later on
    let self = this;

    // Initialize properties
    this.getPageSpecificProperties();

    this.initFunctions = new Map();
    this.footprints = new LewpFootprintCollection();
    this.modules = new LewpModuleCollection();


    this.observer = new MutationObserver(this.__housekeeping);
    this.observer.observe(
        document,
        {
            attributes: false,
            childList: true,
            subtree: true
        }
    );
}

Lewp.prototype.__securityCheck = function() {
    if (window.location.hostname !== window.top.location.hostname) {
        console.error(
            "This website does not run in a separate frame for security reasons"
        );
        window.location = "/404";
    }
}

Lewp.prototype.getPageSpecificProperties = function() {
    this.language = document.querySelector("html").getAttribute("lang");

    this.pageId = window.location.pathname.slice(1).replace('/', ID_SEPARATOR);
    if (this.pageId[this.pageId.length - 1] === ID_SEPARATOR) {
        this.pageId = this.pageId.slice(0, -1);
    }

    return this;
}

Lewp.prototype.__housekeeping = function(mutationList, observer) {
    mutationList.forEach(function(item, index){
        // find all modules added to the page
        let registeredModules = lewp.getAddedModules(item.addedNodes) || [];
        registeredModules.forEach(function(module) {
            // add them to the global register
            lewp.modules.add(module);
        });
        // load deferred images
        LewpModule.prototype.loadDeferredImages.call({domNode: document});
        //let module = lewp.modules.getByDom(item.target);
        //if (module) {
        //    module.loadDeferredImages();
        //}
    });
}

Lewp.prototype.getAddedModules = function(nodeList) {
    let addedModules = [];
    for (let i = 0; i < nodeList.length; ++i) {
        let node = nodeList[i];
        if (!node.tagName) { return; }
        if (
            ["body", "html", "head"]
                .includes(node.tagName.toLowerCase())
        ) {
            return;
        }
        if (!node.dataset.lewpFootprint || false) {
            return;
        }
        let footprint = JSON.parse(
            this.htmlEntitiesDecode(node.dataset.lewpFootprint)
            );
        let module = this.modules.getByDom(node)
            || new LewpModule(
                footprint.clientId,
                footprint,
                node
                );
        addedModules.push(module);
    }
    return addedModules;
}

Lewp.prototype.htmlEntities = function(str) {
    return String(str).replace(/[\u00A0-\u9999<>\&]/gim, (i) => {
        return '&#'+i.charCodeAt(0)+';';
     });
}

Lewp.prototype.htmlEntitiesDecode = function(str) {
    return String(str).replace(/\&#([0-9]){1,4};/gim, (i) => {
        return String.fromCharCode(i.substr(2, (i.length - 3)));
    });
}

/*

Lewp.prototype.__initializeModuleInstance = function(moduleInstance) {
    let initFunction = this.initFunctions.get(moduleInstance.clientId);
    if (!initFunction) {
        return false;
    }
    Object.assign(
        moduleInstance,
        initFunction.call(moduleInstance.domNode)
    );
    return true;
}
*/

/**
 * Creates the instance that could be used during runtime to interact with the
 * server.
 */
const lewp = new Lewp();
