

function LewpModule(moduleId, footprint, domReference) {
    Object.assign(
        this,
        footprint
    );
    Object.defineProperties(
        this, {
        'domNode': {
            get() {
                return domReference;
            }
        }
    });

    if (footprint.lazyLoading) {
        let options = {
            root: null,
            rootMargin: '0px',
            threshold: 0
            };
    
        let observer = new IntersectionObserver(
            LewpModule.prototype.__onIntersection,
            options
            );
        observer.observe(this.domNode);
    } else {
        // call the setup function directly
        this.setup();
    }
}

LewpModule.prototype.__onIntersection = function(entries, observer) {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return;
        }
        lewp.modules.getByDom(entry.target).setup();
    });
}

LewpModule.prototype.isInViewport = function(domNode) {
    let bounding = (domNode)
        ? domNode.getBoundingClientRect()
        : this.domNode.getBoundingClientRect();
    let windowHeight = (
        window.innerHeight
        || document.documentElement.clientHeight
    );
    let windowWidth = (
        window.innerWidth
        || document.documentElement.clientWidth
    );
    return (
        (bounding.top <= windowHeight)
        && ((bounding.top + bounding.height) >= 0)
        && (bounding.left <= windowWidth)
        && ((bounding.left + bounding.width) >= 0)
    );
}

LewpModule.prototype.setup = async function() {
    let self = this;
    // add initialized flag
    this.isInitialized = false;
    // add non render critical css
    this.addNonRenderCriticalCss();
    // load all images
    //this.loadDeferredImages();

    let dependenciesLoaded = false;
    try {
        dependenciesLoaded = await this.addJavaScriptDependencies();
    } catch (err) {
        console.error("Loading dependencies failed: " + err);
        return err;
    }
    if (dependenciesLoaded.filter(i => i === false).length > 0) {
        console.error("Loading dependencies failed!");
    }

    let result = false;
    try {
        result = await this.addJavaScript();
    } catch (err) {
        return err;
    }
    if (result === true) {
        self.initialize();
    }/*else {
        let jsLoaded = await self.addEventListener(result);
        if (jsLoaded === true) {
            self.initialize();
        }
    }*/
    //this.addJavaScript()
        //.then((result) => {
        //    if (result === true) {
        //        self.initialize();
        //        return new Promise((resolve, reject) => {
        //            reject(false);
        //        });
        //    } else {
        //        return new Promise((resolve, reject) => {
        //            result.addEventListener("load", () => {resolve(true)});
        //            result.addEventListener("error", () => {reject(true)});
        //        });
        //    }
        //})
        //.then(() => {
        //    self.initialize();
        //})
        //.catch((e) => {
        //    if (e) {
        //        console.error("Initializing module " + self.clientId + " failed: " + e)
        //    }
        //});
}

LewpModule.prototype.initialize = function() {
    if (this.isInitialized) {
        return;
    }
    //let moduleResult = lewp.initFunctions.get(this.clientId).call(this.domNode);
    //for (var attribute in moduleResult) {
    //    if (!moduleResult.hasOwnProperty(attribute)) {
    //        continue;
    //    }
    //    this[attribute] = moduleResult[attribute];
    //}
    Object.assign(
        this,
        lewp.initFunctions.get(this.clientId).call(this.domNode)
    );
    this.__estimateSubmodules();
    this.isInitialized = true;
}

LewpModule.prototype.loadDeferredImages = function() {
    let images = this.domNode.querySelectorAll(
        "[" + HTML_ATTR_DATA_DEFERRED_IMAGE_SRC + "]"
    );
    images.forEach((item, index) => {
        if (item.getAttribute(HTML_ATTR_DATA_DEFERRED_IMAGE_SRC)) {
            item.setAttribute(
                'src',
                item.getAttribute(HTML_ATTR_DATA_DEFERRED_IMAGE_SRC)
            );
            item.removeAttribute(HTML_ATTR_DATA_DEFERRED_IMAGE_SRC);
        }
    });
}

LewpModule.prototype.__estimateSubmodules = function() {
    let childs = this.domNode.childNodes;
    this.submodules = [];
    childs.forEach(function(moduleNode) {
        if (lewp.modules.getByDom(moduleNode)) {
            this.submodules.push(
                lewp.modules.getByDom(moduleNode)
            );
        }
    }, this);
}

LewpModule.prototype.addNonRenderCriticalCss = function() {
    if (!this.hasCss) {
        return false;
    }
    let cssPath = URI_PATH_NON_RENDER_CRITICAL_CSS
            + "/"
            + this.serverId;

    let existentNodes = document
        .head
        .querySelectorAll('link[href="' + cssPath + '"]');
    if (existentNodes.length > 0) {
        return false;
    }

    let cssElement = document.createElement("link");
    //cssElement.onload = function() {
    //    console.log(this);
    //    this.rel = "stylesheet";
    //    this.type = "text/css";
    //    this.removeAttribute("as");
    //};
    cssElement.setAttribute("rel", "stylesheet");
    cssElement.setAttribute("type", "text/css");
    //cssElement.setAttribute("rel", "preload");
    //cssElement.setAttribute("as", "style");
    cssElement.setAttribute(
        "href",
        URI_PATH_NON_RENDER_CRITICAL_CSS
        + "/"
        + this.serverId
    );
    document.head.appendChild(cssElement);
    return true;
}

LewpModule.prototype.addJavaScript = function() {
    if (!this.hasJavascript) {
        return new Promise((resolve, reject) => {
            reject("No JavaScript to load!")
        });
    }
    let self = this;
    let path = this.getScriptPath();
    return this.loadJavaScriptIfNotExistent(path);
    /*
    let existentNode = document
        .head
        .querySelector(
            'script[src="'
            + path
            + '"]'
        );
    if (existentNode) {
        return new Promise((resolve, reject) => {resolve(existentNode)});
    }

    let jsElement = document.createElement("script");
    jsElement.setAttribute("src", path);
    jsElement.setAttribute("async", "async");
    document.head.appendChild(jsElement);

    return new Promise(function(resolve, reject) {
        jsElement.addEventListener("load", () => {resolve(true)});
        jsElement.addEventListener("error", () => {reject(false)});
        //jsElement.addEventListener("load", () => {
        //    setTimeout(() => {resolve(true)}, 200);
        //});
    });
    */
}

LewpModule.prototype.getScriptPath = function() {
    return URI_PATH_JAVASCRIPT
        + "/"
        + this.serverId;
}

LewpModule.prototype.getDependencyScriptPath = function(serverId) {
    return URI_PATH_JAVASCRIPT_DEPENDENCY
        + "/"
        + this.serverId
        + "/"
        + serverId;
}

LewpModule.prototype.getScript = function(path) {
    let existentNode = document
        .head
        .querySelector(
            'script[src="'
            + path
            + '"]'
        );
    return (existentNode) ? existentNode : false;
}

LewpModule.prototype.loadJavaScriptIfNotExistent = function(path) {
    let self = this;
    let script = this.getScript(path);
    if (!script) {
        script = document.createElement("script");
        script.setAttribute("src", path);
        script.setAttribute("async", "async");
        document.head.appendChild(script);
    }

    return new Promise(function(resolve, reject) {
        script.addEventListener("load", () => {resolve(true)});
        script.addEventListener("error", () => {
            reject("Error on load in script " + self.serverId + "!")
        });
    });
}

LewpModule.prototype.addJavaScriptDependencies = function() {
    let self = this;
    let results = this.jsdep.map((item) => {
        return self.loadJavaScriptIfNotExistent(
            self.getDependencyScriptPath(item)
        );
    });
    return Promise.all(results);
}
