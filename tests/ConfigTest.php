<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\Config;
use Lewp\FileHierarchy;
use Lewp\FileHierarchy\GlobalModuleLevel;

class ConfigDerivative extends Config {
    public function getDefaultConfiguration() : array {
        return $this->defaultConfiguration();
    }

    public function getLoadedConfiguration() : array {
        return $this->load();
    }
}

class ConfigTest extends TestCase
{

    private $tld = "lewp.org";

    private $file_hierarchy;
    private $instance;

    private function initialize($page_id, $module_id)
    {
        //FileHierarchy::initialize(
        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            $this->tld
        );
        $this->instance = new ConfigDerivative($this->file_hierarchy, $page_id, $module_id);
    }

    public function testCanBeInstantiatedSiteLevel()
    {
        $this->initialize("", "");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedPageLevel()
    {
        $this->initialize("home", "");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedGlobalModuleLevel()
    {
        $this->initialize("", "hello-world");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedSiteModuleLevel()
    {
        $this->initialize("", "hello-site-world");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedGlobalModuleLevelIncludingPage()
    {
        $this->initialize("home", "hello-site-world");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedWithPageAndModuleNotExisting()
    {
        $this->initialize("my-page-id-does-not-exist", "the-module-that-does-not-exist");

        $this->assertInstanceOf(
            Config::class,
            $this->instance
        );
    }

    public function testLoadsDefaultConfigurationWhenNoConfigSpecifiedOrDoesNotExist()
    {
        $this->initialize("my-page-id-does-not-exist", "the-module-that-does-not-exist");

        $this->assertSame(
            $this->instance->getDefaultConfiguration(),
            $this->instance->getLoadedConfiguration()
        );
    }

    public function testOverridesDefaultConfigurationWhenConfigSpecified()
    {
        $this->initialize("", "hello-world");
        $moduleconfig = include $this->file_hierarchy->generatePath(new GlobalModuleLevel("hello-world"), "configfile");

        $this->assertSame(
            $moduleconfig['title'],
            $this->instance->getValue("title")
        );
    }

    public function testSetsValueSuccessfully()
    {
        $this->initialize("my-page-id-does-not-exist", "the-module-that-does-not-exist");
        $expectation = "I have been set!";

        $this->assertSame(
            $expectation,
            $this->instance->setValue("title", $expectation)->getValue("title")
        );
    }

    public function testSearchConfigurationKeyArrayForValue()
    {
        $this->initialize("my-page-id-does-not-exist", "the-module-that-does-not-exist");
        $input = [
            "first",
            "second",
            "third"
        ];
        $expectation = 1;
        $search_value = $input[$expectation];
        $key = "somekey";

        $this->instance->setValue($key, $input);

        $this->assertSame(
            $expectation,
            $this->instance->arraySearch($search_value, $key)
        );
    }

}
