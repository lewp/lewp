<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\UrlPaths;

class UrlPathsTest extends TestCase
{

    public function testImplodesArrayToAbsoluteUri()
    {
        $this->assertSame(
            "/res/images/something.png",
            UrlPaths::implodeToAbsolute(["res", "images", "something.png"])
        );
    }
    
}
