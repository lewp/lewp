<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ALevelTest extends TestCase
{

    private $instance;

    private function initialize()
    {
        $this->instance = new class extends \Lewp\FileHierarchy\ALevel {
            protected function specific() : array
            {
                return [
                    'specific-folder' => 'specific'.DIRECTORY_SEPARATOR.'folder'
                ];
            }

            public function getPrefix() : string
            {
                return 'prefix';
            }
        };
    }

    public function testCanBeDerivedAndInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            \Lewp\FileHierarchy\ALevel::class,
            $this->instance
        );
    }

    public function testReturnsThePrefixProperly()
    {
        $this->initialize();

        $this->assertSame(
            "prefix",
            $this->instance->getPrefix()
        );
    }

    public function testReturnsFolderListProperly()
    {
        $this->initialize();

        $folders = [
           'js' => 'js',
           'phpdep' => 'lib',
           'jsdep' => 'lib'.DIRECTORY_SEPARATOR.'js',
           'res' => 'resources',
           'res-images' => 'resources'.DIRECTORY_SEPARATOR.'images',
           'res-fonts' => 'resources'.DIRECTORY_SEPARATOR.'fonts',
           'res-icons' => 'resources'.DIRECTORY_SEPARATOR.'icons',
           'res-movies' => 'resources'.DIRECTORY_SEPARATOR.'movies',
           'res-text' => 'resources'.DIRECTORY_SEPARATOR.'text',
           'res-json' => 'resources'.DIRECTORY_SEPARATOR.'json',
           'res-html' => 'resources'.DIRECTORY_SEPARATOR.'html',
           'res-l10n' => 'resources'.DIRECTORY_SEPARATOR.'l10n',
           'var' => 'var',
           'specific-folder' => 'specific'.DIRECTORY_SEPARATOR.'folder'
        ];

        $this->assertSame(
            $folders,
            $this->instance->getFolderList()
        );
    }

}
