<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy\GlobalLevel;
use Lewp\FileHierarchy\SiteLevel;

class SiteLevelTest extends TestCase
{

    private $top_level_domain = "lewp.org";
    private $instance;

    private function initialize()
    {
        $this->instance = new SiteLevel($this->top_level_domain);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            SiteLevel::class,
            $this->instance
        );
    }

    public function testReturnsThePrefixProperly()
    {
        $this->initialize();

        $global = new GlobalLevel();

        $this->assertSame(
            $global->getFolderList()['websites'].DIRECTORY_SEPARATOR.$this->top_level_domain,
            $this->instance->getPrefix()
        );
    }
}
