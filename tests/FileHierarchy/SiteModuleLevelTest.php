<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy\SiteModuleLevel;

class SiteModuleLevelTest extends TestCase
{

    private $top_level_domain = "lewp.org";
    private $instance;

    private function initialize()
    {
        $this->instance = new SiteModuleLevel($this->top_level_domain, "hello-world");
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            \Lewp\FileHierarchy\SiteModuleLevel::class,
            $this->instance
        );
    }

    public function testGetsSpecificNameProperly()
    {
        $this->initialize();

        $this->assertSame(
            "module.php",
            $this->instance->getSpecificName("modulefile")
        );

        $this->assertSame(
            false,
            $this->instance->getSpecificName("somethingthatdoesnotexist")
        );
    }
}
