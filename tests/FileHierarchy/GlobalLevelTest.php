<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class GlobalLevelTest extends TestCase
{

    private $instance;

    private function initialize()
    {
        $this->instance = new \Lewp\FileHierarchy\GlobalLevel();
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            \Lewp\FileHierarchy\GlobalLevel::class,
            $this->instance
        );
    }

    public function testReturnsThePrefixProperly()
    {
        $this->initialize();

        $this->assertSame(
            "",
            $this->instance->getPrefix()
        );
    }
}
