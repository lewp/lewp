<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy\GlobalModuleLevel;

class GlobalModuleLevelTest extends TestCase
{

    private $instance;

    private function initialize()
    {
        $this->instance = new GlobalModuleLevel("hello-world");
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            \Lewp\FileHierarchy\GlobalModuleLevel::class,
            $this->instance
        );
    }

    public function testGetsSpecificNameProperly()
    {
        $this->initialize();

        $this->assertSame(
            "module.php",
            $this->instance->getSpecificName("modulefile")
        );

        $this->assertSame(
            false,
            $this->instance->getSpecificName("somethingthatdoesnotexist")
        );
    }
}
