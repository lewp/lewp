<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\Module;
use Lewp\Resolve;

class ModuleTest extends TestCase
{

    private $instance;

    private $file_hierarchy;

    private function initialize($module_id)
    {
        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );

        $this->instance = include $this->file_hierarchy->findModuleDefinition($module_id);
        $this->instance->initialize($this->file_hierarchy);
    }

    public function testCanBeInitialized()
    {
        $this->initialize("hello-site-world");

        $this->assertInstanceOf(
            Module::class,
            $this->instance
        );
    }

    public function testDeterminesCorrectModuleId()
    {
        $module_id = "hello-site-world";
        $this->initialize($module_id);

        $this->assertSame(
            $module_id,
            $this->instance->getModuleId()
        );
    }

    public function testReturnsCorrectTopLevelDomain()
    {
        $module_id = "hello-site-world";
        $this->initialize($module_id);
        $tld = $this->file_hierarchy->getTLD();

        $this->assertSame(
            $tld,
            $this->instance->getTLD()
        );
    }

    public function testGetsAndSetsPageId()
    {
        $module_id = "hello-site-world";
        $this->initialize($module_id);
        $expectation = "home";
        $this->instance->setPageId($expectation);

        $this->assertSame(
            $expectation,
            $this->instance->getPageId()
        );
    }

    public function testLoadsTextFile()
    {
        $module_id = "hello-site-world";
        $textfile_id = "en".Resolve::ID_SEPARATOR."sample-text-file";
        $this->initialize($module_id);
        $expectation = file_get_contents(
            Resolve::toFilepath([
                __DIR__,
                "dummy-file-hierarchy",
                "www",
                "lewp.org",
                "modules",
                $module_id,
                "resources",
                "text"
            ]).DIRECTORY_SEPARATOR
            .Resolve::idToFilepathRel($textfile_id).".txt"
        );

        $this->assertSame(
            $expectation,
            $this->instance->loadTextFile($textfile_id)
        );

        $expectation = '';
        $this->assertSame(
            $expectation,
            $this->instance->loadTextFile($textfile_id."i-am-not-existent")
        );
    }

    public function testLoadsJsonFile()
    {
        $module_id = "hello-site-world";
        $file_id = "sample.json";
        $this->initialize($module_id);
        $expectation = json_decode(file_get_contents(
            Resolve::toFilepath([
                __DIR__,
                "dummy-file-hierarchy",
                "www",
                "lewp.org",
                "modules",
                $module_id,
                "resources",
                "json"
            ]).DIRECTORY_SEPARATOR
            .Resolve::idToFilepathRel($file_id)
        ), true);

        $this->assertSame(
            $expectation,
            $this->instance->loadJsonFile($file_id)
        );

        $expectation = false;
        $this->assertSame(
            $expectation,
            $this->instance->loadJsonFile($file_id."i-am-not-existent")
        );
    }

    public function testLoadsAFileFromTheResourceDirectory()
    {
        $module_id = "hello-site-world";
        $file_id = "json".Resolve::ID_SEPARATOR."sample.json";
        $this->initialize($module_id);
        $expectation = file_get_contents(
            Resolve::toFilepath([
                __DIR__,
                "dummy-file-hierarchy",
                "www",
                "lewp.org",
                "modules",
                $module_id,
                "resources",
            ]).DIRECTORY_SEPARATOR
            .Resolve::idToFilepathRel($file_id)
        );

        $this->assertSame(
            $expectation,
            $this->instance->loadResourceFile($file_id)
        );

        $expectation = '';
        $this->assertSame(
            $expectation,
            $this->instance->loadResourceFile($file_id."i-am-not-existent")
        );
    }

}
