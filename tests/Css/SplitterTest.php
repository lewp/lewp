<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\Css\Splitter;
use \Sabberworm\CSS\Parser;

class SplitterTest extends TestCase
{

    private $instance;

    private function initialize()
    {

        $this->instance = new Splitter();
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            Splitter::class,
            $this->instance
        );
    }

    public function testReturnsRenderCriticalCss()
    {
        $this->initialize();

        $css_file = file_get_contents(
            __DIR__.DIRECTORY_SEPARATOR."css-testfile.css"
        );

        $expectation = new Parser(file_get_contents(
            __DIR__.DIRECTORY_SEPARATOR."css-render-critical-output.css"
        ));
        $this->assertSame(
            $expectation->parse()->render(),
            $this->instance->getRenderCriticalCss($css_file)
        );
    }

    public function testReturnsNonRenderCriticalCss()
    {
        $this->initialize();

        $css_file = file_get_contents(
            __DIR__.DIRECTORY_SEPARATOR."css-testfile.css"
        );

        $expectation = new Parser(file_get_contents(
            __DIR__.DIRECTORY_SEPARATOR."css-non-render-critical-output.css"
        ));
        $this->assertSame(
            $expectation->parse()->render(),
            $this->instance->getNonRenderCriticalCss($css_file)
        );
    }

}
