<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\Page;
use Lewp\Resolve;

class PageTest extends TestCase
{

    private $instance;

    private $file_hierarchy;

    private function initialize($page_id)
    {
        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );

        $this->instance = include $this->file_hierarchy->findPageDefinition($page_id);
        $this->instance->initialize($this->file_hierarchy);
    }

    public function testCanBeInitialized()
    {
        $this->initialize("home");

        $this->assertInstanceOf(
            Page::class,
            $this->instance
        );
    }

    public function testReturnsCorrectCssIfItExists()
    {
        $page_id = "home";
        $this->initialize($page_id);
        $expectation = $this->file_hierarchy->getFile(
            "css",
            "home.css",
            $this->file_hierarchy->factoryLevelArray([], '')
        );

        $this->assertSame(
            $expectation,
            $this->instance->getCss()
        );
    }

    public function testReturnsEmptyStringIfCssDoesNotExist()
    {
        $page_id = "home";
        $this->initialize($page_id);
        $filename = $this->file_hierarchy->findFile(
            "css",
            "home.css",
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        // rename the css testfile
        rename($filename, $filename.".moved");

        $expectation = '';

        $this->assertSame(
            $expectation,
            $this->instance->getCss()
        );
        rename($filename.".moved", $filename);
    }

    public function testKnowsThatCssIsExisting()
    {
        $page_id = "home";
        $this->initialize($page_id);

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->hasCss()
        );
    }

    public function testKnowsThatCssIsNotExisting()
    {
        // rename the css testfile
        $page_id = "home";
        $this->initialize($page_id);
        $filename = $this->file_hierarchy->findFile(
            "css",
            "home.css",
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        rename($filename, $filename.".moved");

        $expectation = false;
        $this->assertSame(
            $expectation,
            $this->instance->hasCss()
        );
        rename($filename.".moved", $filename);
    }

    public function testReturnsCorrectJavascriptIfItExists()
    {
        $page_id = "home";
        $this->initialize($page_id);
        $expectation = $this->file_hierarchy->getFile(
            "js",
            "home.js",
            $this->file_hierarchy->factoryLevelArray([], '')
        );

        $this->assertSame(
            $expectation,
            $this->instance->getJavascript()
        );
    }

    public function testReturnsEmptyStringIfJavascriptDoesNotExist()
    {
        // rename the js testfile
        $page_id = "home";
        $this->initialize($page_id);
        $filename = $this->file_hierarchy->findFile(
            "js",
            "home.js",
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        rename($filename, $filename.".moved");

        $expectation = '';

        $this->assertSame(
            $expectation,
            $this->instance->getJavascript()
        );
        rename($filename.".moved", $filename);
    }

    public function testKnowsThatJavascriptIsExisting()
    {
        $page_id = "home";
        $this->initialize($page_id);

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->hasJavascript()
        );
    }

    public function testKnowsThatJavascriptIsNotExisting()
    {
        // rename the css testfile
        $page_id = "home";
        $this->initialize($page_id);
        $filename = $this->file_hierarchy->findFile(
            "js",
            "home.js",
            $this->file_hierarchy->factoryLevelArray([], '')
        );
        rename($filename, $filename.".moved");

        $expectation = false;
        $this->assertSame(
            $expectation,
            $this->instance->hasJavascript()
        );
        rename($filename.".moved", $filename);
    }

    public function testSetsLanguageIfCorrect()
    {
        $page_id = "home";
        $this->initialize($page_id);

        $expectation = "en";
        $this->assertSame(
            $expectation,
            $this->instance->setLanguage($expectation)
        );

    }

    public function testRejectsLanguageIfIncorrect()
    {
        $page_id = "home";
        $this->initialize($page_id);

        $language = "en_US";
        $expectation = "";
        $this->assertSame(
            $expectation,
            $this->instance->setLanguage($expectation)
        );
    }

    public function testReturnsLanguage()
    {
        $page_id = "home";
        $this->initialize($page_id);

        $expectation = "en";
        $this->instance->setLanguage($expectation);
        $this->assertSame(
            $expectation,
            $this->instance->getLanguage()
        );

    }

}
