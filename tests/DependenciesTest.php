<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\Dependencies;
use Lewp\FileHierarchy;

class DependenciesTest extends TestCase
{

    private $tld = "lewp.org";

    private $instance;

    private function initialize($module_id)
    {
        $fh = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            $this->tld
        );
        $this->instance = new Dependencies($fh, $module_id);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize("hello-site-world");

        $this->assertInstanceOf(
            Dependencies::class,
            $this->instance
        );
    }

    public function testReturnsEmptyArrayIfNoDependenciesAreStated()
    {
        $this->initialize("hello-world");

        $expectation = [];
        $this->assertSame(
            $expectation,
            $this->instance->getModuleDependencies()
        );
        $this->assertSame(
            $expectation,
            $this->instance->getJavascriptDependencies()
        );
    }

    public function testDeterminesCorrectModuleDependencies()
    {
        $this->initialize("hello-site-world");

        $expectation = [
            "dependency-modules_dep-of-hello-site-world"
        ];
        $this->assertSame(
            $expectation,
            $this->instance->getModuleDependencies()
        );
    }

    public function testDeterminesCorrectJavascriptDependencies()
    {
        $this->initialize("hello-site-world");

        $expectation = [
        ];
        $this->assertSame(
            $expectation,
            $this->instance->getJavascriptDependencies()
        );
    }

}
