<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\VarFolder;

class VarFolderTest extends TestCase
{

    private $instance;

    private function initialize($module_id)
    {
        $fh = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );
        $this->instance = new VarFolder($fh, $module_id);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize("hello-site-world");

        $this->assertInstanceOf(
            VarFolder::class,
            $this->instance
        );
    }

    public function testReturnsTrueIfFileExistsAndFalseIfNot()
    {
        $this->initialize("hello-site-world");

        $expectation = false;
        $this->assertSame(
            $expectation,
            $this->instance->fileExists('file-does-not-exist')
        );

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->fileExists('existing-folder_file-exists.test')
        );
    }

    public function testGrantsPublicAccessToAFolder()
    {
        $this->initialize("hello-site-world");

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->grantPublicAccess('existing-folder')
        );

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->fileExists('existing-folder_.public')
        );
    }

    public function testCanDetectIfFilesOrFoldersArePubliclyAvailable()
    {
        $this->initialize("hello-site-world");

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->isPublic('existing-folder_file-exists.test')
        );
        $this->assertSame(
            $expectation,
            $this->instance->isPublic('existing-folder')
        );
    }

    public function testRevokePublicAccessToAFolder()
    {
        $this->initialize("hello-site-world");

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->revokePublicAccess('existing-folder')
        );
    }

    public function testCreatesASubfolder()
    {
        $this->initialize("hello-site-world");

        $subfolder_path = $this->instance->createFolder('a-new-subfolder');

        $expectation = true;
        $this->assertSame(
            $expectation,
            is_dir($subfolder_path)
        );
    }

    public function testCreatesFilesInASubfolder()
    {
        $this->initialize("hello-site-world");
        $file_id = 'a-new-subfolder_i-have-been-created.file';

        for ($i = 0; $i < 5; ++$i) {
            $file_handle = $this->instance->openFile($file_id.$i, 'wb');
            $this->instance->closeFile($file_handle);
        }

        $expectation = true;
        for ($i = 0; $i < 5; ++$i) {
            $this->assertSame(
                $expectation,
                $this->instance->fileExists($file_id.$i)
            );
        }
    }

    public function testDeletesAFile()
    {
        $this->initialize("hello-site-world");
        $file_id = 'a-new-subfolder_i-have-been-created.file0';

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->deleteFile($file_id)
        );
    }

    public function testDeletesASubfolderWithFilesInIt()
    {
        $this->initialize("hello-site-world");

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->removeFolder('a-new-subfolder')
        );
    }

    public function testLoadsFileContent()
    {
        $this->initialize("hello-site-world");
        $file_id = 'existing-folder_file-exists.test';

        $expectation = "This testfile contains 33 bytes.\n";
        $this->assertSame(
            $expectation,
            $this->instance->loadFile($file_id)
        );
    }

    public function testLoadsFileSize()
    {
        $this->initialize("hello-site-world");
        $file_id = 'existing-folder_file-exists.test';

        $expectation = 33;
        $this->assertSame(
            $expectation,
            $this->instance->getFileSize($file_id)
        );
    }
}
