<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\Resolve;

class ResolveTest extends TestCase
{

    public function testConvertIdToRelativeFilepath()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testid"]);
        $this->assertSame(
            "my".DIRECTORY_SEPARATOR."nice".DIRECTORY_SEPARATOR."testid",
            Resolve::idToFilepathRel($testid)
        );
    }

    public function testConvertRelativeFilepathToId()
    {
        $testpath = implode(DIRECTORY_SEPARATOR, ["reverse", "testpath"]);
        $this->assertSame(
            "reverse".Resolve::ID_SEPARATOR."testpath",
            Resolve::filepathRelToId($testpath)
        );
    }

    public function testConvertIdToUri()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testid"]);
        $expectation = "my/nice/testid";
        $this->assertSame(
            $expectation,
            Resolve::idToUri($testid)
        );
    }

    public function testConvertUriToId()
    {
        $testuri = implode("/", ["my", "nice", "testuri"]);
        $expectation = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testuri"]);
        $this->assertSame(
            $expectation,
            Resolve::uriToId($testuri)
        );
    }

    public function testFindsCorrectFolderIdOfFileId()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testid"]);
        $expectation = implode(Resolve::ID_SEPARATOR, ["my", "nice"]);
        $this->assertSame(
            $expectation,
            Resolve::findFolderId($testid)
        );
    }

    public function testImplodesArrayToFilepath()
    {
        $testarray = ["my", "nice", "testarray"];
        $expectation = implode(DIRECTORY_SEPARATOR, $testarray);
        $this->assertSame(
            $expectation,
            Resolve::toFilepath($testarray)
        );
    }

    public function testExplodesIdCorrect()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testid"]);
        $expectation = explode(Resolve::ID_SEPARATOR, $testid);
        $this->assertSame(
            $expectation,
            Resolve::idExplode($testid)
        );
    }

    public function testConvertsArrayToId()
    {
        $testarray = ["my", "nice", "testarray"];
        $expectation = implode(Resolve::ID_SEPARATOR, $testarray);
        $this->assertSame(
            $expectation,
            Resolve::arrayToId($testarray)
        );
    }

    public function testConvertsIdToJavascriptConvention()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "test-id"]);
        $expectation = "my_nice_testId";
        $this->assertSame(
            $expectation,
            Resolve::idToJavascript($testid)
        );
    }

    public function testFindsParentId()
    {
        $testid = implode(Resolve::ID_SEPARATOR, ["my", "nice", "testid"]);
        $expectation = implode(Resolve::ID_SEPARATOR, ["my", "nice"]);
        $this->assertSame(
            $expectation,
            Resolve::toParentId($testid)
        );
    }

    public function testConvertsStringToCssClassNameConvention()
    {
        $teststring = ".a/nice.string/";
        $expectation = "a-nice-string";
        $this->assertSame(
            $expectation,
            Resolve::stringToCssClassName($teststring)
        );
    }
}
