<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\Utilities\FileChangeTracker;
use Lewp\VarFolder;

class FileChangeTrackerTest extends TestCase
{

    private $instance;
    private $file_hierarchy;

    private function initialize($module_id, $folder_id, $file_id)
    {
        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );

        $this->instance = new FileChangeTracker(
            $this->file_hierarchy,
            $module_id,
            $folder_id,
            $file_id
        );
    }

    public function testCanBeInstantiated()
    {
        $this->initialize(
            "hello-site-world",
            "var",
            "existing-folder_file-exists.test"
        );

        $this->assertInstanceOf(
            FileChangeTracker::class,
            $this->instance
        );
    }

    public function testDetectsChangesInAFile()
    {

        $file_id = "existing-folder_file-change.test";
        $this->initialize(
            "hello-site-world",
            "var",
            $file_id
        );

        $var = new VarFolder($this->file_hierarchy, "hello-site-world");
        $file_handle = $var->openFile($file_id, 'wb');
        $var->closeFile($file_handle);

        $file_handle = $var->openFile($file_id, 'wb');
        fwrite($file_handle, "this file changed now");
        $var->closeFile($file_handle);

        $this->assertSame(
            true,
            $this->instance->hasStateChanged()
        );
        $var->deleteFile($file_id);
        $var->removeFolder("tmp");
    }
}
