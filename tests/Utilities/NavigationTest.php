<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\Utilities\Navigation;

class NavigationTest extends TestCase
{

    private $instance;
    private $file_hierarchy;

    private function initialize()
    {

        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );
        $this->instance = new Navigation($this->file_hierarchy);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            Navigation::class,
            $this->instance
        );
    }

    public function testFindsTheArrayThatHoldsPagesAndSubpagesFromTheGivenLevelId()
    {
        $this->initialize();

        $this->assertSame(
            json_encode($this->instance->findLevel())."\n",
            file_get_contents(__DIR__.DIRECTORY_SEPARATOR."navigation-find-level.json")
        );

        $this->assertSame(
            json_encode($this->instance->findLevel("about-us"))."\n",
            file_get_contents(__DIR__.DIRECTORY_SEPARATOR."navigation-find-level-about-us.json")
        );
    }

    public function testReturnsPages()
    {
        $this->initialize();

        $this->assertSame(
            $this->instance->findLevel(),
            $this->instance->getPages()
        );
    }

    public function testCollectsCorrectPageIdList()
    {
        $this->initialize();

        $expectation = [
            "home",
            "about-us",
            "products",
            "about-us_max",
            "about-us_moritz",
            "products_hardware",
            "products_software",
            "products_hardware_computer",
            "products_software_a-good-program"
        ];

        $this->assertSame(
            $expectation,
            $this->instance->getCompletePageIdList()
        );
    }

}
