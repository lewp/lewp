<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\Utilities\Sitemap;

class SitemapTest extends TestCase
{

    private $instance;
    private $file_hierarchy;

    private function initialize()
    {
        $top_level_domain = "lewp.org";
        $this->file_hierarchy = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            $top_level_domain
        );
        $startpage_id = "home";
        $this->instance = new Sitemap($this->file_hierarchy, $startpage_id);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize();

        $this->assertInstanceOf(
            Sitemap::class,
            $this->instance
        );
    }

    public function testReturnsCorrectSitemapAsText()
    {
        $this->initialize();

        $this->assertSame(
            file_get_contents(__DIR__.DIRECTORY_SEPARATOR."sitemap-txt.txt"),
            $this->instance->getSitemapTxt()."\n"
        );
    }

    public function testReturnsCorrectSitemapAsXml()
    {
        $this->initialize();

        $this->assertSame(
            file_get_contents(__DIR__.DIRECTORY_SEPARATOR."sitemap-xml.txt"),
            $this->instance->getSitemapXml()
        );
    }

}
