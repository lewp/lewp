<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\FileHierarchy;
use Lewp\FileHierarchy\GlobalLevel;
use Lewp\FileHierarchy\SiteLevel;
use Lewp\FileHierarchy\GlobalModuleLevel;
use Lewp\Resolve;

class FileHierarchyTest extends TestCase
{

    protected $instance;

    protected function initialize()
    {
        $this->instance = new FileHierarchy(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            "lewp.org"
        );
    }

    public function testCanBeInitialized()
    {
        $this->initialize();

        $this->assertSame(
            true,
            $this->instance->isInitialized()
        );
    }

    public function testReturnsCorrectRootFolder()
    {
        $this->initialize();

        $this->assertSame(
            __DIR__.DIRECTORY_SEPARATOR."dummy-file-hierarchy",
            $this->instance->getRootFolder()
        );
    }

    public function testReturnsCorrectTopLevelDomain()
    {
        $this->initialize();

        $this->assertSame(
            "lewp.org",
            $this->instance->getTLD()
        );
    }

    public function testGeneratesGlobalPathSuccessfully()
    {
        $this->initialize();

        $input_level = new GlobalLevel();
        $expectation = $this->instance->getRootFolder()
            .DIRECTORY_SEPARATOR
            .$input_level->getFolderList()['websites'];

        $this->assertSame(
            $expectation,
            $this->instance->generatePath(
                $input_level,
                "websites"
            )
        );
    }

    public function testGeneratesGlobalModulePathSuccessfullyWithIdSeparatorOrWithDash()
    {
        $this->initialize();

        $input_level = new GlobalModuleLevel("hello-world");
        $folder_id = 'res'.Resolve::ID_SEPARATOR."images";
        $expectation = $this->instance->getRootFolder()
            .DIRECTORY_SEPARATOR
            ."modules"
            .DIRECTORY_SEPARATOR
            ."hello-world"
            .DIRECTORY_SEPARATOR
            .$input_level->getFolderList()["res-images"];

        $this->assertSame(
            $expectation,
            $this->instance->generatePath(
                $input_level,
                $folder_id
            )
        );

        $folder_id = 'res-images';
        $this->assertSame(
            $expectation,
            $this->instance->generatePath(
                $input_level,
                $folder_id
            )
        );
    }

    public function testFindsAFileOnTheGlobalModuleLevel()
    {
        $module_id = "hello-world";
        $file_name = "myimage_something.png";
        $this->initialize();
        $expectation = $this->instance->generatePath(
            new GlobalModuleLevel($module_id),
            "res-images"
        )
        .DIRECTORY_SEPARATOR
        .str_replace(Resolve::ID_SEPARATOR, "/", $file_name);

        $folder_id = "res".Resolve::ID_SEPARATOR."images";
        // actual test function
        $result = $this->instance->findFile(
            $folder_id,
            $file_name,
            $this->instance->factoryLevelArray([FileHierarchy::LEVEL_GLOBAL_MODULE], $module_id)
        );
        $this->assertSame(
            $expectation,
            $result
        );
    }

    public function testReturnsFalseIfAFileDoesNotExistOnTheGlobalModuleLevel()
    {
        $module_id = "hello-world";
        $file_name = "myimage_something.png";
        $this->initialize();

        $folder_id = "res".Resolve::ID_SEPARATOR."images";
        $file_name = "an-image-that-does-not-exist-in-any-way.png";
        $expectation = false;
        // actual test function
        $result = $this->instance->findFile(
            $folder_id,
            $file_name,
            $this->instance->factoryLevelArray([FileHierarchy::LEVEL_GLOBAL_MODULE], $module_id)
        );
        $this->assertSame(
            $expectation,
            $result
        );
    }

    public function testFindsTheModuleDefinition()
    {
        $module_id = "hello-world";
        $this->initialize("", $module_id);
        $expectation = $this->instance->generatePath(
            new GlobalModuleLevel($module_id),
            "modulefile"
        );

        $this->assertSame(
            $expectation,
            $this->instance->findModuleDefinition($module_id)
        );
    }

    public function testFindsThePageDefinition()
    {
        $page_id = "home";
        $this->initialize($page_id, "");
        $expectation = $this->instance->generatePath(
            new SiteLevel($this->instance->getTLD()),
            "pages"
        )
        .DIRECTORY_SEPARATOR
        .$page_id.".php";

        $this->assertSame(
            $expectation,
            $this->instance->findPageDefinition($page_id)
        );
    }

    public function testReturnsContentOfExistingFile()
    {
        $module_id = "hello-world";
        $file_id = "myimage".Resolve::ID_SEPARATOR."something.png";
        $this->initialize("", $module_id);
        $expectation = file_get_contents($this->instance->generatePath(
            new GlobalModuleLevel($module_id),
            "res-images"
        )
        .DIRECTORY_SEPARATOR
        .Resolve::idToFilepathRel($file_id));

        $result = $this->instance->getFile(
            "res-images",
            $file_id,
            $this->instance->factoryLevelArray([], $module_id)
        );
        $this->assertSame(
            $expectation,
            $result
        );
    }

}
