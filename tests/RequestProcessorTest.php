<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use Lewp\RequestProcessor;

class RequestProcessorTest extends TestCase
{

    private $instance;

    private $request_basic = "/de/my/basic/page";
    private $request_get = "?q=someQuery";
    private $request_anchor = "#helloAnchor";

    private function initialize(string $request, bool $enable_language_uri)
    {
        $this->instance = new RequestProcessor($request, $enable_language_uri);
    }

    public function testCanBeInstantiated()
    {
        $this->initialize("", false);

        $this->assertInstanceOf(
            RequestProcessor::class,
            $this->instance
        );
    }

    public function testCanBeInstantiatedWithLanguageUriSupport()
    {
        $this->initialize("", true);

        $this->assertInstanceOf(
            RequestProcessor::class,
            $this->instance
        );
    }

    public function testReturnsNextRequestPartUntilNothingLeft()
    {
        $language_support = false;
        $expectation = ["de", "my", "basic", "page"];

        $this->initialize(
            $this->request_basic,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS GET
        $this->initialize(
            $this->request_basic.$this->request_get,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS GET PLUS ANCHOR
        $this->initialize(
            $this->request_basic.$this->request_get.$this->request_anchor,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS ANCHOR
        $this->initialize(
            $this->request_basic.$this->request_anchor,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS ANCHOR PLUS GET
        $this->initialize(
            $this->request_basic.$this->request_anchor.$this->request_get,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }
    }

    public function testReturnsNextRequestPartUntilNothingLeftWithLanguageSupport()
    {
        $language_support = true;
        $expectation = ["my", "basic", "page"];

        $this->initialize(
            $this->request_basic,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS GET
        $this->initialize(
            $this->request_basic.$this->request_get,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS GET PLUS ANCHOR
        $this->initialize(
            $this->request_basic.$this->request_get.$this->request_anchor,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS ANCHOR
        $this->initialize(
            $this->request_basic.$this->request_anchor,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }

        // BASIC REQUEST PLUS ANCHOR PLUS GET
        $this->initialize(
            $this->request_basic.$this->request_anchor.$this->request_get,
            $language_support
        );
        for ($i = 0; $i < sizeof($expectation); ++$i) {
            $this->assertSame(
                $expectation[$i],
                $this->instance->getNextRequestPart()
            );
        }
    }

    public function testRequestPartCountIsCorrect()
    {
        $this->initialize($this->request_basic, true);
        $expectation = 3;

        for ($i = $expectation; $i > 0; --$i) {
            $this->assertSame(
                $i,
                $this->instance->requestPartCount()
            );
            $this->instance->getNextRequestPart();
        }
    }

    public function testRequestPartCountReturnsZeroIfShiftingRequestIsEmpty()
    {
        $this->initialize("", true);
        $expectation = 0;

            $this->assertSame(
                $expectation,
                $this->instance->requestPartCount()
            );
    }

    public function testReturnsFullRequestWithoutLanguageInIt()
    {
        // WITHOUT LANGUAGE SUPPORT
        $language_support = false;
        $this->initialize(
            $this->request_basic,
            $language_support
        );
        $expectation = $this->request_basic;
        // INCLUDE ROOT SLASH
        $this->assertSame(
            $expectation,
            $this->instance->getFullRequest(false, false)
        );
        // REMOVED ROOT SLASH
        $expectation = substr($this->request_basic, 1);
        $this->assertSame(
            $expectation,
            $this->instance->getFullRequest(true, false)
        );


        // WITH LANGUAGE SUPPORT
        $language_support = true;
        $this->initialize(
            $this->request_basic,
            $language_support
        );
        // INCLUDE ROOT SLASH
        $expectation = substr($this->request_basic, 3);
        $this->assertSame(
            $expectation,
            $this->instance->getFullRequest(false, false)
        );
        // REMOVED ROOT SLASH
        $expectation = substr($this->request_basic, 4);
        $this->assertSame(
            $expectation,
            $this->instance->getFullRequest(true, false)
        );
    }

    public function testIsEmptyReturnsCorrectValue()
    {
        $this->initialize(
            "",
            true
        );
        $this->assertSame(
            true,
            $this->instance->isEmpty()
        );

        $this->initialize(
            $this->request_basic,
            true
        );
        $expectation = false;
        for ($i = 0; $i < 3; ++$i) {
            $this->instance->getNextRequestPart();
            if ($i === 2) {
                $expectation = true;
            }
            $this->assertSame(
                $expectation,
                $this->instance->isEmpty()
            );
        }
    }

    public function testReturnsCorrectLanguage()
    {
        $this->initialize(
            $this->request_basic,
            true
        );
        $expectation = "de";
        $this->assertSame(
            $expectation,
            $this->instance->getLanguage()
        );
    }

    public function testReturnsTrueIfLanguageIsAvailable()
    {
        $this->initialize(
            $this->request_basic,
            true
        );
        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->hasLanguage()
        );
    }

    public function testSettingLanguageWorks()
    {
        $this->initialize(
            $this->request_basic,
            true
        );
        $expectation = false;
        $this->assertSame(
            $expectation,
            $this->instance->setLanguage("not-a-language")
        );

        $expectation = true;
        $this->assertSame(
            $expectation,
            $this->instance->setLanguage("en")
        );

        $expectation = "/en/my/basic/page";
        $this->assertSame(
            $expectation,
            $this->instance->getFullRequest(false, true)
        );
    }
}
